﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.tools;

public class DebugDrawer : MonoBehaviour
{
    public Vector2 Velocity;
    public LayerMask layer;
    private Vector2 pos;
    public float timeScale=0.02f;
    public float gravity;
    public int numberOfIterations = 10;
    public Collider2D col;
	// Use this for initialization
	void OnEnable ()
	{
	    pos = transform.position;
	}
	
	// Update is called once per frame
    void OnDrawGizmos()
    {
        MathTools.DebugDrawTrajectory(col.bounds,numberOfIterations,gravity, transform.position, Velocity, 1,Time.fixedDeltaTime,layer);
        
    }
}
