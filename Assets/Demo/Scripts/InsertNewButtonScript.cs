﻿using Assets.Scripts.game.inventory.view;
using UnityEngine;

public class InsertNewButtonScript : MonoBehaviour
{

    public InventoryGrid Grid;
    public int NumberOfButtons;

    void Start()
    {
      Grid.InsertMultipleButtons(NumberOfButtons);
    }
    public void InsertNewButton()
    {
        
         Grid.InsertNewButton();
    }
}
