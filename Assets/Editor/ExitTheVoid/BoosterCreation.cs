﻿ 
using System.Collections.Generic; 
using Assets.Scripts.game.boosters.model;
using Assets.Scripts.game.inventory.Model;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor.ExitTheVoid
{
    /// <summary>
    /// In game editor tool to help create boosters
    /// </summary>
    public class BoosterCreation : EditorWindow
    {
        [SerializeField] // <--- add this for unity to serialize this field on assembly reloads or similar events
        private GuiBoosterModel _boosterModel;

        private bool _showBoosterModelEdits = true;
        private bool _showBonusAttributesEdits = true;
        private Dictionary<string, ConcreteBoosterModel> _boosterModels;
        private List<IInventoryItem> _boosters;
        private ExitTheVoidJsonConverterWriter _jsonConverterWriter;
       // private ExitTheVoidDBConnection _dbConnection;
        private int _id  ;
        [MenuItem("Window/Tools/Booster Creator")]
        public static void InitWindow()
        {
            GetWindow<BoosterCreation>();


        }
        // ReSharper disable once UnusedMember.Local
        void OnEnable()
        {
            hideFlags = HideFlags.HideAndDontSave;
            if (_boosterModel == null)
                _boosterModel = new GuiBoosterModel();
            if (_boosters == null)
            {
                _boosters = new List<IInventoryItem>();
            }
            if (_boosterModels == null)
            {
                _boosterModels = new Dictionary<string, ConcreteBoosterModel>();
            }
           /* if (_dbConnection == null)
            {
                _dbConnection = new ExitTheVoidDBConnection(Application.dataPath);
            }*/
            if (_jsonConverterWriter == null)
            {
                _jsonConverterWriter = new ExitTheVoidJsonConverterWriter();
            }
        }


        void SaveFile()
        {
            string path = EditorUtility.SaveFilePanel("Save Booster File",
                Application.dataPath + "/Resources/db", "", "booster");
            //save file 
            if (path.Length != 0)
            {
                _jsonConverterWriter.ConvertBoosterModelToJson(_boosterModels, path, true);
                _boosterModels.Clear();
                _id = 0;
            }
        }

        string LoadFile()
        {
            var returnpath = EditorUtility.OpenFilePanel("Select Booster File", Application.dataPath + "/Resources/db", "booster");
            Dictionary<string, ConcreteBoosterModel> boosters = _jsonConverterWriter.LoadJsonIntoDictionary(returnpath);
            _boosterModels = boosters;
            //_boosterModels = _jsonConverterWriter.LoadJsonIntoDictionary(returnpath);
            return returnpath;
        }
        private bool saveFileOption = false;

        // ReSharper disable once UnusedMember.Local
        // ReSharper disable once InconsistentNaming
        private void OnGUI()
        {

            // ReSharper disable once UnusedVariable
            Rect r = EditorGUILayout.BeginHorizontal("Button");
            GUILayout.Label("Booster model", EditorStyles.boldLabel);
            GUILayout.Label("Total number of boosters: " + _boosterModels.Count, EditorStyles.numberField);
            // GUILayout.Label("Total number of boosters: " + _dbConnection.GetBoosterCount(), EditorStyles.numberField);

            EditorGUILayout.EndHorizontal();
             
            if (!saveFileOption)
            {
                _showBoosterModelEdits = EditorGUILayout.Foldout(_showBoosterModelEdits, "Booster model");

                if (_showBoosterModelEdits)
                {

                    _boosterModel.OnGUI();
                }
                GUILayout.Space(10f);
                if (_boosterModel.Alteration == BoosterAlteration.PlayerStats)
                {
                    _showBonusAttributesEdits = EditorGUILayout.Foldout(_showBonusAttributesEdits, "Bonus Attributes");

                    _boosterModel.OnGUIBonus();
                }

                //Add booster item
                if (GUILayout.Button("Add Booster"))
                {
                    /* if (_boosterModel.Alteration == BoosterAlteration.PlayerStats)
                     {
                         _dbConnection.InsertNewBoosterAndBonusStats(_boosterModel);
                     }
                     else
                     {
                         _dbConnection.InsertNewBoosterIntoDB(_boosterModel);

                     }*/
                    _boosterModel.Id = _id;
                    _boosterModels.Add(_boosterModel.Id + "", _boosterModel);
                    _id++;

                    //  _jsonConverterWriter.ConvertBoosterModelToJson(_boosterModel);

                    _boosterModel = new GuiBoosterModel();
                }
                if (GUILayout.Button("Load Booster File"))
                {
                    string loadBoosterFilePath = LoadFile();
                    Debug.Log(loadBoosterFilePath);
                }
                if (_boosterModels.Count > 0)
                {
                    if (GUILayout.Button("Save Boosters to  file"))
                    {
                        SaveFile();
                    }
                }
                if (GUILayout.Button("Clear current boosters"))
                {
                    _boosterModels.Clear();

                }
            }

            else
            {
                SaveFile();
            }


        }

        [SerializeField]
        private class GuiBoosterModel : ConcreteBoosterModel
        {
            private string _spritePath;//need this string to remove Assets/Resources/
            private string _substring = "Assets/Resources/";
            // ReSharper disable once InconsistentNaming
            public void OnGUI()
            {
                GUILayout.MaxWidth(30);
                Name = EditorGUILayout.TextField("Booster Name", Name);
                Description = EditorGUILayout.TextField("Booster Description", Description);
                CoolDownTimer = EditorGUILayout.FloatField("Booster CoolDown", CoolDownTimer);
                Alteration = (BoosterAlteration)EditorGUILayout.EnumPopup("Booster Alteration", Alteration);
                Alignment = (BoosterAlignment)EditorGUILayout.EnumPopup("Booster Alignment", Alignment);

                Duration = (BoosterDuration)EditorGUILayout.EnumPopup("Number of charges", Duration);
                if (Duration == BoosterDuration.Cooldown)
                {
                    RemainingCharges = EditorGUILayout.IntField("Number of charges", RemainingCharges);
                }
                BoosterSprite = (Sprite)EditorGUILayout.ObjectField("Booster icon", BoosterSprite, typeof(Sprite), false);
                //get the index of the beginning of Assets
                _spritePath = (AssetDatabase.GetAssetPath(BoosterSprite));
                int lengthOfSubstring = _substring.Length;
                if (_spritePath.Length != 0)
                {
                    SpritePath = _spritePath.Substring(lengthOfSubstring, _spritePath.Length - lengthOfSubstring);
                }
                /*
                 * OnGUI(){ backgroundImage = (Texture2D) EditorGUILayout.ObjectField("Image", backgroundImage, typeof (Texture2D), false); }
                 */
            }

            // ReSharper disable once InconsistentNaming
            public void OnGUIBonus()
            {
                BonusStats.BonusHealthPool = EditorGUILayout.IntField("Bonus Health Pool", BonusStats.BonusHealthPool);
                BonusStats.BonusAttackPower = EditorGUILayout.IntField("Bonus Attack Power", BonusStats.BonusAttackPower);
                BonusStats.BonusAgility = EditorGUILayout.IntField("Bonus Agility", BonusStats.BonusAgility);
                BonusStats.BonusAccuracy = EditorGUILayout.Slider("Bonus Accuracy", BonusStats.BonusAccuracy, 0, 1);
                BonusStats.BonusChanceToStun = EditorGUILayout.Slider("Bonus Chance To Stun", BonusStats.BonusChanceToStun, 0, 1);
                BonusStats.BonusCritChance = EditorGUILayout.Slider("Bonus Chance To Crit", BonusStats.BonusCritChance, 0, 1);
                BonusStats.BonusDefence = EditorGUILayout.Slider("Bonus Defence%", BonusStats.BonusDefence, 0, 1);
            }
        }
    }


}