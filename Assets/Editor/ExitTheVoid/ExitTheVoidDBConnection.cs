﻿using System.Data; 
using Assets.Scripts.game.inventory.Model;
using Mono.Data.SqliteClient; 

namespace Assets.Editor.ExitTheVoid
{
    // ReSharper disable once InconsistentNaming
    public class ExitTheVoidDBConnection
    {
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private const string DbName = "ExitTheVoid.sqlite";
        private IDbConnection _dbConnection;
        
        public ExitTheVoidDBConnection(string path)
        {
            var applicationPath = "URI=file:" +path+ "/" + DbName;
            _dbConnection = new SqliteConnection(applicationPath);
       
            
        }

        public int ReturnLastInsertedId()
        {
            _dbConnection.Open();
            IDbCommand dbcmd = _dbConnection.CreateCommand();
            string query = @"select last_insert_rowid()";
            int id = 0;
            dbcmd.CommandText = query;
            IDataReader reader = dbcmd.ExecuteReader();
            while (reader.Read())
            {
                id = reader.GetInt32(0);
            }
            //cleanup
            reader.Close();
            // ReSharper disable once RedundantAssignment
            reader = null;
            dbcmd.Dispose();
            // ReSharper disable once RedundantAssignment
            dbcmd = null;
            _dbConnection.Close();
            return id;
        }
         
        public void InsertNewBoosterIntoDb(IInventoryItem booster)
        {
             
            _dbConnection.Open();
            IDbCommand dbcmd = _dbConnection.CreateCommand();
            string query = @"INSERT INTO Booster (NAME,DESCRIPTION,COOLDOWNTIMER,ALTERATION,DURATION,REMAININGCHARGES) ";
            string values = " VALUES (" + booster.Name + "," + booster.Description + "," + booster.CoolDownTimer + "," +
                            booster.Alteration + "," + booster.Duration+","+booster.RemainingCharges+")"; 
            query += values;
            dbcmd.CommandText = query;
            dbcmd.ExecuteNonQuery();
//VALUES (1, 'Paul', 32, 'California', 20000.00 );";
            
            dbcmd.Dispose();
            // ReSharper disable once RedundantAssignment
            dbcmd = null;
            _dbConnection.Close();
        }

        public void InsertNewBoosterAndBonusStats(IInventoryItem booster)
        {
            _dbConnection.Open();
            IDbCommand dbcmd = _dbConnection.CreateCommand();
            string query = @"INSERT INTO Booster (NAME,DESCRIPTION,COOLDOWNTIMER,ALTERATION,DURATION,REMAININGCHARGES)";
            string values = " VALUES (" + "\"" + booster.Name + "\"" + "," + "\"" + booster.Description + "\"" + "," + booster.CoolDownTimer + "," + "\"" +
                            booster.Alteration + "\"" + "," + "\"" + booster.Duration + "\"" + ","  + booster.RemainingCharges   + ")";
            
            query += values;
            
            dbcmd.CommandText = query;
            dbcmd.ExecuteNonQuery();
           
            query = @"INSERT INTO BoosterBonusStats (ID,HEALTHPOOL,BONUSAP,BONUSAGI,BONUSACC,BONUSCHANCESTUN,BONUSCRIT,BONUSDEFENCE) ";
            values = " VALUES (" + booster.Id + "," + booster.BonusStats.BonusHealthPool + "," +
                     booster.BonusStats.BonusAttackPower + "," + booster.BonusStats.BonusAgility + "," + booster.BonusStats.BonusAccuracy + "," +
                     booster.BonusStats.BonusChanceToStun + ","+ booster.BonusStats.BonusCritChance + "," + booster.BonusStats.BonusDefence + ")"; //todo: check if parentheses are closed in queries
          
            query += values;
            dbcmd.CommandText = query;
            dbcmd.ExecuteNonQuery();

            dbcmd.Dispose();
            // ReSharper disable once RedundantAssignment
            dbcmd = null;
            _dbConnection.Close();
        }

        public int GetBoosterCount()
        {
            string query = @"SELECT Count(*) FROM Booster";
            _dbConnection.Open();
            IDbCommand dbcmd = _dbConnection.CreateCommand();
           
            int count = 0;
            dbcmd.CommandText = query;
            IDataReader reader = dbcmd.ExecuteReader();
            while (reader.Read())
            {
                count = reader.GetInt32(0);
            }
            //cleanup
            reader.Close();
            // ReSharper disable once RedundantAssignment
            reader = null;
            dbcmd.Dispose();
            // ReSharper disable once RedundantAssignment
            dbcmd = null;
            _dbConnection.Close();
            return count;
        }

    }
}
