﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text; 
using Assets.Scripts.game.boosters.model;
using Assets.Scripts.game.inventory.Model;
using Assets.Scripts.game.model;
using LitJson;
using UnityEngine;

namespace Assets.Editor.ExitTheVoid
{
    /// <summary>
    /// this converts booster data into a json file 
    /// </summary>
    public class ExitTheVoidJsonConverterWriter
    {
     
        private JsonWriter _writer;
        private StringBuilder _stringBuilder;
        public ExitTheVoidJsonConverterWriter( )
        {
             
             Init();
        }

        private void Init()
        {
            _stringBuilder = new StringBuilder();
            _writer = new JsonWriter(_stringBuilder);
            _writer.PrettyPrint = true;
            _writer.IndentValue = 0;
        }

        public void ConvertBoosterModelToJson(IInventoryItem model)
        {
            JsonMapper.ToJson(model, _writer);
      
           ClearLocalStringBuilder();
        }

        /// <summary>
        /// Pass in a dictionary of booster models and if the flag is set to true, then create a file with the specified path file
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="filepath"></param>
        /// <param name="writeToFile"></param>
        public void ConvertBoosterModelToJson(Dictionary<string, ConcreteBoosterModel> dictionary, string filepath, bool writeToFile)
        {
            Init();
            JsonMapper.ToJson(dictionary, _writer);
            //save to file
            using (StreamWriter outfile = new StreamWriter(filepath))
            {
                outfile.Write(_stringBuilder.ToString());
            }
            ClearLocalStringBuilder();
        }

        public void ConvertLevelStateToModel(LevelStateModel model, string filepath, bool writeToFile)
        {
            Init();
            JsonMapper.ToJson(model, _writer);
            //save to file
            using (StreamWriter outfile = new StreamWriter(filepath))
            {
                outfile.Write(_stringBuilder.ToString());
            }
            ClearLocalStringBuilder();
        }

        private  void ClearLocalStringBuilder()
        {
            _stringBuilder.Length = 0;
            _stringBuilder.Capacity = 0;
        }



        /// <summary>
        /// returns a dictionary from the provided path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        internal Dictionary<string, ConcreteBoosterModel> LoadJsonIntoDictionary(string path)
        {
            string json = "";
            // ReSharper disable once RedundantAssignment
            Dictionary<string, ConcreteBoosterModel> returnDictionary = new Dictionary<string, ConcreteBoosterModel>(); 
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    json = sr.ReadToEnd();
                  
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.StackTrace);
               
            }
            returnDictionary = JsonMapper.ToObject<Dictionary<string, ConcreteBoosterModel>>(json);
            return returnDictionary;
            
        }
    }
}
