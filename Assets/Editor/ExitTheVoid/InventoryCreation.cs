﻿ 

using System.Collections.Generic;
using Assets.Scripts.game.boosters.model;
using UnityEditor;

namespace Assets.Editor.ExitTheVoid
{
    public class InventoryCreation : EditorWindow
    {
        private ExitTheVoidJsonConverterWriter _jsonConverterWriter;
        private Dictionary<string, ConcreteBoosterModel> _boosterModels;
        [MenuItem("Window/Tools/Booster Creator")]
        public static void InitWindow()
        {
            GetWindow<InventoryCreation>();


        }
    }
}
