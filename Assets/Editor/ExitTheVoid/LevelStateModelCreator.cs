﻿
using Assets.Scripts.game.model; 
using UnityEditor;
using UnityEngine;

namespace Assets.Editor.ExitTheVoid
{
    public class LevelStateModelCreator : EditorWindow
    {

        private GuiLevelStateModel _levelStateModel;
        private Scripts.tools.Json.ExitTheVoidJsonConverterWriter _jsonWriter;
        [MenuItem("Window/Tools/Level Creator")]
        public static void InitWindow()
        {
            GetWindow<LevelStateModelCreator>(); 
        }
        // ReSharper disable once UnusedMember.Local
        void OnEnable()
        {
            hideFlags = HideFlags.HideAndDontSave;
            if (_levelStateModel == null)
            {
                _levelStateModel = new GuiLevelStateModel();
            }
            if (_jsonWriter == null)
            {
                _jsonWriter = new Scripts.tools.Json.ExitTheVoidJsonConverterWriter();
            }
        }
 
        void SaveFile()
        {
            string path = EditorUtility.SaveFilePanel("Save Level File",
                Application.dataPath + "/Resources/db/levels", "", "levelmodel");
            //save file 
            _jsonWriter.ConvertLevelStateToJson(_levelStateModel,path,true);

        }
        // ReSharper disable once InconsistentNaming
        // ReSharper disable once UnusedMember.Local
        private void OnGUI()
        {
 
         _levelStateModel.OnGUI();
            if (GUILayout.Button("Create new Level"))
            {
                SaveFile();
            }
             
        }
          
    }
    class GuiLevelStateModel : LevelStateModel
    {
        // ReSharper disable once InconsistentNaming
        public void OnGUI()
        {
            GUILayout.MaxWidth(30);
            CurrentLevelName = EditorGUILayout.TextField("Level Name", CurrentLevelName);
            NextLevelName = EditorGUILayout.TextField("Next Level Name", NextLevelName);


            /*
             * OnGUI(){ backgroundImage = (Texture2D) EditorGUILayout.ObjectField("Image", backgroundImage, typeof (Texture2D), false); }
             */
        }
    }
}

