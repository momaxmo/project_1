﻿using UnityEngine;
using UnityEditor;
using System.IO;
using Assets.Scripts.pathfinding.navigation;
using Assets.Scripts.tools;

public  static  class ScriptableObjectUtility     {

    public static void SaveNavMap<T>(T asset, string path) where T : NavMapData
    {  
      //  string path = ExitTheVoidFilePathRefs.NavigationPath() + levelname + ".asset";
      
        NavMapData navmap = AssetDatabase.LoadAssetAtPath(path,typeof(NavMapData)) as NavMapData  ; 
        if (navmap != null)
        {
            EditorUtility.CopySerialized(asset, navmap);
            AssetDatabase.SaveAssets();
        } 
        
        else
        {
            AssetDatabase.CreateAsset(asset, path );
            AssetDatabase.SaveAssets();
        }
        
    }

    public static ScriptableObject LoadAsset<T>(string path) where T : ScriptableObject
    {
        T loadedData = AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
        return loadedData;
    }
    
    public static void SaveNavMapAsset<T>(T asset,string levelname) where T : ScriptableObject
    {
        string path = ExitTheVoidFilePathRefs.NavigationPath( );
        /* string path = AssetDatabase.GetAssetPath(Selection.activeObject);*/
        if (path == "")
        {
            path = "Assets";
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
    public static void SaveAsset<T>(T asset) where T : ScriptableObject
    {
        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "")
        {
            path = "Assets";
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }
         
        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
 public static void CreateAsset<T>() where T: ScriptableObject 
    {
        T asset = ScriptableObject.CreateInstance<T>();

        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "")
        {
            path = "Assets";
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}
