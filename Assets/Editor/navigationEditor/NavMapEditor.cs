﻿
using System;
using Assets.Scripts.filemanagement.controller.load;
using Assets.Scripts.pathfinding.navigation;
using Assets.Scripts.tools;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor.navigationEditor
{
    [CustomEditor(typeof(NavMap))]
    public class NavMapEditor : UnityEditor.Editor
    {
        // private bool _showButtonGroup;
        // private bool _navMapCreated;
        // private SerializedProperty _points, _showButtonGroup, _navMapCreated, _navMapLoaded; //float _nodeSizes;



        // private static Color _pointsColor;
        //private NavMapData _navigationData;
        private NavMap _navMap;
        private float _handleGizmoSize = 0.05f;
        private int _selectedPoint = -1;
        private Vector2 _gridSize;


        void OnEnable()
        {
            if (_navMap == null)
            {
                _navMap = target as NavMap;
                if (_navMap != null)
                {
                    _gridSize = new Vector2(_navMap.SizeX,_navMap.SizeY);
                }
            }
            /*_navMapLoaded = serializedObject.FindProperty("_navMapLoaded");
            _navMapCreated = serializedObject.FindProperty("_navMapCreated");
            _showButtonGroup = serializedObject.FindProperty("_showButtonGroup");
            _navMap = target as NavMap;*/

            //  _navigationData = CreateInstance<NavMapData>();
            // _navigationData.NodeSize = 1f;
            //    _navigationData.RescaleGridSize(10f);

            //   _navMap.TempNavMapData = _navigationData;
        }
        public override void OnInspectorGUI()
        {
            if (_navMap.TempNavMapData == null)
            {
                //check if there is some data that can be loaded
                string path = ExitTheVoidFilePathRefs.CleanedNavigationPath(EditorApplication.currentScene);
              //  string path = EditorApplication.currentScene+".asset";
                Debug.Log(path);

                if (ExitTheVoidTools.ResourceExist<NavMapData>("db/navmapdata/Level_1")) //todo fix path for generic level loading
                {
                    if (GUILayout.Button("Load existing Nav Map for this level"))
                    {
                        _navMap.Load(EditorApplication.currentScene);
                    }
                }
                else
                {
                    Debug.Log("resource doesn't exists");

                }
                if (GUILayout.Button("Create New Nav Map"))
                {
                    _navMap.CreateNaviationMapData(1, 10f);

                }
            }
            else
            {
                EditorGUILayout.LabelField("Number of nodes: "+ _navMap.Count);
                EditorGUILayout.LabelField("Grid sizes: "+_gridSize  );
              
                //target.lookAtPoint = EditorGUILayout.Vector3Field ("Look At Point", target.lookAtPoint);
                _navMap.Points[0] = EditorGUILayout.Vector3Field("Upper Left Point " , _navMap.Points[0]);
                _navMap.Points[1] = EditorGUILayout.Vector3Field("Upper Right Point ", _navMap.Points[1]);
                _navMap.Points[2] = EditorGUILayout.Vector3Field("Lower Left Point ", _navMap.Points[2]);
                _navMap.Points[3] = EditorGUILayout.Vector3Field("Lower Left Point ", _navMap.Points[3]);

                _navMap.GroundableMask= EditorGUILayout.LayerField("Groundable mask", _navMap.GroundableMask);
                _navMap.NodeSize = EditorGUILayout.FloatField("Node sizes", _navMap.NodeSize);
                if (_navMap.NodeSize <= 0)
                {
                    _navMap.NodeSize = -0.1f;
                }

                PickColorData();
                
                //Save/ReLoad/Reset
                ShowButtonGroup();
            }


            /*  bool navMapCreated = _navMapCreated.boolValue;
            Debug.Log(navMapCreated);
            if (navMapCreated)
            {
                _navMap.NodeSize = EditorGUILayout.FloatField("Node sizes", _navMap.NodeSize);
                //check if less than 0-> set to 0.1f
                if (_navMap.NodeSize <= 0)
                {
                    _navMap.NodeSize = _navMap.NodeSize = 0.25f;
                }

                PickColorData();
                ShowButtonGroup();
                DrawDefaultInspector();
            }
            else
            {
                if (GUILayout.Button("Create Nav Map"))
                {
                    _navMap = (NavMap)target;
                    _navMap.Init(1f, 10f);
                    _navMapCreated.boolValue = true;
                }
                LoadButtonData();
            }
*/

        }


        private void OnSceneGUI()
        {
            if (_navMap.TempNavMapData != null)
            {
                var points = _navMap.Points;
                Matrix4x4 invMatrix = Matrix4x4.identity;
                #region handles

                //create a handles for the movable points

                for (int i = 0; i < points.Length; i++)
                {
                    Handles.color = Color.red;
                    Undo.RecordObject(_navMap, "Moved position");
                    Handles.SphereCap(-i - 1, points[i], Quaternion.identity, _handleGizmoSize);
                    Vector3 prePos = points[i];
                    Vector3 postPos = Handles.PositionHandle(points[i], Quaternion.identity);
                    Handles.BeginGUI();
                    // information label
                    if (i == 1 || i == 3)
                        Handles.Label(points[i] + Vector3.right * 0.5f, "index: " + i + "\n Position " + points[i]);
                    else
                        Handles.Label(points[i] - Vector3.right * 0.5f, "index: " + i + "\n Position " + points[i]);
                    GUILayout.BeginArea(new Rect(Screen.width - 100, Screen.height - 80, 90, 50));
                    GUILayout.EndArea();
                    Handles.EndGUI();
                    //detect changes on x

                    //detect changes on y
                    if (prePos != postPos)
                    {
                        _navMap.Reset();
                        points[i] = invMatrix.MultiplyPoint3x4(postPos); 
                        switch (i)
                        {
                            case 0:
                                if (!Mathf.Approximately(prePos.x, postPos.x))
                                {
                                    ChangeXPos(2, postPos.x, ref points);
                                }
                                if (!Mathf.Approximately(prePos.y, postPos.y))
                                {
                                    ChangeYPos(1, postPos.y, ref points);
                                }
                                break;
                            case 1:
                                if (!Mathf.Approximately(prePos.x, postPos.x))
                                {
                                    ChangeXPos(3, postPos.x, ref points);
                                }
                                if (!Mathf.Approximately(prePos.y, postPos.y))
                                {
                                    ChangeYPos(0, postPos.y, ref points);

                                }
                                break;
                            case 2:

                                if (!Mathf.Approximately(prePos.x, postPos.x))
                                {
                                    ChangeXPos(0, postPos.x, ref points);
                                }
                                if (!Mathf.Approximately(prePos.y, postPos.y))
                                {
                                    ChangeYPos(3, postPos.y, ref points);

                                }
                                break;
                            case 3:
                                if (!Mathf.Approximately(prePos.x, postPos.x))
                                {
                                    ChangeXPos(1, postPos.x, ref points);
                                }
                                if (!Mathf.Approximately(prePos.y, postPos.y))
                                {
                                    ChangeYPos(2, postPos.y, ref points);

                                }
                                break;

                        }
                        float sizeX = Mathf.Abs(points[1].x - points[0].x);
                        float sizeY = Mathf.Abs(points[2].x - points[0].x);
                        _navMap.SetNewSize(sizeX, sizeY);
                        //EditorUtility.SetDirty(target);
                    }

                    #endregion

                }
                _navMap.Points = points;
                EditorUtility.SetDirty(target);
                DrawLines(points);
              //  DrawGrid(points);
                if (Event.current.type == EventType.MouseDown)
                {
                    int pre = _selectedPoint;
                    _selectedPoint = -(HandleUtility.nearestControl + 1);
                    if (pre != _selectedPoint)
                    {
                        GUI.changed = true;
                    }

                }
                
            }
            /*if (_navMapCreated.boolValue)
            {
                //NavMap script = (NavMap)target;
                var points = _navMap.Points;
                Matrix4x4 invMatrix = Matrix4x4.identity;

                #region handles

                //create a handles for the movable points

                for (int i = 0; i < points.Length; i++)
                {
                    Handles.color = Color.red;
                    Undo.RecordObject(_navMap, "Moved position");
                    Handles.SphereCap(-i - 1, points[i], Quaternion.identity, _handleGizmoSize);
                    Vector3 prePos = points[i];
                    Vector3 postPos = Handles.PositionHandle(points[i], Quaternion.identity);
                    Handles.BeginGUI();
                    // information label
                    if (i == 1 || i == 3)
                        Handles.Label(points[i] + Vector3.right * 0.5f, "index: " + i + "\n Position " + points[i]);
                    else
                        Handles.Label(points[i] - Vector3.right * 0.5f, "index: " + i + "\n Position " + points[i]);
                    GUILayout.BeginArea(new Rect(Screen.width - 100, Screen.height - 80, 90, 50));
                    GUILayout.EndArea();
                    Handles.EndGUI();
                    //detect changes on x

                    //detect changes on y
                    if (prePos != postPos)
                    {
                        _navMap.Reset();
                        points[i] = invMatrix.MultiplyPoint3x4(postPos);
                        Debug.Log(i);
                        switch (i)
                        {
                            case 0:
                                if (!Mathf.Approximately(prePos.x, postPos.x))
                                {
                                    ChangeXPos(2, postPos.x, ref points);
                                }
                                if (!Mathf.Approximately(prePos.y, postPos.y))
                                {
                                    ChangeYPos(1, postPos.y, ref points);
                                }
                                break;
                            case 1:
                                if (!Mathf.Approximately(prePos.x, postPos.x))
                                {
                                    ChangeXPos(3, postPos.x, ref points);
                                }
                                if (!Mathf.Approximately(prePos.y, postPos.y))
                                {
                                    ChangeYPos(0, postPos.y, ref points);

                                }
                                break;
                            case 2:

                                if (!Mathf.Approximately(prePos.x, postPos.x))
                                {
                                    ChangeXPos(0, postPos.x, ref points);
                                }
                                if (!Mathf.Approximately(prePos.y, postPos.y))
                                {
                                    ChangeYPos(3, postPos.y, ref points);

                                }
                                break;
                            case 3:
                                if (!Mathf.Approximately(prePos.x, postPos.x))
                                {
                                    ChangeXPos(1, postPos.x, ref points);
                                }
                                if (!Mathf.Approximately(prePos.y, postPos.y))
                                {
                                    ChangeYPos(2, postPos.y, ref points);

                                }
                                break;

                        }
                        float sizeX = Mathf.Abs(points[1].x - points[0].x);
                        float sizeY = Mathf.Abs(points[2].x - points[0].x);
                        _navMap.SetNewSize(sizeX, sizeY);
                        //EditorUtility.SetDirty(target);
                    }

                    #endregion

                }
                _navMap.Points = points;
                EditorUtility.SetDirty(target);
                DrawLines(points);
                DrawGrid(points);
                if (Event.current.type == EventType.MouseDown)
                {
                    int pre = _selectedPoint;
                    _selectedPoint = -(HandleUtility.nearestControl + 1);
                    if (pre != _selectedPoint)
                    {
                        GUI.changed = true;
                    }

                }
                DrawNodes();
            }
*/

        }
       
        private void PickColorData()
        {
            GUILayout.Space(5);
            GUILayout.Label("Node colors");
            GUILayout.Space(2);
            GUILayout.BeginVertical();
            _navMap.TempNavMapData.NoneColor = EditorGUILayout.ColorField("Color for none type", _navMap.TempNavMapData.NoneColor);
            _navMap.TempNavMapData.LeftEdgeColor = EditorGUILayout.ColorField("Color for left edges", _navMap.TempNavMapData.LeftEdgeColor);
            _navMap.TempNavMapData.RightEdgeColor = EditorGUILayout.ColorField("Color for right edges", _navMap.TempNavMapData.RightEdgeColor);
            _navMap.TempNavMapData.PlatformColor = EditorGUILayout.ColorField("Color for platforms", _navMap.TempNavMapData.PlatformColor);
            _navMap.TempNavMapData.SoloEdgeColor = EditorGUILayout.ColorField("Color for solo edges", _navMap.TempNavMapData.SoloEdgeColor);
            GUILayout.EndVertical();

        }

        private void ShowButtonGroup()
        {


            //button to scan
            if (GUILayout.Button("Scan"))
            {
                _navMap.Scan();
            }
            //button to save 
            if (GUILayout.Button("Save Nav Map data"))
            {
                _navMap.Save(EditorApplication.currentScene);

            }
            LoadButtonData();

        }

        private void LoadButtonData()
        {
            if (GUILayout.Button("Reload Nav Map data"))
            {
                //_navMap.Load(EditorApplication.currentScene);
                _navMap.ReloadData(); 
            }

            
        }
        private void DrawHelperHandles(Vector3[] points)
        {

        }
        private void DrawLines(Vector3[] points)
        {
            if (PointsAreValid(points))
            {
                Handles.color = Color.green;
                Handles.DrawLine(points[0], points[1]);
                Handles.DrawLine(points[0], points[2]);
                Handles.DrawLine(points[1], points[3]);
                Handles.DrawLine(points[2], points[3]);
            }
            else
            {
                Handles.color = Color.red;
                Handles.DrawLine(points[0], points[1]);
                Handles.DrawLine(points[0], points[2]);
                Handles.DrawLine(points[1], points[3]);
                Handles.DrawLine(points[2], points[3]);
                Handles.DrawLine(points[1], points[2]);
                Handles.DrawLine(points[0], points[3]);
            }

        }

        private bool PointsAreValid(Vector3[] points)
        {
            //check if points 0 and 1 are above points 2 and 3
            bool check1 = points[0].y > points[2].y;
            bool check2 = points[1].y > points[3].y;
            if (!(check1 && check2))
            {
                return false;
            }
            //check if points 0 is left of point 1, points 2 is less than point 3
            bool check3 = points[0].x < points[1].x;
            bool check4 = points[2].x < points[3].x;
            if (!(check3 && check4))
            {
                return false;
            }
            return true;
        }

        private void DrawGrid(Vector3[] points)
        {
            if (PointsAreValid(points))
            {
                Color gridCol = Color.green;
                gridCol.a = 0.1f;
                Handles.color = gridCol;
                //calc vertical segments
                float vLength = _navMap.SizeY = Mathf.Abs(points[2].y - points[0].y);
                int vSegments = (int)(vLength / _navMap.NodeSize);

                //calculate horizontal segments
                float hLength = _navMap.SizeX = Mathf.Abs(points[1].x - points[0].x);
                int hSegments = (int)(hLength / _navMap.NodeSize);

                Vector2 vStart = points[0];
                Vector2 vEnd = points[2];
                Vector2 hStart = points[0];
                Vector2 hEnd = points[1];
                for (int i = 0; i <= hSegments; i++)
                {
                    Vector2 startPos = vStart;
                    Vector2 endPos = vEnd;
                    startPos.x += i * _navMap.NodeSize;
                    endPos.x += i * _navMap.NodeSize;
                    Handles.DrawLine(startPos, endPos);
                }
                for (int i = 0; i <= vSegments; i++)
                {
                    Vector2 startPos = hStart;
                    Vector2 endPos = hEnd;
                    startPos.y -= i * _navMap.NodeSize;
                    endPos.y -= i * _navMap.NodeSize;
                    Handles.DrawLine(startPos, endPos);
                }
            }

        }

        private void DrawNodes()
        {
            //before drawing nodes, check if their count >1
            if (_navMap.Count > 0)
            {
                
              //  Debug.Log("x " + x + " y" + y);
                bool scanned = _navMap.Scanned;
                if (scanned)
                {
                    int[] length = _navMap.Length();
                    int x = length[0];
                    Debug.Log("x " + x);
                    int y = length[1]; Debug.Log("y " + y);
                    // for (int i = 0; i < (int)_navMap.SizeX; i++)
                    for (int i = 0; i <x ; i++)
                    {
                        // for (int j = 0; j < (int)_navMap.SizeY; j++)
                        for (int j = 0; j < y; j++)
                        {
                            
                                NavigationPoint point = _navMap[i, j];
                                DrawNodeHelper(point);
                             
                          
                                //Debug.Log("tried to access i"+i+" j "+j +" while sizex and y are "+ (int)_navMap.SizeX+" "+ (int)_navMap.SizeY);
                                 
                             
                           

                        }
                    }
                }
                HandleUtility.Repaint();
            }



        }

        private void DrawNodeHelper(NavigationPoint point)
        {
            switch (point.NodeType)
            {
                case NavigationType.None:
                    Handles.color = _navMap.TempNavMapData.NoneColor;
                    break;
                case NavigationType.LeftEdge:
                    Handles.color = _navMap.TempNavMapData.LeftEdgeColor;
                    break;
                case NavigationType.RightEdge:
                    Handles.color = _navMap.TempNavMapData.RightEdgeColor;
                    break;
                case NavigationType.Solo:
                    Handles.color = _navMap.TempNavMapData.SoloEdgeColor;
                    break;
                case NavigationType.Platform:
                    Handles.color = _navMap.TempNavMapData.PlatformColor;
                    break;
            }
            int controlid = point.Id;
            Handles.CubeCap(controlid, point.TilePosition, Quaternion.identity, _navMap.NodeSize / 2);
        }
        private void ChangeXPos(int index, float to, ref Vector3[] points)
        {
            Matrix4x4 invMatrix = Matrix4x4.identity;
            Vector3 newPos = points[index];
            newPos.x = to;
            points[index] = invMatrix.MultiplyPoint3x4(newPos);
            Handles.SphereCap(-index - 1, points[index], Quaternion.identity, 1f);
            HandleUtility.Repaint();
        }
        private void ChangeYPos(int index, float to, ref Vector3[] points)
        {
            Matrix4x4 invMatrix = Matrix4x4.identity;
            Vector3 newPos = points[index];
            newPos.y = to;
            points[index] = invMatrix.MultiplyPoint3x4(newPos);
            Handles.SphereCap(-index - 1, points[index], Quaternion.identity, 1f);
            HandleUtility.Repaint();

        }


    }
    /*    [CustomEditor(typeof(NavMap))]
        public class NavMapEditor : UnityEditor.Editor
        {
            private bool _dataExists;
            private SerializedProperty _testButtonPressed;
            private bool buttonpressed;
            void OnEnable()
            {
                _testButtonPressed = Findpor
            }
            public override void OnInspectorGUI()
            {
               /* if (!_dataExists)
                {
                    NavMapData _data = ScriptableObjectUtility.LoadAsset<NavMapData>(ExitTheVoidFilePathRefs.CleanedNavigationPath(EditorApplication.currentScene)) as NavMapData;
                    if (_data != null)
                    {
                        if (GUILayout.Button("Load Nav Map data"))
                        {

                            _dataExists = true;
                        }
                    }
                }
                if (GUILayout.Button("set button press to true"))
                {
                    buttonpressed = true;
                }
                    Debug.Log("button pressed "+ buttonpressed);
                //check if the data actually exists
               #1#
            }
        }*/

        
}
