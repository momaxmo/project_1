﻿ 
using System.Collections.Generic;
using Assets.Scripts.game.inventory.view;
using UnityEngine;  

public class GridTest : MonoBehaviour
{
    

    public InventoryGrid Grid;
    public int RowId;
    public int ColumnId;
	// Use this for initialization
	void Start ()
	{
	    Grid = GetComponent<InventoryGrid>();
	   List<IterableButton> buttons =   Grid.InsertMultipleButtons(20);
	    for (int i = 0; i < buttons.Count; i++)
	    {
	        IterableButton button = buttons[i];
            button.UpdateCallback(()=>RemoveButton(button.RowIndex,button.ColumnIndex));
	         
	    }
	}
	
	// Update is called once per frame
	void Update () {

	}
  
    void RemoveButton(int row, int column)
    {
        Grid.RemoveElementAndShiftLeftCopy(row, column); 
    }
    static void SomeMethod(int n)
    {
    }

    public void InsertButton()
    {
        IterableButton button = Grid.InsertNewButton();
        //  button.UpdateCallback(method, new List<int>() { button.RowIndex, button.ColumnIndex });
        button.UpdateCallback(() => RemoveButton(button.RowIndex, button.ColumnIndex));
    }


  
    
    
}
