﻿using UnityEngine;
using System.Collections;

public class InputAngleTest : MonoBehaviour
{

    public UnityEngine.UI.Text test;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	    float h = Input.GetAxis("Horizontal");
	    float v = Input.GetAxis("Vertical");
        Vector2 two = new Vector2(h,v);
	    Vector2 pos = transform.position;
	    Vector2 forwardVect = pos + Vector2.right;
	    Vector2 toVector = pos+ two;
	    float angle1 = Mathf.Atan2(v, h);
        float angle2 = angle1 * Mathf.Rad2Deg;
        float angle3 = Vector2.Angle(forwardVect, toVector);
        test.text = "" + angle1 + "\n" + angle2 + "\n toVector" + toVector + "\n Vector angle: " + angle3;

	}
}
