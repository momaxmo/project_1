﻿
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Assets.Scripts.Debugging
{
    public class AddTestItem : View
    {
        public Signal<string> SendSignal = new Signal<string>();

        void AddItem()
        {
            SendSignal.Dispatch("3");
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                AddItem();  
            }
        }
    }
}
