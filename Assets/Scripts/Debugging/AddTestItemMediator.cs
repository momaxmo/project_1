﻿ 
using Assets.Scripts.common.config;
using Assets.Scripts.Debugging.signals;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.Debugging
{
    public class AddTestItemMediator: Mediator
    {
        [Inject]
        public TakeALookAtSignal Takealook { get; set; }
        [Inject]
        public AddTestItem AddTestItem { get; set; }
        [Inject]
        public AddNewInventoryItemSignal AddNewInvSig { get; set; }

        [Inject]
        public CreateTestObjectWithTwoViewSignal CreateObjectWithTwoViews { get; set; }
        public override void OnRegister()
        {
            AddTestItem.SendSignal.AddListener(AddNewItem);
        }

        public override void OnRemove()
        {
            AddTestItem.SendSignal.RemoveListener(AddNewItem);

        }

        private void AddNewItem(string key)
        {
            AddNewInvSig.Dispatch(key);
            Takealook.Dispatch();
            CreateObjectWithTwoViews.Dispatch();
            Debug.Log("adding item in "+GetType());
            
        }
    }
}
