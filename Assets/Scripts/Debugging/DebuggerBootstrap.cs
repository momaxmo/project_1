﻿ 

using strange.extensions.context.impl;
using UnityEngine;

namespace Assets.Scripts.Debugging
{
   public  class DebuggerBootstrap: ContextView
    {
       void Awake()
       {
           context = new DebuggerContext(this);
           Debug.Log("adding new item");
       }
    }
}
