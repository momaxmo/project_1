﻿ 

using Assets.Scripts.Debugging.controller;
using Assets.Scripts.Debugging.onScreenDebugger.model;
using Assets.Scripts.Debugging.onScreenDebugger.view;
using Assets.Scripts.Debugging.signals;
using Assets.Scripts.Debugging.testgameobject;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using UnityEngine;

namespace Assets.Scripts.Debugging
{
    public class DebuggerContext : MVCSContext
    {
        public DebuggerContext(MonoBehaviour contextView)
            : base(contextView)
        {
        }
        protected override void addCoreComponents()
        {
            base.addCoreComponents();
            injectionBinder.Unbind<ICommandBinder>();
            injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
         
        }
        protected override void mapBindings()
        {
            injectionBinder.Bind<DebugableItemsSingleton>().ToSingleton();
            injectionBinder.Bind<AddPlayerToDebugView>().ToSingleton().CrossContext();
            commandBinder.Bind<AddPlayerToDebugView>().To<AddToDebugSingletonCommand>();
            mediationBinder.BindView<OnScreenDebuggerView>().ToMediator<OnScreenDebuggerMediator>();

            injectionBinder.Bind<Testmodel>().ToSingleton();
            mediationBinder.BindView<AddTestItem>().ToMediator<AddTestItemMediator>();
            injectionBinder.Bind<TakeALookAtSignal>().ToSingleton().CrossContext();
            commandBinder.Bind<TakeALookAtSignal>().To<LookAtInjectionBinder>();

            mediationBinder.Bind<TestObjectFirstView>().To<TestObjectFirstViewMediator>();
            mediationBinder.Bind<TestObjectSecondView>().To<TestObjectSecondViewMediator>();

            commandBinder.Bind<CreateTestObjectWithTwoViewSignal>().To<CreateTestObject>();
        }
    }
}
