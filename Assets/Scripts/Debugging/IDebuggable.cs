﻿ 
namespace Assets.Scripts.Debugging
{
    public interface IDebuggable
    {
        string ViewDebugText();
        string Id { get; }
    }
}
