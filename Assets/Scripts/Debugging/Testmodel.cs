﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Debugging
{
    public class Testmodel
    {
        private string _testerString = "******* Tried , tested********";

        public string TesterString
        {
            get { return _testerString; }
        }
    }
}
