﻿
using Assets.Scripts.Debugging.onScreenDebugger.model;
using Assets.Scripts.game.player.model;
using Assets.Scripts.game.view.player;
using strange.extensions.command.impl;

namespace Assets.Scripts.Debugging.controller
{
  public   class AddToDebugSingletonCommand: Command
    {
      [Inject]
      public DebugableItemsSingleton DebuggableItemSingleton { get; set; }
      [Inject]
      public MainPlayerModel MainPlayerModel { get; set; }

      public override void Execute()
      {
            DebuggableItemSingleton.Add(MainPlayerModel.PlayerBehaviourParameters as PlayerBehaviourParameters);
          DebuggableItemSingleton.Add(MainPlayerModel.PlayerControllerParameters as PlayerControllerParameters);
           DebuggableItemSingleton.Add(MainPlayerModel.PlayerBehaviourPermissions as PlayerBehaviourPermissions);
           DebuggableItemSingleton.Add(MainPlayerModel.PlayerBehaviourState as CharacterBehaviourState);
           DebuggableItemSingleton.Add(MainPlayerModel.PlayerControllerState as PlayerControllerState);
           DebuggableItemSingleton.Add(MainPlayerModel.PlayerStats as PlayerAttributesAndStats); 
          DebuggableItemSingleton.Add(MainPlayerModel.PlayerBehaviourView as PlayerBehaviourView);
          DebuggableItemSingleton.Add(MainPlayerModel.PlayerControllerView as PlayerControllerView);
      }

    
    }
}
