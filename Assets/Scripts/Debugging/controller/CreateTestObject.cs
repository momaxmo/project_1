﻿ 
using Assets.Scripts.Debugging.testgameobject;
using Assets.Scripts.tools;
using strange.extensions.command.impl;
using UnityEngine;

namespace Assets.Scripts.Debugging.controller
{
    public class CreateTestObject: Command
    {
        public override void Execute()
        {
            GameObject dbug = GameObject.Find("debuggercontext");
            //GameObject go = new GameObject();
            GameObject go =
                GameObject.Instantiate(UnityEngine.Resources.Load(ExitTheVoidFilePathRefs.DebugPrefabPath)) as
                    GameObject;
            go.name = "DebugObject";
            go.transform.parent = dbug.transform;/*
            go.AddComponent<TestObjectFirstView>();
            go.AddComponent<TestObjectSecondView>();*/
        }
    }
}
