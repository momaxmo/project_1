﻿  
using Assets.Scripts.Debugging.signals;
using strange.extensions.command.impl; 
using UnityEngine;

namespace Assets.Scripts.Debugging.controller
{
    public class LookAtInjectionBinder: Command
    {
        public override void Execute()
        {
           
            injectionBinder.GetBinding<TakeALookAtSignal>();
        }
    }
}
