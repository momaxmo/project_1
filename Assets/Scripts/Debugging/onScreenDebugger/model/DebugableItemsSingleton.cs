﻿ 

using System.Collections.Generic;

namespace Assets.Scripts.Debugging.onScreenDebugger.model
{
    /// <summary>
    /// A class that holds a set of IDebuggables
    /// </summary>
    public class DebugableItemsSingleton
    {
         
     
        private Dictionary<string, IDebuggable> _debuggableSet;
        private List<string> _keys;
        private int _currentIndex;
        private static readonly PlaceholderDebuggableItem Placeholder = new PlaceholderDebuggableItem();
        public Dictionary<string, IDebuggable> DebuggableSet
        {
            get
            {
                if (_debuggableSet == null)
                {
                    _debuggableSet = new Dictionary<string, IDebuggable>(5);
                }
                return _debuggableSet;
            }
            set { _debuggableSet = value; }
        }

        public List<string> Keys
        {
            get
            {
                if (_keys == null)
                { _keys = new List<string>(5);}
                return _keys;
            }
        }
 
        /// <summary>
        /// ensuring that an item doesn't already exist in the set, adds an item to the list
        /// </summary>
        /// <param name="item"></param>
        public void Add(IDebuggable item)
        {
            if (!DebuggableSet.ContainsKey(item.Id))
            {
                Keys.Add(item.Id);
                DebuggableSet.Add(item.Id,item);
            }
            
        }

        public IDebuggable Next()
        {
            if (DebuggableSet.Count == 0)
            {
                return Placeholder;
            }
            _currentIndex++;
            if (_currentIndex > Keys.Count - 1)
            {
                _currentIndex = 0;
            }
            string key = Keys[_currentIndex];
            IDebuggable item = DebuggableSet[key];
           
            return item;
        }

        public IDebuggable Previous()
        { 
            if (DebuggableSet.Count == 0)
            {
                return Placeholder;
            }
            _currentIndex--;
            if (_currentIndex < 0)
            {
                _currentIndex = Keys.Count - 1;
            }
            string key = Keys[_currentIndex];
            IDebuggable item = DebuggableSet[key];
          
            return item;
        }
        /// <summary>
        /// Placeholder item in the case that the singleton doesn't contain any items.
        /// </summary>
        private class PlaceholderDebuggableItem : IDebuggable
        {
            public string ViewDebugText()
            {
                return "Nothing registered yet";
            }

            public string Id
            {
                get { return "0"; }
            }
        }
    }
}
