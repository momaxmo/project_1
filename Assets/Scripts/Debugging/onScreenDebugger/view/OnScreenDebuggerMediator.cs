﻿ 
using Assets.Scripts.Debugging.onScreenDebugger.model;
using Assets.Scripts.game.player.model;
using Assets.Scripts.game.uielements.healthbar.signal;
using strange.extensions.mediation.impl;

namespace Assets.Scripts.Debugging.onScreenDebugger.view
{
     public class OnScreenDebuggerMediator : Mediator
    {
         [Inject]
         public DebugableItemsSingleton DebugableItemsSingleton { get; set; }

         [Inject]
         public OnScreenDebuggerView View { get; set; }
         [Inject]
         public UpdateHealthBarSignal UpdateHealthBarSignal { get; set; }

         [Inject]
         public IPlayerModel Player { get; set; }
         public override void OnRegister()
         {
            View.NextItemSignal.AddListener(NextItemInDebugSet);
            View.PreviousItemSignal.AddListener(PreviousItemInDebugSet);
             View.Player = Player;
             View.UpdateNewHealthSignal.AddListener(DispatchHealthSignal);
         }

         public override void OnRemove()
         {
             View.NextItemSignal.RemoveListener(NextItemInDebugSet);
             View.PreviousItemSignal.RemoveListener(PreviousItemInDebugSet);
             View.UpdateNewHealthSignal.RemoveListener(DispatchHealthSignal);
         }

         private void NextItemInDebugSet()
         {
             IDebuggable item = DebugableItemsSingleton.Next();
             View.UpdateItemToView(item);
         }

         private void PreviousItemInDebugSet()
         {
             IDebuggable item = DebugableItemsSingleton.Previous();
             View.UpdateItemToView(item);
         }

         private void DispatchHealthSignal()
         {
             UpdateHealthBarSignal.Dispatch();
         }

    }

}
