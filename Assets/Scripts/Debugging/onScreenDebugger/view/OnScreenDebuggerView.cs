﻿ 
using Assets.Scripts.game.player.model;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Assets.Scripts.Debugging.onScreenDebugger.view
{
    public class OnScreenDebuggerView : View
    {


 public IDebuggable ItemToView { get; set; }
        private UnityEngine.UI.Text _debugText;
        public int Damage = 3;
        public  IPlayerModel Player { get; set; }
        public Signal UpdateNewHealthSignal = new Signal();
        public UnityEngine.UI.Text DebugText
        {
            get
            {
                if (_debugText == null)
                {
                    _debugText = gameObject.GetComponent<UnityEngine.UI.Text>();
                }
                return _debugText;
                
            }
        }

 

        public  Signal NextItemSignal = new Signal();
        public Signal PreviousItemSignal = new Signal();
        // ReSharper disable once UnusedMember.Local
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.O))
            {
                Previous();
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                Next();
            }

            /*if (Input.GetKeyDown(KeyCode.F9))
            {
                if (Player != null)
                {
                   
                    int damage = Player.PlayerStats.CurrentDamageTaken;
                    if (Player.PlayerStats.CurrentHealth > 0)
                    {
                        damage += Damage;
                        Player.PlayerStats.CurrentDamageTaken = damage;
                        UpdateNewHealthSignal.Dispatch();
                    }
                    if (Player.PlayerStats.CurrentHealth < Damage)
                    {
                        Player.PlayerStats.CurrentDamageTaken = 0;
                        UpdateNewHealthSignal.Dispatch();
                    }
                }
                
                
            }*/
            DebugText.text = ItemToView !=null? ItemToView.ViewDebugText(): "";
        }

        private void Next()
        {   
            NextItemSignal.Dispatch();   
        }

        void Previous()
        {
            PreviousItemSignal.Dispatch();
        }

        public void UpdateItemToView(IDebuggable newItem)
        {
            ItemToView = newItem;
           
        }


    }
}
