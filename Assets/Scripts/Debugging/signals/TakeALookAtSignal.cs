﻿ 
using Assets.Scripts.game.player.model;
using strange.extensions.signal.impl;

namespace Assets.Scripts.Debugging.signals
{
    public class TakeALookAtSignal: Signal
    {
    }

    public class CreateTestObjectWithTwoViewSignal : Signal
    {
    }

    public class AddPlayerToDebugView : Signal<MainPlayerModel>
    {
    }
}
