﻿ 
  
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.Debugging.testgameobject
{
    public class TestObjectFirstView : View
    {
        public Testmodel Model;

        void Update()
        {
            Debug.Log(Model.TesterString);
            
        }

    }
}
