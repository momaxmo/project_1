﻿ 
using strange.extensions.mediation.impl;

namespace Assets.Scripts.Debugging.testgameobject
{
   public class TestObjectFirstViewMediator : Mediator
    {
       [Inject]
       public TestObjectFirstView View { get; set; }
       [Inject]
       public Testmodel Model { get; set; }

       public override void OnRegister()
       {
           View.Model = Model;
       }
    }
}
