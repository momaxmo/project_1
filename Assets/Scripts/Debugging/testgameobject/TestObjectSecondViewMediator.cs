﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.mediation.impl;

namespace Assets.Scripts.Debugging.testgameobject
{
    public class TestObjectSecondViewMediator : Mediator
    {
        [Inject]
        public TestObjectSecondView Testview2 { get; set; }
    }
}
