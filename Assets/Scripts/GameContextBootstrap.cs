﻿ 

using Assets.Scripts.common.config;
using strange.extensions.context.impl;

namespace Assets.Scripts
{
    public class GameContextBootstrap: ContextView
    {
        private void Awake()
        {
            context = new GameContext(this);
        }
    }
}
