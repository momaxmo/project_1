﻿ 
using UnityEngine; 

namespace Assets.Scripts.tools
{
    /// <summary>
    /// simple static class for file paths and their references
    /// </summary>
    public static class ExitTheVoidFilePathRefs
    {
        public static string TesterBoosterDb
        {
            get { return Application.dataPath + "/Resources/db/Testbooster.booster"; }
        }

        public static string PlayerFileDataPath
        {
            get
            {
                return Application.dataPath + "/Resources/data/PlayerFile.gs";
            }
        }

        public static string LevelModelDataPath(string levelName)
        {
            return Application.dataPath + "/Resources/db/levels/" + levelName + ".levelmodel";
        }

        public static string InventoryDbPath(string playerId)
        {
            return Application.dataPath + "/Resources/db/playerData/" + playerId + "inv.inventory";
        }

        public static string NavigationPath( )
        {
            
            string path =  "Assets/Resources/db/navmapdata/";
            return path;
            //return Application.dataPath + "/Resources/db/navmapdata/"  ;
        }

        /// <summary>
        /// returns the path  of the level navigation map saved asset
        /// </summary>
        /// <param name="levelName"></param>
        /// <returns></returns>
        public static string CleanedNavigationPath(string levelName)
        {
           // string currentSceneName = EditorApplication.currentScene;
            string sceneName = "";
            //find the index of .unity
            int indexUnity = levelName.IndexOf(".unity");

            //work backwards until the first forward slash(/)
            int startIndex = 0;
            int go = indexUnity;

            while (go >= 0)
            {
                if (levelName[go] == '/')
                {
                    startIndex = go;
                    break;
                }
                go--;
            }

            sceneName = levelName.Substring(startIndex + 1, indexUnity - startIndex - 1);
            return  NavigationPath() + sceneName+".asset" ;
            
        }
        public const string DebugPrefabPath = "Prefabs/debug/DebugObject";

        public const string PlayerPrefabPath = "Prefabs/Player/Player";
    }
}
