﻿ 
using System;
using UnityEngine;

namespace Assets.Scripts.tools
{
    public static class  ExitTheVoidStringFormatting
    {

        public const string DebugFormatTwoParams = "{0,0} {1,-25}";
        public const string DebugFloatFormatTwoParams = "{0,0} {1:0.00}";
        private const string DebugVector2Format = "{0:0.00} {1,20} {2:0.00}";
        /// <summary>
        /// returns yes, no depending on the boolean passed
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string BoolToYesNo(bool value)
        {
            return value ? "Yes" : "No";

        }

        public static string BoolToOnOff(bool value)
        {
            return value ? "On" : "Off";
        }

        public static string Vector2ToString(Vector2 value)
        {
            float x = value.x;
            float y = value.y;
            return String.Format(DebugVector2Format, x, ",",y);
        }
    }
}
