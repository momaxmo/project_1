﻿
using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.common.utilities;
using Assets.Scripts.exceptions;
using Assets.Scripts.filemanagement.config;
using Assets.Scripts.pathfinding.navigation;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.tools
{
    /// <summary>
    /// Static methods that will be used throughout the game
    /// </summary>
    public class ExitTheVoidTools
    {
        [Inject]
        public static IRoutineRunner RoutineRunner { get; set; }
        /// <summary>
        /// fades the image to the target opacity
        /// </summary>
        /// <param name="targetImage"></param>
        /// <param name="duration"></param>
        /// <param name="targetOpacity"></param>
        /// <returns></returns>
        public static IEnumerator FadeImage(Image targetImage, float duration, float targetOpacity)
        {
            if (targetImage == null)
            {
                Debug.Log("target image is null");
                yield break;
            }
            float alpha = targetImage.color.a;

            for (float i = 0; i < 1.0f; i += Time.deltaTime / duration)
            {
                if (targetImage == null)
                {
                    yield break;
                }
                Color newColor = targetImage.color;
                newColor.a = Mathf.SmoothStep(alpha, targetOpacity, i);
                targetImage.color = newColor;
                yield return null;
            }
        }

        /// <summary>
        /// fades the MaskableGraphic object to the target opacity
        /// </summary>
        /// <param name="targetImage"></param>
        /// <param name="duration"></param>
        /// <param name="targetOpacity"></param>
        /// <returns></returns>
        public static IEnumerator FadeUiObject(MaskableGraphic maskableGraphicObj, float duration, float targetOpacity)
        {
            if (maskableGraphicObj == null)
            {
                Debug.Log("target maskableGraphicObj is null");
                yield break;
            }
            float alpha = maskableGraphicObj.color.a;

            for (float i = 0; i < 1.0f; i += Time.deltaTime / duration)
            {
                if (maskableGraphicObj == null)
                {
                    yield break;
                }
                Color newColor = maskableGraphicObj.color;
                newColor.a = Mathf.SmoothStep(alpha, targetOpacity, i);
                maskableGraphicObj.color = newColor;
                yield return null;
            }
        }
        /// <summary>
        /// fades the target object to the target opacity
        /// </summary>
        /// <param name="targetImage"></param>
        /// <param name="duration"></param>
        /// <param name="targetOpacity"></param>
        /// <returns></returns>
        public static IEnumerator FadeUiObject(object uiObj, float duration, float targetOpacity)
        {
            if (uiObj == null)
            {
                Debug.Log("target object is null");
                yield break;
            }
            if (uiObj is MaskableGraphic)
            {
                MaskableGraphic maskableGraphicObj = (MaskableGraphic)uiObj;

                float alpha = maskableGraphicObj.color.a;

                for (float i = 0; i < 1.0f; i += Time.deltaTime / duration)
                {
                    if (maskableGraphicObj == null)
                    {
                        yield break;
                    }
                    Color newColor = maskableGraphicObj.color;
                    newColor.a = Mathf.SmoothStep(alpha, targetOpacity, i);
                    maskableGraphicObj.color = newColor;
                    yield return null;
                }
            }

            else if (uiObj is Button)//try button
            {
                Button button = (Button)uiObj;
                float alpha = button.image.color.a;

                for (float i = 0; i < 1.0f; i += Time.deltaTime / duration)
                {
                    if (button == null)
                    {
                        yield break;
                    }
                    Color newButtonColor = button.image.color;
                    Text text = button.GetComponentInChildren<Text>();
                    Color textColor = text.color;
                    newButtonColor.a = Mathf.SmoothStep(alpha, targetOpacity, i);
                    textColor.a = Mathf.SmoothStep(alpha, targetOpacity, i);

                    button.image.color = newButtonColor;
                    text.color = textColor;
                    yield return null;
                }
            }


        }




        /// <summary>
        /// fades the text to the target opacity
        /// </summary>
        /// <param name="targetImage"></param>
        /// <param name="duration"></param>
        /// <param name="targetOpacity"></param>
        public static IEnumerator FadeText(Text targetText, float duration, float targetOpacity)
        {
            if (targetText == null)
            {
                Debug.Log("target text is null");
                yield break;
            }
            float alpha = targetText.color.a;

            for (float i = 0; i < 1.0f; i += Time.deltaTime / duration)
            {
                if (targetText == null)
                {
                    yield break;
                }
                Color newColor = targetText.color;
                newColor.a = Mathf.SmoothStep(alpha, targetOpacity, i);
                targetText.color = newColor;
                yield return null;
            }
        }
        /// <summary>
        /// Attempts to find a transform in the scene by first checking the scene, then the immediate children and finally a deep search
        /// </summary>
        /// <param name="originalTransform"></param>
        /// <param name="targetToFind"></param>
        /// <returns></returns>
        public static Component FindObjectByName(Component originalTransform, string targetToFind)
        {

            Transform foundTransform = originalTransform.transform.Find(targetToFind);
            if (foundTransform == null)
            {
                foundTransform = originalTransform.transform.FindChild(targetToFind);
                if (foundTransform == null)
                {
                    Transform[] transforms = originalTransform.GetComponentsInChildren<Transform>();
                    foreach (Transform t in transforms)
                    {
                        if (t.name == targetToFind)
                        {
                            foundTransform = t;
                            break;
                        }
                    }
                    if (foundTransform == null)
                    {
                        throw new NoTransformFoundException(
                            "You requested to find a transform " + targetToFind + " that doesn't exist in the scene. " +
                            "Please ensure that it exists");
                    }

                }
            }
            return foundTransform;
        }
        /// <summary>
        /// Disables a gameobject after a set amount of time
        /// </summary>
        /// <param name="go"></param>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static IEnumerator DisableGameObjectAfterSeconds(GameObject go, float seconds)
        {
            yield return new WaitForSeconds(seconds);
            go.SetActive(false);
        }
        /// <summary>
        /// Disables a gameobject after a set amount of time. Checks what type of object the object passed in is
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static IEnumerator DisableGameObjectAfterSeconds(object obj, float seconds)
        {
            yield return new WaitForSeconds(seconds);

            if (obj is GameObject)
            {
                GameObject gameObject = (GameObject)obj;
                gameObject.SetActive(false);
            }
            else if (obj is Array)
            {
                GameObject[] gameObjects = (GameObject[])obj;
                for (int i = 0; i < gameObjects.Length; i++)
                {
                    gameObjects[i].SetActive(false);
                }
            }
            else if (obj is IList)
            {

                List<GameObject> gameObjects = (List<GameObject>)obj;
                for (int i = 0; i < gameObjects.Count; i++)
                {
                    gameObjects[i].SetActive(false);
                }
            }
        }

        /// <summary>
        /// Enables a gameobject after a set amount of time. Checks what type of object the object passed in is
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static IEnumerator EnableGameObjectsAfterNSeconds(object obj, float seconds)
        {
            yield return new WaitForSeconds(seconds);

            if (obj is GameObject)
            {
                GameObject gameObject = (GameObject)obj;
                gameObject.SetActive(true);
            }
            else if (obj is Array)
            {
                GameObject[] gameObjects = (GameObject[])obj;
                for (int i = 0; i < gameObjects.Length; i++)
                {
                    gameObjects[i].SetActive(true);
                }
            }
            else if (obj is IList)
            {

                List<GameObject> gameObjects = (List<GameObject>)obj;
                for (int i = 0; i < gameObjects.Count; i++)
                {
                    gameObjects[i].SetActive(true);
                }
            }
        }



        /// <summary>
        /// Validates the file passed in begins with Application.persistent data path
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool ValidateFileName(string filename)
        {
            bool returner = filename.StartsWith(Application.persistentDataPath + "/");
            if (!returner)
            {
                throw new InvalidExitTheVoidFileName("Filename passed was " + filename + ". This is invalid. Please make sure to start with persistentDataPath");
            }
            return returner;
        }
        public static bool ResourceExist<T>(string path) where T : ScriptableObject
        {
            T t = (T)UnityEngine.Resources.Load(path);
            return t != null;
        }
        /// <summary>
        /// Returns a place holder icon 
        /// </summary>
        /// <returns></returns>
        public static Sprite PlaceholderIconsprite()
        {

            Sprite returner = UnityEngine.Resources.Load<Sprite>("Art/Sprites/Tazo_2D/Icon/PlaceholderIcon"); //todo: check if other loaded sprites are using the png extension
            if (returner == null)
            {
                Debug.Log("placeholder sprite was loaded null");
            }
            return returner;
        }
        /// <summary>
        /// used in the booster inventory as a check if a button is below its grandparents transform
        /// todo: add a condition that only this use case is allowed
        /// </summary>
        /// <param name="button"></param>
        /// <param name="rectTransform"></param>
        /// <returns></returns>
        public static bool ButtonBelowRectTransform(Button button, BoosterInventoryLayoutGroup LayoutGroup)
        {
            float bottomYPos = button.transform.position.y - (LayoutGroup.LayoutGroup.cellSize.y / 2) - LayoutGroup.LayoutGroup.padding.bottom -
                          LayoutGroup.LayoutGroup.spacing.y;
            float currentTransformYPos = LayoutGroup.ObjectTransform.transform.position.y - (LayoutGroup.RecttTransform.rect.height / 2);
            return bottomYPos < currentTransformYPos;
        }
        /// <summary>
        /// Checks if a string contains a keyword(toCheck) and ignoring its case
        /// </summary>
        /// <param name="source"></param>
        /// <param name="toCheck"></param>
        /// <returns></returns>
        public static bool StringIgnoreCaseContains(string source, string toCheck)
        {
            return source.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0;
        }


    }

    public struct BoosterInventoryLayoutGroup
    {
        [SerializeField]
        public GridLayoutGroup LayoutGroup;
        [SerializeField]
        public Transform ObjectTransform;
        [SerializeField]
        public RectTransform RecttTransform;

        public BoosterInventoryLayoutGroup(GridLayoutGroup layoutGroup, Transform objectTransform,
            RectTransform transform)
        {
            LayoutGroup = layoutGroup;

            ObjectTransform = objectTransform;

            RecttTransform = transform;

        }

        
    }


}
