﻿ 
using System;
using UnityEngine; 
namespace Assets.Scripts.tools
{
    /// <summary>
    /// Algorithm from http://stackoverflow.com/questions/87304/calculating-frames-per-second-in-a-game/7796547#7796547
    /// </summary>
    public class Fps : MonoBehaviour
    {
        private int _numberOfRenderedFrames;
        private float _timeStart; 
        private int _fps;
 
        void Start()
        { 
            _timeStart = Time.time;
        }

        void Update()
        {
            _numberOfRenderedFrames ++;
            if (_numberOfRenderedFrames == Int32.MaxValue  ) //prevent overflow
            {
                _numberOfRenderedFrames--;
            }
            float divisor = Time.time - _timeStart;
            if (divisor != 0)
            {
                _fps = (int)(_numberOfRenderedFrames/divisor); 
            }
        }

        public float AverageTimeBetweenFrames()
        {
            return 1f/_fps;
        }

    }
}
