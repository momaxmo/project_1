﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text; 
using Assets.Scripts.game.boosters.model;
using Assets.Scripts.game.inventory.Model; 
using Assets.Scripts.game.model;
using LitJson;
using UnityEngine;

namespace Assets.Scripts.tools.Json
{
    /// <summary>
    /// this converts booster data into a json file 
    /// </summary>
    public class ExitTheVoidJsonConverterWriter
    {

        private JsonWriter _writer;
        private StringBuilder _stringBuilder;
        public ExitTheVoidJsonConverterWriter()
        {

            Init();
        }

        private void Init()
        {
            _stringBuilder = new StringBuilder();
            _writer = new JsonWriter(_stringBuilder);
            _writer.PrettyPrint = true;
            _writer.IndentValue = 0;
        }

        public void ConvertBoosterModelToJson(IInventoryItem model)
        {
            JsonMapper.ToJson(model, _writer);

            ClearLocalStringBuilder();
        }

        /// <summary>
        /// Pass in a dictionary of booster models and if the flag is set to true, then create a file with the specified path file
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="filepath"></param>
        /// <param name="writeToFile"></param>
        public void ConvertBoosterModelToJson(Dictionary<string, ConcreteBoosterModel> dictionary, string filepath, bool writeToFile)
        {
            Init();
            JsonMapper.ToJson(dictionary, _writer);
            //save to file
            using (StreamWriter outfile = new StreamWriter(filepath))
            {
                outfile.Write(_stringBuilder.ToString());
            }
            ClearLocalStringBuilder();
        }
        /// <summary>
        /// Pass in an inventory to save as a json file 
        /// </summary>
        /// <param name="inventory"></param>
        /// <param name="filepath"></param>
        /// <param name="writeToFile"></param>
        public void SaveInventory(Inventory inventory, string filepath, bool writeToFile)
        {
            Init();
            JsonMapper.ToJson(inventory, _writer);
            //save to file
            using (StreamWriter outfile = new StreamWriter(filepath))
            {
                outfile.Write(_stringBuilder.ToString());
            }
            ClearLocalStringBuilder();
        }

        public Inventory LoadInventory(  string suffix)
        {
            string json = "";
            string path = ExitTheVoidFilePathRefs.InventoryDbPath(suffix); 
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    json = sr.ReadToEnd();

                }
            }
            catch (Exception e)
            {
                Debug.Log("Problem loading level file, given path was: " + path);
                Debug.Log(e.StackTrace);
            }
          return JsonMapper.ToObject<Inventory>(json); 
        }
        private void ClearLocalStringBuilder()
        {
            _stringBuilder.Length = 0;
            _stringBuilder.Capacity = 0;
        }

        public void ConvertLevelStateToJson(LevelStateModel model, string filepath, bool writeToFile)
        {
            Init();
            JsonMapper.ToJson(model, _writer);
            //save to file
            if (writeToFile)
            {
                using (StreamWriter outfile = new StreamWriter(filepath))
                {
                    outfile.Write(_stringBuilder.ToString());
                }
            }
            ClearLocalStringBuilder();
        }
        /// <summary>
        /// from a json file, converts it to a string and returns a gamestatemodel
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        internal LevelStateModel LoadJsonIntoGameStateModel(string path)
        {
            string json = "";
            // ReSharper disable once RedundantAssignment
            LevelStateModel model = new LevelStateModel();
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    json = sr.ReadToEnd();

                }
            }
            catch (Exception e)
            {
                Debug.Log("Problem loading level file, given path was: " + path);
                Debug.Log(e.StackTrace);
            }
            model = JsonMapper.ToObject<LevelStateModel>(json);
            return model;
        }

        /// <summary>
        /// returns a dictionary from the provided path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        internal Dictionary<string, ConcreteBoosterModel> LoadJsonIntoDictionary(string path)
        {
            string json = "";
            // ReSharper disable once RedundantAssignment
            Dictionary<string, ConcreteBoosterModel> returnDictionary = new Dictionary<string, ConcreteBoosterModel>();
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    json = sr.ReadToEnd();

                }
            }
            catch (Exception e)
            {
                Debug.Log("file problem, given path was: " + path);
                Debug.Log(e.StackTrace);

            }
            returnDictionary = JsonMapper.ToObject<Dictionary<string, ConcreteBoosterModel>>(json);
            return returnDictionary;

        }
    }
}
