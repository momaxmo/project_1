﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.tools.MainMenuTools
{
    public class CanvasRepositioning: MonoBehaviour
    {
        public Camera UiCamera;
        public  Vector3 InitialPosition;
        private RectTransform _thisObjectsTransform;
        

        private string logger;
        private void Start()
        {

            _thisObjectsTransform = GetComponent<RectTransform>();
            InitialPosition = _thisObjectsTransform.position;
            float h = _thisObjectsTransform.rect.height;
            float halfFieldOfView = UiCamera.fieldOfView/2;
            halfFieldOfView *= Mathf.Deg2Rad;
         
           
            float newZpos = (h/2)/((Mathf.Tan(halfFieldOfView)));
         
            newZpos += UiCamera.transform.position.z;
            Vector3 newPos = InitialPosition;
            newPos.z = newZpos;
            _thisObjectsTransform.position = newPos;
         
         

        }
    }
}
