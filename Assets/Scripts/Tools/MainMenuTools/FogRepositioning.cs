﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Tools.MainMenuTools
{
    /// <summary>
    /// This class's purpose is to reposition the fog effect to the bottom of the screen
    /// </summary>
   public  class FogRepositioning: MonoBehaviour
   {
       public Vector3 m_viewPortPosition;
        public Camera cam;
         void Start()
        {
            Vector3 reposition = cam.ViewportToWorldPoint(m_viewPortPosition);
            
            transform.position = reposition;
        }


   }
}
