﻿ 
using UnityEngine;

namespace Assets.Scripts.tools
{
    public static class MathTools
    {
        /// <summary>
        /// returns true if the value is between a miniRange and maxiRange excluding the mini and maxrange
        /// </summary>
        /// <param name="miniRange"></param>
        /// <param name="maxiRange"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ExclusiveRange(float miniRange, float maxiRange, float value)
        {
            return value < maxiRange && value > miniRange;
        }
        /// <summary>
        /// returns true if the value is between a miniRange and maxiRange including the mini and maxrange
        /// </summary>
        /// <param name="miniRange"></param>
        /// <param name="maxiRange"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool InclusiveRange(float miniRange, float maxiRange, float value)
        {

            return value <= maxiRange && value >= miniRange;
        }

        public static Collider2D TrajectoryRayCast(Bounds b, int iteration,float gravity, Vector2 originalPos,  
            Vector2 velocity, float acceleration, float timeStep, LayerMask collidableLayerMask, out bool isValidGroundHit)
        {
            Collider2D col = null;
            // Vector2 startPos = originalPos;
            Vector2 lowerStartPos = b.min;
            Vector2 upperStartPos = b.max;
            if (Mathf.Sign(velocity.x) > 0)
            {
                lowerStartPos.x = b.max.x;
            }
            else
            {
                upperStartPos.x = b.min.x;
            }
            for (int i = 0; i < iteration; i++)
            {
                Vector2 newLowerPosition = Vector2.zero;
                Vector2 newUpperPosition = Vector2.zero;

                newLowerPosition.x = lowerStartPos.x + velocity.x * timeStep;
                newLowerPosition.y = lowerStartPos.y + velocity.y * timeStep + (gravity * timeStep * timeStep * 0.5f);

                newUpperPosition.x = upperStartPos.x + velocity.x * timeStep;
                newUpperPosition.y = upperStartPos.y + velocity.y * timeStep + (gravity * timeStep * timeStep * 0.5f);


                velocity.y = velocity.y + gravity * timeStep;
                Vector2 lowerDir = newLowerPosition - lowerStartPos;
                Vector2 upperDir = newUpperPosition - upperStartPos;

                float lowerDistance = lowerDir.magnitude;
                float upperDistance = upperDir.magnitude;

                RaycastHit2D lowerRay = Physics2D.Raycast(lowerStartPos, lowerDir, lowerDistance, collidableLayerMask);
                RaycastHit2D upperRay = Physics2D.Raycast(upperStartPos, upperDir, upperDistance, collidableLayerMask);

              //  Debug.DrawLine(lowerStartPos, newLowerPosition, Color.yellow);
              //  Debug.DrawLine(upperStartPos, newUpperPosition, Color.yellow);
                lowerStartPos = newLowerPosition;
                upperStartPos = newUpperPosition;
                if (upperRay.collider != null)
                {
                    col = upperRay.collider;
                    isValidGroundHit = false;
                    break;
                }
                if (lowerRay.collider != null)
                {
                    col = lowerRay.collider;
                    isValidGroundHit = false;
                    //check if the ray hit on descent
                    if (velocity.y <= 0)
                    {
                        isValidGroundHit = true;
                    }
                    break;
                }
              

            }

            isValidGroundHit = false;
            return col;
        }
        public static void DebugDrawTrajectory(Bounds b,int iteration, float gravity, Vector2 originalPos ,
            Vector2 velocity, float acceleration, float timeStep, LayerMask collidableLayerMask)
        {
            Collider2D col = null; 
           // Vector2 startPos = originalPos;
            Vector2 lowerStartPos = b.min;
            Vector2 upperStartPos = b.max;
            if (Mathf.Sign(velocity.x) > 0)
            {
                lowerStartPos.x = b.max.x;
            }
            else
            {
                upperStartPos.x = b.min.x;
            }
            for (int i = 0; i < iteration; i++)
            {
                Vector2 newLowerPosition = Vector2.zero;
                Vector2 newUpperPosition = Vector2.zero;

                newLowerPosition.x = lowerStartPos.x + velocity.x * timeStep ;
                newLowerPosition.y = lowerStartPos.y + velocity.y * timeStep   + (gravity * timeStep * timeStep*0.5f)  ;

                newUpperPosition.x = upperStartPos.x + velocity.x * timeStep;
                newUpperPosition.y = upperStartPos.y + velocity.y * timeStep + (gravity * timeStep * timeStep * 0.5f);


                velocity.y = velocity.y + gravity*timeStep;
                Vector2 lowerDir = newLowerPosition - lowerStartPos;
                Vector2 upperDir = newUpperPosition - upperStartPos;

                float lowerDistance = lowerDir.magnitude;
                float upperDistance = upperDir.magnitude;

                RaycastHit2D lowerRay = Physics2D.Raycast(lowerStartPos,lowerDir,lowerDistance,collidableLayerMask);
                RaycastHit2D upperRay = Physics2D.Raycast(upperStartPos,upperDir,upperDistance,collidableLayerMask);

                Debug.DrawLine(lowerStartPos,newLowerPosition,Color.yellow);
                Debug.DrawLine(upperStartPos,newUpperPosition,Color.yellow);
                lowerStartPos = newLowerPosition;
                upperStartPos = newUpperPosition;
                if (lowerRay.collider != null)
                {
                    col = lowerRay.collider;
                    break;
                }
                 if (upperRay.collider != null)
                 {
                     col = upperRay.collider;
                     break;
                }

            }

           
        }


    }
}
