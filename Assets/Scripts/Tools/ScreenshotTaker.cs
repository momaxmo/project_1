﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;

public class ScreenshotTaker : MonoBehaviour {

    public bool StartTakingImages;
    int count = 0;
    public int framesTo = 10;
    int countFrames;
    public float maxTimer = 5;
    float timer;
    string path = "";

    void Update()
    {
        if (StartTakingImages)
        {
            if (path.Equals(""))
            {
                path = EditorUtility.SaveFilePanel("Save Screen Shot Folder", Application.dataPath, "", "");
                if (path.Equals(""))
                {
                    StartTakingImages = false;

                }
            }
            timer += Time.deltaTime;
            countFrames++;

            if (countFrames > framesTo)
            {
                StartCoroutine(CaptureScreenShot());
                countFrames = 0;
                Debug.Log("Taking pix");
            }
            if (timer > maxTimer)
            {
                timer = 0;
                StartTakingImages = false;
            }
        }
    }

    IEnumerator CaptureScreenShot()
    {

        yield return new WaitForEndOfFrame();
        Texture2D texture =  new Texture2D(Screen.width, Screen.height, TextureFormat.ARGB32, false);
         
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height),0,0);
        texture.Apply();
        yield return 0;
        //store bytes inside an array
        byte[] bytes = texture.EncodeToPNG();
        File.WriteAllBytes(path + count + ".png", bytes); 
        count++;
        DestroyObject(texture);
    }
}
