﻿ 

namespace Assets.Scripts.tools

{
    internal class VariousGameStateEnums
    {
    }

    internal enum GameElement
    {
        MainPlayer,GameField,
        EnemyPool,MainCamera,LevelLimits
    }
    internal enum GameMusicState
    {
        MainMenu = 0 , 
        InGame, 
       
        LoadLevel,
        GameOver
    }

    

    public  enum GameState
    {
        Paused,
        MainMenu,
        Unpaused
    }

    internal enum GameSettings
    {
        MusicVolume,
        FxVolume
    }

    internal enum ControllerViewType
    {
        Player,
        NPC
    }

    internal enum HitTypes
    {
        Heavy,
        Strong
    }

    internal enum BoosterType
    {
        Light,
        Dark
    }
}
