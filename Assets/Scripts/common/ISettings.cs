﻿ 
namespace Assets.Scripts.common
{
    public interface ISettings<T>
    {
       T Value { get; }
        void SetValue(T value);
    }
}
