﻿

using Assets.Scripts.common.utilities;
using Assets.Scripts.game.input.controller;  
using strange.extensions.context.impl;
using UnityEngine;

namespace Assets.Scripts.common.config
{
    /// <summary>
    /// Place bindings here that would be common for all context 
    /// </summary>
    public class GameContext : MVCSContext
    {
        public GameContext(MonoBehaviour contextView)
            : base(contextView)
        {
        }

        protected override void mapBindings()
        {
            //We need to call mapBindings up the inheritance chain (see SignalContext)
            base.mapBindings();
/*

            Debug.Log("adding gamestatemodel to the injection binder. For now its a default state but as you build the game you should change this, inside " + GetType());
            LevelStateModel gameModel = new LevelStateModel() { IsPaused = false };
            injectionBinder.Bind<LevelStateModel>().ToValue(gameModel).ToSingleton().CrossContext();
*/


            //inputs
            injectionBinder.Bind<IRoutineRunner>().To<RoutineRunner>().CrossContext();
            injectionBinder.Bind<IInput>().To<KeyboardInput>().ToSingleton().CrossContext();
       

        }
    }
}
