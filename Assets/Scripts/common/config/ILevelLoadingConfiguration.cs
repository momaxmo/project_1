﻿
namespace Assets.Scripts.common.config
{
   public  interface ILevelLoadingConfiguration
    {
       string LevelName { get; set; }
    }
}
