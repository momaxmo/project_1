﻿ 
using Assets.Scripts.game.player.signals;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
 
using UnityEngine;

namespace Assets.Scripts.common.config
{

    /// <summary>
    /// Strange recommends that we use a MVCS(model-MenuView-controller-service) architecture 
    /// This class will bind signals to binders
    /// </summary>
    public class SignalContext : MVCSContext
    {
        private bool _hasSearched = false;
        private bool _implementedInput;
        public SignalContext(MonoBehaviour contextView)
            : base(contextView)
        {
        }



        protected override void addCoreComponents()
        {
            base.addCoreComponents();
            injectionBinder.Unbind<ICommandBinder>();
            injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
            

        }

        public override void Launch()
        {
            base.Launch();
            StartSignal signal = injectionBinder.GetInstance<StartSignal>();
            signal.Dispatch(); 
        }

        protected override void postBindings()
        {
           
        }

        protected override void mapBindings()
        {
            base.mapBindings();

            // injectionBinder.Bind<KeyboardInput>().ToSingleton().CrossContext(); 
         
 
            //check if input has already been mapped in the injection binder
           
           
            /*if (injectionBinder.GetInstance<IInput>() == null)
            {
                Debug.Log("injectionBinder.GetInstance<IInput>() == null in "+GetType());

                injectionBinder.Bind<IInput>().To<KeyboardInput>().ToSingleton().CrossContext();
            }*/
            

        }
        
    }
}
