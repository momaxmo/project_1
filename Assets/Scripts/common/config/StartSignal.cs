﻿ 
 
using Assets.Scripts.game.inventory.Model;
using Assets.Scripts.game.inventory.view;
using Assets.Scripts.game.view.environment;
using Assets.Scripts.ui.model;
using Assets.Scripts.ui.view.pausemenu;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Assets.Scripts.common.config
{
    /// <summary>
    /// this file contains all signals dispatched between contexts
    /// </summary>
    public class StartSignal : Signal
    {
    }

    /// <summary>
    /// used for input
    /// </summary>
  

    public class PlayerVelocityUpdatedSignal : Signal<Vector2>
    {

    }

    public class   UpdateCameraFollowTarget : Signal<Transform>
    {

    }

    public class UpdateLevelBoundsSignal : Signal<LevelBounds>
    {
    }

    public class LevelStartSignal : Signal
    {
    }

    public class LevelEndSignal : Signal
    {
    }

    public class WeaponPickedUpSignal : Signal
    {
    }

    public class MusicVolumeUpdatedSignal<T> : Signal<T>
    {
    }


    public class GameStartSignal : Signal
    {
    }

    public class LoadNextLevelSignal : Signal
    {
    }

    public class GameExitSignal : Signal
    {
    }

    /// <summary>
    /// Listeners to this ensure the game state when paused/resumed are respected. 
    /// </summary>
    public class GamePausedSignal : Signal<bool>
    {
    }

    /// <summary>
    /// Dispatch this signal if the state of the game is currently unpausable.
    /// Can be used to show a warning sign if the PlayerModel tries to unpause certain key   events
    /// </summary>
    public class UnpausableStateSignal : Signal 
    {
        
    }

    /// <summary>
    /// Signals that a pause command has been issued
    /// </summary>
    public class GamePauseCommandSignal : Signal
    {
    }

    public class UpdateHealthSignal : Signal<int, int>
    {
    }

    public class ResetMenuSignal : Signal
    {
    }
    #region ui
    /// <summary>
    /// Signal the booster switch modal to enable, the iterable button holds the position of the button that the modal needs to hover over, the
    /// inventory item is the inventory item that the PlayerModel wishes to switch to 
    /// </summary>
    public class EnableBoosterModalSwitchView : Signal<IterableButton, IInventoryItem >
    {
        
    }
    /// <summary>
    /// initiates a signal to transition from the regular pause view to the character sheet view
    /// </summary>
    public class SwitchToCharacterSheetSignal : Signal<bool> 
    {
    }
    /// <summary>
    /// Tells the other pause menu views to fade if they are enabled 
    /// </summary>
    public class FadeViewSignal : Signal
    {
        
    }
    /// <summary>
    /// Dispatches a signal to switch between different views dependent on the PauseMenuState
    /// </summary>
    public class PauseMenuSignal : Signal<PauseMenuState>
    {
        
    }
    /// <summary>
    /// Dispatches a signal to the input manager controller which in turn will dispatch different signals according to the int payload
    /// </summary>
    public class InputSignal : Signal<int> { }
    /// <summary>
    /// signal to disable grid buttons
    /// </summary>
    public class DisableGridButtonSignal : Signal
    {
    }
    /// <summary>
    /// signal to enable buttons
    /// </summary>
    public class EnableGridButton : Signal
    {
    }

    /// <summary>
    /// Display the pause menu interface
    /// </summary>
    public class SwitchToPauseMenuInterface : Signal<bool>
    { }
 
    public class SwitchToBoosterViewSignal: Signal <bool>
    { }
    public class ShowPauseButtonsSignal: Signal<bool>
    { }

    public class EnableGridInteractivity : Signal 
    {
        
    }

    public class ShowExitGameDialogueSignal : Signal<bool>
    {

    }
    public class ShowExitToMainMenuDialogueSignal : Signal<bool>
    {

    }

  

    public class SwitchToSettingsAndOptionsSignal: Signal<bool>
    { }
    #endregion
    #region NPC signals
    /// <summary>
    /// Signal to send whenever an enemy is hit. The gameobject is the discriminator enemy and int is the value that 
    /// the enemy was hit by, float is the chance that the enemy is hit
    /// </summary>
    public class HurtEnemySignal : Signal<StrikeEnemy>
    { }

    /// <summary>
    /// dispatch this signal on melee hit
    /// </summary>
    public class MeleeHitSignal : Signal<GameObject> { }

    /// <summary>
    /// Signals the meleeview to start melee'ing
    /// </summary>
    public class StartMeleeHitSignal : Signal { }
    #endregion
    #region Inventory
    public class BuildInventory : Signal<Inventory>
    {
        
    }

    public class AddButtonForInventory : Signal<IInventoryItem>
    {
    }
    /// <summary>
    /// signals to add a new inventory item with the string as a key
    /// </summary>
    public class AddNewInventoryItemSignal : Signal<string>
    {
    }

    #endregion

    #region file saving and loading signals

    public class CreateNewPlayerFileSignal : Signal<string>
    {
    }

    public class LoadPlayerFileSignal : Signal<string>
    {
    }

    public class LoadLevelByNameSignal : Signal<string>
    {
    }

    public class LoadBoosterDataSignal : Signal
    {
    }

    public class LoadInventoryData : Signal
    {
        
    }

    public class SaveInventoryData : Signal
    {
        
    }

    #endregion

    #region booster 

    /// <summary>
    /// Switches boosters with params boostermodel(new booster) , the booster slot id,  and the initiating button(holding an old booster model)
    /// </summary>
    public class SwitchBoosterSignal : Signal<IInventoryItem, int,  IterableButton>
    {
        
    }
  
    /// <summary>
    /// updates the boosters of the interested listeners.
    /// </summary>
    public class UpdateBooster : Signal<IInventoryItem, int>
    {
    }

    public class ChangeBoosterSignal : Signal<IInventoryItem>
    {
    }

    public class EquipBoosterSignal : Signal<IInventoryItem>
    {
    }


    /// <summary>
    /// Signal to turn on the booster modal window. It payload is the booster model and the Iterable button that initiated the signal
    /// </summary>
    public class TurnOnBoosterModalSwitch : Signal<IInventoryItem, IterableButton,InventorySwitchingModalSelector> { }

    #endregion
    /// <summary>
    /// Structure to hold the enemy's game object, the power of the melee hit, the level of the PlayerModel and the accuracy of the hit
    /// </summary>
    public struct StrikeEnemy
    {
        public GameObject EnemyGameObject;
        public int AttackPowerHit;
        public int PlayerLevel;
        public float Accuracy;
        public float ChanceToStun;

        public StrikeEnemy(GameObject enemy, int ap, int level, float accuracy, float chanceToStun)
        {
            EnemyGameObject = enemy;
            AttackPowerHit = ap;
            PlayerLevel = level;
            Accuracy = accuracy;
            ChanceToStun = chanceToStun;
        }
    }
}
