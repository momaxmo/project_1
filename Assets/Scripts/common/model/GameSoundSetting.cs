﻿ 
namespace Assets.Scripts.common.model
{
  public  class GameSoundSetting<T>: ISettings<T>
  {
      private T _value;
      public T Value
      {
          get { return _value; }
      }

      public void SetValue(T value)
      {
          _value = value; 
      }
  }
}
