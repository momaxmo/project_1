﻿

using System.Collections;
using UnityEngine;

namespace Assets.Scripts.common.utilities
{
    public interface IRoutineRunner
    {
        Coroutine StartCoroutine(IEnumerator method);
    }
}
