﻿using System; 
using strange.extensions.context.api;
using strange.extensions.injector.api; 
using UnityEngine;

namespace Assets.Scripts.common.utilities
{
    /// <summary>
    /// I did not write this class but I am rewriting word for word to understand the logic behind it.
    /// The main idea of this class is to allow something that is usually reserved for monobehaviours to be 
    /// available for the rest of the project. In this case : routines
    /// [Implements(typeof(IRoutineRunner), InjectionBindingScope.CROSS_CONTEXT)] 
    /// this tells us that we are implicitly binding IRoutineRunner accross context
    /// we need to ensure that implicitBinder.ScanForAnnotatedClassesis called in the mapBindings method
    /// 
    /// </summary>
   [Implements(typeof(IRoutineRunner), InjectionBindingScope.CROSS_CONTEXT)] 
    public class RoutineRunner: IRoutineRunner
    {
       [Inject(ContextKeys.CONTEXT_VIEW)] 
       public GameObject InjectedContextView { get; set; }

       private RoutineRunnerBehaviour _monoBehaviour;


       [PostConstruct]
       public void PostConstruct()
       {
           //add the RoutineRunnerBehaviour to the gameobject
           _monoBehaviour = InjectedContextView.AddComponent<RoutineRunnerBehaviour>();
       }

        public  Coroutine StartCoroutine(System.Collections.IEnumerator method)
        {
            return _monoBehaviour.StartCoroutine(method);
        }

    }
   public class RoutineRunnerBehaviour : MonoBehaviour
   {
   }
}
