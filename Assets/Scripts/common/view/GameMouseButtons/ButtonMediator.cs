﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using strange.extensions.mediation.impl;
using UnityEngine.UI;

namespace Assets.Scripts.common.view.GameMouseButtons
{
    public class ButtonMediator : Mediator
    {
        [Inject]
        public ButtonView view { get; 
            set; }




        public override void OnRegister()
        {
            UnityEngine.Debug.Log("in button mediator on register");
            //listen to MenuView for a local signal
            view.PressSignal.AddListener(ButtonAction);
        }

        public override void OnRemove()
        {
            view.PressSignal.RemoveListener(ButtonAction);
        }

        private void ButtonAction()
        {
            UnityEngine.Debug.Log("Button action in mediator: not entirely sure what to do with this ");
        }
    }
}

