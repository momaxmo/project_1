﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;

namespace Assets.Scripts.common.view.GameMouseButtons
{
    public class ButtonView: View
    {
        public Signal PressSignal = new Signal();
        public UnityEngine.UI.Button MouseButton;
        public UnityEngine.UI.Text ButtonText;
        public string Label;

        protected override void Start()
        {
            base.Start();
            if (ButtonText == null)
            {
                UnityEngine.UI.Text textField = GetComponentInChildren<UnityEngine.UI.Text>();
                ButtonText = textField;
            }
            if (ButtonText.text == null)
            {
                ButtonText.text = Label;
            }
            MouseButton = GetComponent<UnityEngine.UI.Button>();
            MouseButton.onClick.RemoveAllListeners();
            MouseButton.onClick.AddListener(OnButtonPress);
        }


        public void OnButtonPress()
        {
            UnityEngine.Debug.Log("Mouse button clicked");
            PressSignal.Dispatch();

        }
    }
}
