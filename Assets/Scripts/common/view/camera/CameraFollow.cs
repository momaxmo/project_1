﻿
using System.Collections;
using Assets.Scripts.game.player.model;
using Assets.Scripts.game.view.environment;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.common.view.camera
{
    public class CameraFollow : View
    {

        #region injectors

        #endregion
        #region fields

        private float _shakIntensity, _shakeDuration, _shakeDecay;
        private Vector3 _lookDirectionModifier;
        private float _currentZoom;
        public bool FollowPlayer;
        private Camera _thisCamera;
        [SerializeField]
        public float VerticalLookDistance;
        [SerializeField]
        public float MinimumZoom;
        [SerializeField]
        public float MaximumZoom;
        [SerializeField]
        private Transform _target;

        [SerializeField] public float LookAheadTrigger;
        private Vector3 _lookAheadPosition;
        [SerializeField] public float HorizontalLookDistance;
        private Vector2 _targetPlayerVelocity;
        private LevelBounds _levelLimits;
        private bool _levelLimitAndTargetUpdated = false;
        private float _offsetZ;
        private Vector3 _lastTargetPosition;
        [SerializeField]
        public float ResetSpeed;

        [SerializeField] private Vector3 _currentVelocity;
        [SerializeField] public float CameraSpeed;
        private float _xMin, _xMax, _yMin, _yMax;
        public float ZoomSpeed;
        #endregion
        #region properties
        [Inject]
        public IPlayerModel MainPlayer { get; set; }
        public Camera ThisCamera
        {
            get
            {
                if (_thisCamera == null)
                {
                    _thisCamera = GetComponent<Camera>();
                }
                return _thisCamera;
            }

        }
        #endregion
        public void Init()
        {
            _currentZoom = MinimumZoom;
            FollowPlayer = true;
            StartCoroutine(WaitForLevelBoundsAndTargetUpdate());

        }

        /// <summary>
        /// This method shakes the camera
        /// </summary>
        /// <param name="shakeIntensity">how intense is the camera shake?</param>
        /// <param name="shakeDuration">how long does the shaking last for?</param>
        /// <param name="shakeDecay">how quickly does the camera stabilize after shaking?</param>
        public void Shake(float shakeIntensity, float shakeDuration, float shakeDecay)
        {
            _shakIntensity = shakeIntensity;
            _shakeDuration = shakeDuration;
            _shakeDecay = shakeDecay;
        }

        /// <summary>
        /// Move the camera up
        /// </summary>
        public void LookUp()
        {
            _lookDirectionModifier = new Vector3(0, VerticalLookDistance, 0);
        }
        /// <summary>
        /// Move the camera down
        /// </summary>
        public void LookDown()
        {
            _lookDirectionModifier = new Vector3(0, -VerticalLookDistance, 0);
        }

        public void ResetLookUpDown()
        {
            _lookDirectionModifier = new Vector3(0, 0, 0);
        }

        public void UpdateSpeed(Vector2 s)
        {
            _targetPlayerVelocity = s;
        }

        public void UpdateTarget(Transform t)
        {
            _target = t;
        }

        public void UpdateLevelBounds(LevelBounds levelLimit)
        {
            _levelLimits = levelLimit;
        }
        /// <summary>
        /// Waits until the Levelbounds and target parameters are set. 
        /// </summary>
        /// <returns></returns>
        private IEnumerator WaitForLevelBoundsAndTargetUpdate()
        {
            
            while (_levelLimits == null || _target == null)
            {
                yield return null;
        
            }
            FollowPlayer = true;
            _lastTargetPosition = _target.position;
            _offsetZ = (transform.position - _target.position).z;
            transform.parent = null;
            _levelLimitAndTargetUpdated=true;
            Zoom();
        }

        private void Zoom()
        {
            float characterSpeed = Mathf.Abs(MainPlayer.PlayerControllerView.Velocity.x);
            float currentSpeed = 0f;
            _currentZoom = Mathf.SmoothDamp(_currentZoom, (characterSpeed/10)*(MaximumZoom - MinimumZoom) + MinimumZoom,
                ref currentSpeed, ZoomSpeed);
            ThisCamera.orthographicSize = _currentZoom;
            GetLevelLimits();
        }

        void GetLevelLimits()
        {
            float camHeight = ThisCamera.orthographicSize*2f;
            float camWidth = camHeight*ThisCamera.aspect;

            _xMin = _levelLimits.LeftLimit + (camWidth/2);
            _xMax = _levelLimits.RightLimit - (camWidth / 2);
            _yMin = _levelLimits.BottomLimit + (camHeight / 2);
            _yMax = _levelLimits.UpperLimit - (camHeight / 2);
        }
        void LateUpdate()
        {
            if (!_levelLimitAndTargetUpdated || !FollowPlayer)
            {
                return;
            }
            Zoom();
            float deltaX = (_target.position - _lastTargetPosition).x;
            bool lookAheadOfTarget = Mathf.Abs(deltaX) > LookAheadTrigger;
            if (lookAheadOfTarget)
            {
                _lookAheadPosition = HorizontalLookDistance*Vector3.right*Mathf.Sign(deltaX);
            }
            else
            {
                _lookAheadPosition = Vector3.MoveTowards(_lookAheadPosition, Vector3.zero, Time.deltaTime*ResetSpeed);
            }
            Vector3 aheadTargetPos = _target.position + _lookAheadPosition + Vector3.forward*_offsetZ +
                                     _lookDirectionModifier;
            Vector3 newCamPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref _currentVelocity, CameraSpeed);

            Vector3 shakeFactorPosition = Vector3.zero;
            if (_shakeDuration > 0)
            {
                shakeFactorPosition = Random.insideUnitSphere*_shakIntensity*_shakeDuration;
                _shakeDuration -= _shakeDecay*Time.deltaTime;
            }
            newCamPos = shakeFactorPosition + newCamPos;

            //clamp to level bounds

            float posX = Mathf.Clamp(newCamPos.x, _xMin, _xMax);
            float posY = Mathf.Clamp(newCamPos.y, _yMin, _yMax);
            float posZ = newCamPos.z;

            transform.position = new Vector3(posX,posY,posZ);
            _lastTargetPosition = _target.position;
        }

    }
}
