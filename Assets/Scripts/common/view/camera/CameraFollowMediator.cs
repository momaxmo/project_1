﻿ 
using Assets.Scripts.common.config;
using Assets.Scripts.game.view.environment;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.common.view.camera
{
    public class CameraFollowMediator: Mediator
    {
        [Inject]
        public CameraFollow FollowCamera { get; set; }
/*        [Inject]
        public PlayerVelocityUpdatedSignal UpdateSignal { get; set; }*/
        [Inject]
        public UpdateCameraFollowTarget CameraUpdateTargetSignal { get; set; }
        [Inject]
        public UpdateLevelBoundsSignal LevelBoundsSignalUpdateSignal { get; set; }
        [Inject]
        public GamePausedSignal PauseSignal { get; set; }
       
      
        public override void OnRegister()
        { 
            PauseSignal.AddListener(Pause);
            CameraUpdateTargetSignal.AddListener(UpdateTargetToCamera);
            //UpdateSignal.AddListener(UpdateVelocityToCamera);
            LevelBoundsSignalUpdateSignal.AddListener(UpdateLevelBound);
            FollowCamera.Init();
        }

        public override void OnRemove()
        {
            PauseSignal.RemoveListener(Pause);
           // UpdateSignal.RemoveListener(UpdateVelocityToCamera);
            CameraUpdateTargetSignal.RemoveListener(UpdateTargetToCamera);
            LevelBoundsSignalUpdateSignal.AddListener(UpdateLevelBound);
        }

 /*       private void UpdateVelocityToCamera(Vector2 vel)
        {
            FollowCamera.UpdateSpeed(vel); 
        }*/

        private void UpdateTargetToCamera(Transform newTarget)
        {
            FollowCamera.UpdateTarget(newTarget);
           
        }

        private void UpdateLevelBound(LevelBounds levelLimits)
        {
            FollowCamera.UpdateLevelBounds(levelLimits);
        }

        /// <summary>
        /// Disables the Camera follow script. 
        /// The parameter passed in is a pause flag. enabled should be opposite of this parameter
        /// </summary>
        /// <param name="pause"></param>
        private void Pause(bool pause)
        {
            FollowCamera.enabled = !pause;
        }
    }
}
