﻿ 

using System;
using UnityEngine;

namespace Assets.Scripts.exceptions
{
    public class NoObjectAttachedException : Exception
    {
         

        public NoObjectAttachedException(string msg):base(msg)
        {
            
        }

        public NoObjectAttachedException(Transform t, System.Object notAttachedObject,System.Object obj, GameObject name)
            :this(notAttachedObject.GetType() + " is not attached to the " + name + "gameobject in its " + obj.GetType())
        {
            
            
        }
    }

    public class NoTransformFoundException : Exception
    {
        public NoTransformFoundException(string msg)
            : base(msg)
        {
            
        }
    }
}
