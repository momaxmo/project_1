﻿ 

using Assets.Scripts.filemanagement.config;
using strange.extensions.context.impl;

namespace Assets.Scripts.filemanagement
{
    public class FileManagementBootstrap : ContextView
    {
        void Awake()
        {
            context = new FilemanagementContext(this);
        }
    }
}
