﻿
using System.IO;
using Assets.Scripts.common.config;
using Assets.Scripts.game.player.model;
using Assets.Scripts.tools;
using Assets.Scripts.tools.Json;
using strange.extensions.command.impl;

namespace Assets.Scripts.filemanagement.Inventory.controller
{
    /// <summary>
    /// Loads the PlayerModel file into memory
    /// </summary>
    public class InventoryLoader : Command
    {
        [Inject]
      public  IPlayerModel MainPlayerModel { get; set; }
        [Inject]
        public game.inventory.Model.Inventory Inventory { get; set; }

        private ExitTheVoidJsonConverterWriter _jsonConverter = new ExitTheVoidJsonConverterWriter();

        public override void Execute()
        {
            //see if path exists
            string path = ExitTheVoidFilePathRefs.InventoryDbPath(MainPlayerModel.Name);
            if (File.Exists(path))
            {
                if (MainPlayerModel.Inventory == null)
                {
                     MainPlayerModel.Inventory = new game.inventory.Model.Inventory();
                }
                MainPlayerModel.Inventory = _jsonConverter.LoadInventory(MainPlayerModel.Name);
            }
            else //dispatch a signal to create a new file
            {
                SaveInventoryData saveDataSignal = injectionBinder.GetInstance<SaveInventoryData>();
                saveDataSignal.Dispatch();
            }
        }
    }
}
