﻿ 

 
using Assets.Scripts.game.player.model;
using Assets.Scripts.tools;
using Assets.Scripts.tools.Json;
using strange.extensions.command.impl;

namespace Assets.Scripts.filemanagement.Inventory.controller
{
    public class InventorySaver: Command
    {
        [Inject]
       public  IPlayerModel MainPlayerModel { get; set; }
      
        private ExitTheVoidJsonConverterWriter _jsonConverter = new ExitTheVoidJsonConverterWriter();
        public override void Execute()
        {
            if (MainPlayerModel.Inventory == null)
            {
                game.inventory.Model.Inventory inventory = new game.inventory.Model.Inventory();
                _jsonConverter.SaveInventory(inventory, ExitTheVoidFilePathRefs.InventoryDbPath(MainPlayerModel.Name), true);
            }
            else
            {
                _jsonConverter.SaveInventory(MainPlayerModel.Inventory ,
                    ExitTheVoidFilePathRefs.InventoryDbPath(MainPlayerModel.Name), true);
            }
        }
    }
}
