﻿

using Assets.Scripts.common.config;
using Assets.Scripts.common.utilities;
using Assets.Scripts.filemanagement.controller.load;
using Assets.Scripts.filemanagement.controller.save_create;
using Assets.Scripts.filemanagement.Inventory.controller;
using Assets.Scripts.game.boosters.model; 
using Assets.Scripts.game.player.model;
using Assets.Scripts.tools;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using UnityEngine;

namespace Assets.Scripts.filemanagement.config
{
    public class FilemanagementContext : MVCSContext
    {
        #region constructors

        public FilemanagementContext(MonoBehaviour contextView)
            : base(contextView) { }

        #endregion

        protected override void addCoreComponents()
        {
            base.addCoreComponents();
            injectionBinder.Unbind<ICommandBinder>();
            injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
        }

        protected override void mapBindings()
        {
           
            game.inventory.Model.Inventory inventory = new game.inventory.Model.Inventory();  


            injectionBinder.Bind<game.inventory.Model.Inventory>() 
                .ToValue(inventory)
                .ToSingleton()
                .CrossContext();

            injectionBinder.Bind<LoadPlayerFileSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<CreateNewPlayerFileSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<LoadBoosterDataSignal>();
            injectionBinder.Bind<SaveInventoryData>().ToSingleton().CrossContext();
            injectionBinder.Bind<LoadInventoryData>().ToSingleton().CrossContext();

            BoosterDb boosterDb = new BoosterDb();  //create a cross context singleton of the booster db model
            injectionBinder.Bind<BoosterDb>().SetValue(boosterDb).ToSingleton().CrossContext();

            //create a PlayerModel model so it can hold all this data
            MainPlayerModel mainPlayerModel = new MainPlayerModel(boosterDb);
            injectionBinder.Bind<IPlayerModel>().ToValue(mainPlayerModel).ToSingleton().CrossContext();
            

            commandBinder.Bind<LoadPlayerFileSignal>().To<LoadLocalPlayerFile>();
            commandBinder.Bind<CreateNewPlayerFileSignal>().To<CreateNewPlayerFileCommand>();
            commandBinder.Bind<LoadBoosterDataSignal>().To<LoadBoosterData>();

            commandBinder.Bind<SaveInventoryData>().To<InventorySaver>();
            commandBinder.Bind<LoadInventoryData>().To<InventoryLoader>();
            injectionBinder.Bind<IRoutineRunner>().To<RoutineRunner>();
        }

        protected override void postBindings()
        { 
            LoadBoosterDataSignal lbds = injectionBinder.GetInstance<LoadBoosterDataSignal>();
            lbds.Dispatch();
            LoadInventoryData loadInvSignal = injectionBinder.GetInstance<LoadInventoryData>();
            loadInvSignal.Dispatch();
        }

        public override void Launch()
        {
            base.Launch();
            TempMethod(); //Todo : refactor temp method. I entered this solely for testing purposes
        }
 

        private void TempMethod()
        {
            LoadPlayerFileSignal lpfs = injectionBinder.GetInstance<LoadPlayerFileSignal>();
            lpfs.Dispatch(ExitTheVoidFilePathRefs.PlayerFileDataPath);
            commandBinder.Bind<LoadLevelByNameSignal>().To<LoadLevelCommand>();
            //injectionBinder.Bind<LoadLevelByNameSignal>().ToSingleton();
            LoadLevelByNameSignal load = injectionBinder.GetInstance<LoadLevelByNameSignal>();
            Debug.Log("dispatching new scene");
            load.Dispatch("Level_1 - b");
        }
    }
}
