﻿
using System.Collections;
using Assets.Scripts.common.utilities;
using Assets.Scripts.game.boosters.model;
using Assets.Scripts.tools;
using Assets.Scripts.tools.Json;
using strange.extensions.command.impl;
using UnityEngine;

namespace Assets.Scripts.filemanagement.controller.load
{
    /// <summary>
    /// simply loads the booster database into the boosterDB model
    /// </summary>
    public class LoadBoosterData : Command
    {
        [Inject]
        public BoosterDb BoosterDatabase { get; set; }
 [Inject]
        public IRoutineRunner runn { get; set; }
        
        private ExitTheVoidJsonConverterWriter _jsonConverter = new ExitTheVoidJsonConverterWriter();
        public override void Execute()
        {
            
            BoosterDatabase.Boosters = _jsonConverter.LoadJsonIntoDictionary(ExitTheVoidFilePathRefs.TesterBoosterDb);
           
        }

        
    }
}
