﻿using Assets.Scripts.game.view.player.interactions;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using UnityEngine;

namespace Assets.Scripts.filemanagement.controller.load
{
    public class LoadLevelCommand: Command
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; } 
        [Inject]
        public string LevelName { get; set; }
        public override void Execute()
        {
            Debug.Log("loadlevelcommand execute");
            Application.LoadLevelAdditive(LevelName);

          
        }
    }
}
