﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Assets.Scripts.common.config;
using Assets.Scripts.game.player.model;
using strange.extensions.command.impl;
using UnityEngine;

namespace Assets.Scripts.filemanagement.controller.load
{
    /// <summary>
    /// this command attempts to locally load a character file and places it in the PlayerModel attributes model
    /// </summary>
    public class LoadLocalPlayerFile : Command
    {
        [Inject]
        public string FileName { get; set; }
        [Inject]
        public CreateNewPlayerFileSignal CreateNewPlayer { get; set; }
        [Inject]
        public IPlayerModel MainPlayerModel { get; set; }

        private PlayerAttributesAndStats _playerAttributes;

        public override void Execute()
        { 
            //check if the file exists

            if (File.Exists(FileName))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fileStream = File.Open(FileName, FileMode.Open);
                _playerAttributes = bf.Deserialize(fileStream) as PlayerAttributesAndStats;
              
                MainPlayerModel.PlayerStats = _playerAttributes;
            }
            else //send a signal that a file doesn't exist and to create a new file
            {
                CreateNewPlayer.Dispatch(FileName);
                Debug.Log("sending request to create a new  PlayerModel file");

            }


        }

         
    }
}
