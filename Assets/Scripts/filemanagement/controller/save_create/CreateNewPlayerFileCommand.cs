﻿  
using Assets.Scripts.filemanagement.tools;
using Assets.Scripts.game.player.model;
using strange.extensions.command.impl; 
namespace Assets.Scripts.filemanagement.controller.save_create
{
    public class CreateNewPlayerFileCommand: Command
    {
        
        [Inject]
        public string Filename { get; set; }
        private PlayerAttributesAndStats _playerAttributes;
        [Inject]
        public IPlayerModel MainPlayerModel { get; set; }
        public override void Execute()
        {
            if (MainPlayerModel.PlayerStats == null)
            {
                _playerAttributes = new PlayerAttributesAndStats();
                MainPlayerModel.PlayerStats = _playerAttributes;
                SaverLoader.SavePlayerFile(_playerAttributes, Filename);
            }
            
            else
            {
                _playerAttributes = MainPlayerModel.PlayerStats as PlayerAttributesAndStats;
                SaverLoader.SavePlayerFile(_playerAttributes, Filename);
            }
        }
    }
}
