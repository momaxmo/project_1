﻿ 

namespace Assets.Scripts.filemanagement.controller.save_create
{
    public interface ISaverLoader
    {
        /// <summary>
        /// Ensures that the file is valid
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        bool Validate(string filename);
    }
}
