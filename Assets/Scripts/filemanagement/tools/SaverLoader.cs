﻿
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Assets.Scripts.filemanagement.config;
using Assets.Scripts.game.player.model;
using UnityEngine;

namespace Assets.Scripts.filemanagement.tools
{
    /// <summary>
    /// Helper class for saving an loading context
    /// </summary>
    public static class SaverLoader
    {

        /// <summary>
        /// serializes a playerstat instance and creates a file from the provided file   path
        /// </summary>
        /// <param name="playerstats"></param>
        /// <param name="filePath"></param>
        public static void SavePlayerFile(PlayerAttributesAndStats playerstats, string filePath)
        {
            try
            {
                 BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Create(filePath);
                bf.Serialize(file, playerstats);
                file.Close();
            }
            catch (InvalidExitTheVoidFileName exception)
            {
                Debug.Log(exception.StackTrace);
                throw;
            }


        }
    }
}
