﻿
namespace Assets.Scripts.game
{
    public interface IPausable
    {
        bool IsPaused { get; set; }
    }
}
