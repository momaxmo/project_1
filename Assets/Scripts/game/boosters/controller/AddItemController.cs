﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.game.inventory.Model;
using strange.extensions.command.impl;

namespace Assets.Scripts.game.boosters.controller
{
   public  class AddItemController : Command
    {
       /// <summary>
       /// in game item inventory
       /// </summary>
       [Inject]
       public Inventory ItemInventory { get; set; }
       [Inject]
       public IInventoryItem NewItem { get; set; }
       public override void Execute()
       {
            ItemInventory.Add(NewItem);
       }
    }
}
