﻿using System.Collections.Generic;
using LitJson;

namespace Assets.Scripts.game.boosters.model
{
    /// <summary>
    /// holds a dictionary of concrete boosters
    /// </summary>
    public class BoosterDb
    {
        private Dictionary<string, ConcreteBoosterModel> _boosters;
   
        public BoosterDb()
        {
            _boosters = new Dictionary<string, ConcreteBoosterModel>(0);
        }
        public Dictionary<string, ConcreteBoosterModel> Boosters
        {
            get
            {
                return _boosters;
            }
            set { _boosters = value; }
        }
         [JsonIgnore]
        public ConcreteBoosterModel this[string key]
        {
            get { return Boosters[key]; }
        }

    }

}
