﻿using System;
using Assets.Scripts.game.inventory.Model;
using Assets.Scripts.game.model;
using Assets.Scripts.game.player.model;
using Assets.Scripts.tools;
using LitJson;
using UnityEngine;

namespace Assets.Scripts.game.boosters.model
{
   public  class ConcreteBoosterModel: IInventoryItem
   {
       [JsonIgnore] private short MaxRemainingChargeValue = Int16.MaxValue - 1;
        [SerializeField]
        private const bool IsitUsable = true;
        [SerializeField]
        private BoosterAlteration _alteration = BoosterAlteration.PlayerStats;
        [SerializeField]
        private BoosterDuration _duration = BoosterDuration.Cooldown;
        [SerializeField]
        private int _remainingCharges = 1;
        [SerializeField]
        private BoosterAlignment _alignment = BoosterAlignment.Light;
        [SerializeField]
        private int _id;
        [SerializeField]
        private string _name;

        [SerializeField]
        private float _coolDownTimer;

        private float _previousTimeUsed = -120; //initialize to a negative number as to allow the use of this booster once enabled in game. This works around time limits

        [SerializeField]
        private string _description;

        [SerializeField]
        private BonusStatsMod _bonusStats;
        [SerializeField]
        private string _spritePath;

        [JsonIgnore]
        private Sprite _boosterSprite;


        public BoosterAlteration Alteration
        {
            get { return _alteration; }
            set { _alteration = value; }
        }

        public BoosterDuration Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        public int RemainingCharges
        {
            get
            {
                return _remainingCharges;
            }
            set
            {
                //check what value is to prevent overflow
                if (value > MaxRemainingChargeValue || value < 0)
                {
                    _remainingCharges = MaxRemainingChargeValue;
                }
                else
                {
                    _remainingCharges = value;                    
                }
            }
        }

        public BoosterAlignment Alignment
        {
            get { return _alignment; }
            set { _alignment = value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }


        public bool IsUsable
        {
            get { return IsitUsable; }
            set { }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public float CoolDownTimer
        {
            get { return _coolDownTimer; }
            set { _coolDownTimer = value; }
        }

        public float PreviousTimeUsed
        {
            get { return _previousTimeUsed; }
            set { _previousTimeUsed = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public BonusStatsMod BonusStats
        {
            get
            {
                if (_bonusStats == null)
                    _bonusStats = new BonusStatsMod();
                return _bonusStats;
            }
            set { _bonusStats = value; }
        }
        [JsonIgnore]
        public Sprite BoosterSprite
        {
            get
            {
                if (_boosterSprite == null && SpritePath != null)
                {
                    //load asset 
                    if (SpritePath.Length != 0) //load the placeholder sprite
                    {
                        _boosterSprite = UnityEngine.Resources.Load<Sprite>(SpritePath); 
                    }
                    else
                    { 
                        _boosterSprite = ExitTheVoidTools.PlaceholderIconsprite();
                    }
                }
                return _boosterSprite;
            }
            set { _boosterSprite = value; }
        }

        public string SpritePath
        {
            get { return _spritePath; }
            set
            {
                _spritePath = value;
            }
        }
    }
}
