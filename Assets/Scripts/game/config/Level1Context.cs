﻿
using Assets.Scripts.common.config;
using Assets.Scripts.common.model;
using Assets.Scripts.common.view.camera;
using Assets.Scripts.game.controller.player.interactions;
using Assets.Scripts.game.input.controller;
using Assets.Scripts.game.input.signals;
using Assets.Scripts.game.levels.controller.command; 
using Assets.Scripts.game.model;
using Assets.Scripts.game.player.model;
using Assets.Scripts.game.view.environment;
using Assets.Scripts.game.view.player;
using Assets.Scripts.game.view.player.interactions;
 
using UnityEngine;

namespace Assets.Scripts.game.config
{
    public class Level1Context : SignalContext
    {

        GameSoundSetting<float> _soundVolume = new GameSoundSetting<float>();
        public Level1Context(MonoBehaviour contextview)
            : base(contextview)
        {

        }


        protected override void mapBindings()
        {
            base.mapBindings();
            commandBinder.Bind<StartSignal>().To<Level1StartCommand>(); 

          

            //game state signals

           
            injectionBinder.Bind<GameStartSignal>().ToSingleton();
            injectionBinder.Bind<GameExitSignal>().ToSingleton().CrossContext();
            if (firstContext == this)
            {


                // injectionBinder.Bind<LoadLevel1Signal>().ToSingleton().CrossContext();

                injectionBinder.Bind<GameExitSignal>().ToSingleton().CrossContext();


                //   injectionBinder.Bind<GamePausedSignal>().ToSingleton().CrossContext();
                //    injectionBinder.Bind<GamePauseCommandSignal>().ToSingleton().CrossContext();
                // commandBinder.Bind<GamePauseCommandSignal>().To<PauseCommand>();

            }

            //lets check if the gamestatemodel exists already
            if (injectionBinder.GetInstance<LevelStateModel>() == null)
            {
                Debug.Log("adding gamestatemodel to the injection binder. For now its a default state but as you build the game you should change this, inside "+GetType());
                LevelStateModel levelModel = new LevelStateModel() { IsPaused = false };
                injectionBinder.Bind<LevelStateModel>().ToValue(levelModel).ToSingleton().CrossContext();
            }

           


            injectionBinder.Bind<UpdateCameraFollowTarget>().ToSingleton(); //putting this first because the PlayerControllerViewMediator is dependent on this class
            injectionBinder.Bind<PlayerVelocityUpdatedSignal>().ToSingleton();
            injectionBinder.Bind<UpdateLevelBoundsSignal>().ToSingleton();
          
            //level starting and ending
            injectionBinder.Bind<LevelStartSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<LevelEndSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<WeaponPickedUpSignal>().ToSingleton().CrossContext();

            //enemy signals
            injectionBinder.Bind<HurtEnemySignal>().ToSingleton().CrossContext();
            //Boosters for PlayerModel
             
            injectionBinder.Bind<UseBooster1Signal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UseBooster2Signal>().ToSingleton().CrossContext();
            //PlayerModel
           
            injectionBinder.Bind<IPlayerBehaviourState>().To<CharacterBehaviourState>();
            injectionBinder.Bind<IPlayerBehaviourParameters>().To<PlayerBehaviourParameters>();
            injectionBinder.Bind<IPlayerBehaviourPermissions>().To<PlayerBehaviourPermissions>();
            injectionBinder.Bind<IPlayerControllerState>().To<PlayerControllerState>();
            injectionBinder.Bind<IPlayerControllerParameters>().To<PlayerControllerParameters>();
            injectionBinder.Bind<MeleeHitSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<StartMeleeHitSignal>().ToSingleton().CrossContext();

            commandBinder.Bind<MeleeHitSignal>().To<PlayerAttackInteractionController>();

            mediationBinder.Bind<MeleeView>().To<MeleeViewMediator>();
           // mediationBinder.Bind<View>().To<PlayerControllerViewMediator>();
            mediationBinder.Bind<PlayerBehaviourView>().To<PlayerBehaviourViewMediator>();
            //input 
            injectionBinder.Bind<MovementGameInputSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<GameJumpButtonSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<InputSignal>().ToSingleton().CrossContext();
            commandBinder.Bind<InputSignal>().To<InputManagerController>() ;

           
            
        }

        protected override void postBindings()
        {
            base.postBindings();
            //camera 
            mediationBinder.Bind<CameraFollow>().To<CameraFollowMediator>();
            mediationBinder.Bind<LevelBounds>().To<LevelBoundsMediator>();
 
        }
    }
}
