﻿ 
using Assets.Scripts.common;
using Assets.Scripts.common.config;
using Assets.Scripts.common.model;
using Assets.Scripts.common.view.camera;
using Assets.Scripts.filemanagement.controller.load;
using Assets.Scripts.game.controller;
using Assets.Scripts.game.controller.player.interactions;
using Assets.Scripts.game.input.controller;
using Assets.Scripts.game.input.signals;
using Assets.Scripts.game.inventory.controller;
using Assets.Scripts.game.inventory.view;
using Assets.Scripts.game.levels.checkpoints;
using Assets.Scripts.game.levels.checkpoints.controller;
using Assets.Scripts.game.levels.checkpoints.view;
using Assets.Scripts.game.levels.controller.command;
using Assets.Scripts.game.levels.controller.input;
using Assets.Scripts.game.model;
using Assets.Scripts.game.player;
using Assets.Scripts.game.player.controller; 
using Assets.Scripts.game.player.signals;
using Assets.Scripts.game.player.view;
using Assets.Scripts.game.uielements.healthbar.view;
using Assets.Scripts.game.view.environment;
using Assets.Scripts.game.view.player;
using Assets.Scripts.game.view.player.interactions;
using Assets.Scripts.tools; 
using Assets.Scripts.tools.Json;
using Assets.Scripts.ui.controller;
using Assets.Scripts.ui.model;
using Assets.Scripts.ui.view;
using Assets.Scripts.ui.view.ingame;
using Assets.Scripts.ui.view.pausemenu;
using UnityEngine;

namespace Assets.Scripts.game.config
{
   public  class Level1bContext : SignalContext
    {
       GameSoundSetting<float> _soundVolume = new GameSoundSetting<float>();
       private ExitTheVoidJsonConverterWriter _jsonConverter = new ExitTheVoidJsonConverterWriter();
       public Level1bContext(MonoBehaviour contextview)
            : base(contextview)
        {

        }


        protected override void mapBindings()
        {
            base.mapBindings();
            commandBinder.Bind<StartSignal>().To<Level1bStartCommand>();
              
            //game state signals
             
            injectionBinder.Bind<GameStartSignal>().ToSingleton(); //todo: what are these for ???
            injectionBinder.Bind<GameExitSignal>().ToSingleton().CrossContext();
 

            //load the gamestatemodel from a file
            LevelStateModel levelLevelModel =
                _jsonConverter.LoadJsonIntoGameStateModel(ExitTheVoidFilePathRefs.LevelModelDataPath("Level1b"));
            injectionBinder.Bind<LevelStateModel>().ToValue(levelLevelModel).ToSingleton().CrossContext();
           


            injectionBinder.Bind<UpdateCameraFollowTarget>().ToSingleton(); //putting this first because the PlayerControllerViewMediator is dependent on this class
            injectionBinder.Bind<PlayerVelocityUpdatedSignal>().ToSingleton();
            injectionBinder.Bind<UpdateLevelBoundsSignal>().ToSingleton();
          
            //level starting and ending
            injectionBinder.Bind<LevelStartSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<LevelEndSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<WeaponPickedUpSignal>().ToSingleton().CrossContext();

            //enemy signals
            injectionBinder.Bind<HurtEnemySignal>().ToSingleton().CrossContext();
            //Boosters for PlayerModel
             
          
            injectionBinder.Bind<SwitchBoosterSignal>().ToSingleton().CrossContext();
            commandBinder.Bind<SwitchBoosterSignal>().To<SwitchBoosterController>();

            //inventory
            injectionBinder.Bind<BuildInventory>().ToSingleton().CrossContext();
            //PlayerModel
            SetupPlayerBindings();
         
            SetupInput();
      
            //Whenever StartSignal is fired, UiStartCommand is executed
           // commandBinder.Bind<StartSignal>().To<UiStartCommand>();
            commandBinder.Bind<LoadNextLevelSignal>().To<LoadLevelCommand>();
            //Mediation  allows the MenuView code to be separated from the rest of the game.
            mediationBinder.Bind<CrossPlatformHomeView>().To<CrossPlatformHomeViewMediator>();

            mediationBinder.Bind<MusicPlayer>().To<MusicPlayerMediator>();
            // mediationBinder.Bind<SettingsView>().To<SettingsMediator>();
            mediationBinder.Bind<MusicVolumeControlView>().To<MusicVolumeControlMediator>();
            //this check is to verify whether we are in stand alone mode, otherwise it is being called somewhere else in the game
 

            //pausing
            injectionBinder.Bind<GamePausedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<GamePauseCommandSignal>().ToSingleton().CrossContext();
            commandBinder.Bind<GamePauseCommandSignal>().To<PauseCommand>();
            //health meter
            injectionBinder.Bind<UpdateHealthSignal>().ToSingleton().CrossContext();
            //game menu
            mediationBinder.Bind<GameMenuView>().To<GameMenuViewMediator>();
            mediationBinder.Bind<InGameHud>().To<InGameHudMediator>();
            mediationBinder.Bind<PauseMenu>().To<PauseMenuMediator>();
            mediationBinder.Bind<BoosterInventoryView>().To<BoosterInventoryViewMediator>();
            mediationBinder.Bind<CharacterSheetView>().To<CharacterSheetViewMediator>();
            injectionBinder.Bind<PauseMenuSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UnpausableStateSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UpdateBooster>().ToSingleton().CrossContext();
            commandBinder.Bind<PauseMenuSignal>().To<PauseMenuController>();


            PauseMenuModel pausemenuModel = new PauseMenuModel();


            injectionBinder.Bind<PauseMenuModel>().ToValue(pausemenuModel).ToSingleton();
            injectionBinder.Bind<SwitchToPauseMenuInterface>().ToSingleton();
            injectionBinder.Bind<SwitchToCharacterSheetSignal>().ToSingleton();
            injectionBinder.Bind<SwitchToBoosterViewSignal>().ToSingleton();
            injectionBinder.Bind<ShowPauseButtonsSignal>().ToSingleton();
            injectionBinder.Bind<ShowExitGameDialogueSignal>().ToSingleton();
            injectionBinder.Bind<ShowExitToMainMenuDialogueSignal>().ToSingleton();
            injectionBinder.Bind<SwitchToSettingsAndOptionsSignal>().ToSingleton();
            //Inventory grid
            injectionBinder.Bind<EnableBoosterModalSwitchView>().ToSingleton().CrossContext(); 
            injectionBinder.Bind<EnableGridInteractivity>().ToSingleton().CrossContext();
            mediationBinder.Bind<InventoryGrid>().To<InventoryGridMediator>();
            mediationBinder.Bind<InventorySwitchingModalSelector>().To<InventorySwitchingModalSelectorMediator>();
            injectionBinder.Bind<AddButtonForInventory>().ToSingleton().CrossContext();
            injectionBinder.Bind<AddNewInventoryItemSignal>().ToSingleton().CrossContext();
            commandBinder.Bind<AddNewInventoryItemSignal>().To<AddNewInventoryItem>();

            //Music volume update
            injectionBinder.Bind<MusicVolumeUpdatedSignal<float>>().ToSingleton().CrossContext();
            injectionBinder.Bind<ISettings<float>>().ToValue(_soundVolume).ToName(GameSettings.MusicVolume).ToSingleton().CrossContext();
           
            //Checkpoints
            injectionBinder.Bind<AddCheckpointSignal>().ToSingleton();//no reason this should be cross context
            mediationBinder.Bind<CheckpointView>().ToMediator<CheckpointViewMediator>(); //mediate the checkpoint view to the mediator
            commandBinder.Bind<AddCheckpointSignal>().To<AddCheckpointCommand>();
            injectionBinder.Bind<SpawnPlayerSignal>().ToSingleton();
            commandBinder.Bind<SpawnPlayerSignal>().To<SpawnPlayer>();

             injectionBinder.Bind<LoadPlayerOnStartSignal>().ToSingleton().CrossContext();
            commandBinder.Bind<LoadPlayerOnStartSignal>().To<LoadPlayerOnStart>();

            injectionBinder.Bind< uielements.healthbar.signal.UpdateHealthBarSignal>().ToSingleton().CrossContext();
 
            mediationBinder.Bind<FilledHealthbarView>().ToMediator<HealthbarViewMediator>();

        }
        void SetupInput()
        {
            //input 
            injectionBinder.Bind<MovementGameInputSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<GameJumpButtonSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<InputSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<DashInputSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UseBooster1Signal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UseBooster2Signal>().ToSingleton().CrossContext();
            commandBinder.Bind<InputSignal>().To<InputManagerController>();
        }

       void SetupPlayerBindings()
       {
           injectionBinder.Bind<MeleeHitSignal>().ToSingleton().CrossContext();
           injectionBinder.Bind<StartMeleeHitSignal>().ToSingleton().CrossContext();

           commandBinder.Bind<MeleeHitSignal>().To<PlayerAttackInteractionController>();

           mediationBinder.Bind<MeleeView>().To<MeleeViewMediator>();
           mediationBinder.Bind<PlayerControllerView>().To<PlayerControllerViewMediator>();
           mediationBinder.Bind<PlayerBehaviourView>().To<PlayerBehaviourViewMediator>();
           mediationBinder.BindView<PlayerBounds>().ToMediator<PlayerBoundsMediator>();
       }
        protected override void postBindings()
        {
            base.postBindings();
            //camera 
            mediationBinder.Bind<CameraFollow>().To<CameraFollowMediator>();
            mediationBinder.Bind<LevelBounds>().To<LevelBoundsMediator>(); 

            LoadPlayerOnStartSignal loadPlayer = injectionBinder.GetInstance<LoadPlayerOnStartSignal>();
            loadPlayer.Dispatch();
    
 
        }
    }
}
