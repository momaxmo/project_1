﻿ 

using strange.extensions.command.impl;

namespace Assets.Scripts.game.controller
{
    public class BoosterUseCommand : Command
    {
        /*
         Todo: inject a booster model. I am thinking about using some sort of routine that sets a boosters valid state for the seconds 
         * use a flyweight pattern for the boosters as to keep two instances on the PlayerModel running at all times. Just load 
         * the model of the booster 
         * 
         * controller --> model <---> view --->controller
         * 
         * on change, update the booster model 
         * the model would have a special effect on use? n
         */
    }
}
