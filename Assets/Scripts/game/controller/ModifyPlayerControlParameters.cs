﻿using Assets.Scripts.game.player.model;
using Assets.Scripts.game.view;
using strange.extensions.command.impl;

namespace Assets.Scripts.game.controller
{
   public class ModifyPlayerControlParameters:Command
   {
       [Inject]
       public IPlayerControllerParameters ControllerParameters { get; set; }
       [Inject]
       public IPlayerView PlayersView { get; set; }

       public override void Execute()
       {
           base.Execute();
       }
   }
}
