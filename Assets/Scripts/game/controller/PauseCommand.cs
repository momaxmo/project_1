﻿using Assets.Scripts.common.config;
using Assets.Scripts.game.model;
using Assets.Scripts.ui.model;
using strange.extensions.command.impl;
using UnityEngine;


namespace Assets.Scripts.game.controller
{
    /// <summary>
    /// initiates the gamepause signal with _isPaused value
    /// </summary>
    public class PauseCommand : Command
    {

        [Inject]
        public GamePausedSignal PausedSignal { get; set; }
        [Inject]
        public LevelStateModel LevelState { get; set; }
        [Inject]
        public UnpausableStateSignal UnpausableSignal { get; set; }
        [Inject]
        public PauseMenuSignal PauseViewSignal { get; set; }
     
        public override void Execute()
        {
            
            if (!LevelState.IgnorePauseSignals)
            {
                bool isPaused = LevelState.IsPaused; 
                isPaused = !isPaused; 
                LevelState.IsPaused = isPaused;
                PausedSignal.Dispatch(isPaused);
                if (isPaused)
                { 
                    PauseViewSignal.Dispatch(PauseMenuState.MainPauseMenu);
                }
                else
                {
                    PauseViewSignal.Dispatch(PauseMenuState.Off);
                }
            }
            else
            {
                UnpausableSignal.Dispatch(); 
                Debug.Log("tried to unpause the game when in an unpaused state");
            }
        }
    }
}
