﻿
 
using Assets.Scripts.game.inventory.Model; 
using Assets.Scripts.game.player.model;
using Assets.Scripts.game.view.player.interactions;
using strange.extensions.command.impl;

namespace Assets.Scripts.game.controller.player.interactions
{
    /// <summary>
    /// command to attach boosters into the booster view
    /// </summary>
    public class AttachBooster : Command
    { 
        [Inject]
        public int BoosterSlotId { get; set; }

        [Inject]
        public IInventoryItem BoosterModelToAttach { get; set; }
        /// <summary>
        ///   if the boostermodeltoattach affects the PlayerModel stats and is permanent, directly call the use method in   Booster1/2
        /// </summary>
        [Inject]
        public IPlayerModel PlayerModel { get; set; }

        public override void Execute()
        {
            if (BoosterSlotId == 1)
            {
                PlayerModel.Booster1.InventoryItem = BoosterModelToAttach;
                CheckIfPermanentAndAffectsPlayerStats(BoosterModelToAttach, PlayerModel.Booster1);
            }
            else
            {
                PlayerModel.Booster2.InventoryItem = BoosterModelToAttach;
                CheckIfPermanentAndAffectsPlayerStats(BoosterModelToAttach, PlayerModel.Booster2);
            }
        }

        private void CheckIfPermanentAndAffectsPlayerStats(IInventoryItem boostermodel, IBooster booster)
        {
            if (boostermodel != null)
            {
                if (boostermodel.Alteration == BoosterAlteration.PlayerStats
                    && boostermodel.Duration == BoosterDuration.PermanentEffect)
                {
                    PlayerModel.PlayerStats.BonusStats = boostermodel.BonusStats;
                }
            }
            else// null boostermodel passed in, means that the booster has been detached
            {
                PlayerModel.PlayerStats.BonusStats.Reset();
            }
            
        }
    }
}
