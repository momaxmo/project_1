﻿ 
using Assets.Scripts.common.config; 
using Assets.Scripts.game.player.model;
using strange.extensions.command.impl;
using UnityEngine;

namespace Assets.Scripts.game.controller.player.interactions
{
    /// <summary>
    /// The purpose of this controller is to transmit proper signals according to the interactable game object.
    /// </summary>
    public class PlayerAttackInteractionController: Command
    {
        [Inject]
        public IPlayerModel PlayerModel { get; set; }
        [Inject]
        public GameObject InteractableGameObject { get; set; }
        [Inject]
        public HurtEnemySignal HurtSignal { get; set; }

        public override void Execute()
        {
            if (InteractableGameObject.tag == "Enemy")
            {
                int totalAttackPower = PlayerModel.PlayerStats.BaseAttackPower;
                int playerLevel = PlayerModel.PlayerStats.Level;
                float chanceToHitEnemy = PlayerModel.PlayerStats.BaseAccuracy;
                float chanceToStunEnemy = PlayerModel.PlayerStats.BaseChanceToStun;
                StrikeEnemy strike = new StrikeEnemy(InteractableGameObject, totalAttackPower, playerLevel, chanceToHitEnemy, chanceToStunEnemy);
                HurtSignal.Dispatch(strike);
            }
        }

       
    }
}
