﻿ 

namespace Assets.Scripts.game.levels.controller.input
{
    public class InputEvents
    {
    }
    /// <summary>
    /// Static input events, doesn't matter what the type of controllers in use is but the input would remain the same
    /// 
    /// </summary>
    public class GameInputEvent
    {
        public static int None = 0;
        public static int UseBooster1 = 1; // PlayerModel pressed booster button 1
        public static int UseBooster2 = 2;// PlayerModel pressed booster button 2
        public static int PressedPause = 4; // PlayerModel pressed pause 
        public static int MeleeHit1 = 8; //PlayerModel quick melee hit
        public static int MeleeHit2 = 16;
        public static int Dash = 32; 


    }
}
