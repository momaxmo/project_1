﻿using Assets.Scripts.common.config;
using Assets.Scripts.game.input.signals;
using Assets.Scripts.game.levels.controller.input;
using strange.extensions.command.impl;

namespace Assets.Scripts.game.input.controller
{
    /// <summary>
    /// using bitwise operations, we test to see if the input value has a particular game input event
    /// "&  Test if the value appears in the result"
    /// </summary>
    public class InputManagerController: Command
    {
        [Inject]
        public StartMeleeHitSignal StartMelee { get; set; }
        [Inject]
        public UseBooster1Signal UseBoosterSignal1 { get; set; }
        [Inject]
        public UseBooster2Signal UseBoosterSignal2 { get; set; }
        [Inject]
        public int InputValue { get; set; }
        [Inject]
        public DashInputSignal DashInputSignal { get; set; }
        public override void Execute()
        {
   
            bool hasMeleed = (InputValue & GameInputEvent.MeleeHit1) > 0;
            bool booster1Used = (InputValue & GameInputEvent.UseBooster1) > 0;
            bool booster2Used = (InputValue & GameInputEvent.UseBooster2) > 0;
            bool hasDashed = (InputValue & GameInputEvent.Dash) >0;
            if (hasMeleed)
            {
                StartMelee.Dispatch();
                 
            }
            if (booster1Used)
            {
                UseBoosterSignal1.Dispatch();
            }
            if (booster2Used)
            {
                UseBoosterSignal2.Dispatch();
            }
            if (hasDashed)
            {
                DashInputSignal.Dispatch();
            }

        }
        
    }
}
