﻿using System.Collections;
using Assets.Scripts.common.config;
using Assets.Scripts.common.utilities;
using Assets.Scripts.game.input.signals;
using Assets.Scripts.game.levels.controller.input;
using Assets.Scripts.game.model;
using UnityEngine;

namespace Assets.Scripts.game.input.controller
{
    public class KeyboardInput : IInput
    {
        private float _horizontalAxis, _verticalAxis; //used to determine movement on these axes
        private float _lastTimePaused;
        private bool _jumpButtonHeldDown;
        private bool _jumpButtonPressed;
        private bool _releasedJumpButton;

        //[Inject(ContextKeys.CONTEXT_DISPATCHER)]
        //public IEventDispatcher Dispatcher { get; set; }
        [Inject]
        public MovementGameInputSignal MovementGameInputSignal { get; set; }
        [Inject]
        public GameJumpButtonSignal GameJumpInputSignal { get; set; }
        [Inject]
        public IRoutineRunner RoutineRunner { get; set; }
        [Inject]
        public GamePauseCommandSignal PauseCommandSignal { get; set; }
        [Inject]
        public LevelStateModel GamestateModel { get; set; }
        [Inject]
        public InputSignal InputSig { get; set; }

        [Inject]
        public DashInputSignal DashInputSignal { get; set; }
        [PostConstruct]
        public void PostConstruct()
        {

            RoutineRunner.StartCoroutine(Update());

        }
        protected IEnumerator Update()
        {
            while (true)
            {
                int input = GameInputEvent.None;
                _jumpButtonPressed = Input.GetButtonDown("Jump");
              //  _jumpButtonHeldDown = Input.GetButton("Jump");
                _releasedJumpButton = Input.GetButtonUp("Jump");
                _horizontalAxis = Input.GetAxis("Horizontal");
                _verticalAxis = Input.GetAxis("Vertical");

                if (Input.GetButtonDown("Pause"))
                {
                    //This check is done to prevent keymashing. If a key is mashed
                    //too many times, it messes with the animation of the pause fading
                    if (Time.time - _lastTimePaused > GamestateModel.MinimumPauseTime)
                    {
                        PauseCommandSignal.Dispatch();


                        _lastTimePaused = Time.time;
                    }


                }
                if (Input.GetButtonDown("Booster1"))
                {
                    input |= GameInputEvent.UseBooster1;
                }
                if (Input.GetButtonDown("Booster2"))
                {
                    input |= GameInputEvent.UseBooster2;
                }
                if (Input.GetButtonDown("Fire1"))
                {
                    input |= GameInputEvent.MeleeHit1;
                }
                if (Input.GetButtonDown("Dash"))    
                {
                    input |= GameInputEvent.Dash;
                }

                MovementGameInputSignal.Dispatch(_horizontalAxis, _verticalAxis);
                GameJumpInputSignal.Dispatch(_jumpButtonPressed  , _releasedJumpButton);
                if (input > 0)
                    InputSig.Dispatch(input);

                yield return null;

            }

        }


    }
}
