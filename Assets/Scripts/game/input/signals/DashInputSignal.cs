﻿ 

using strange.extensions.signal.impl;

namespace Assets.Scripts.game.input.signals
{
    public class DashInputSignal :Signal
    {
    }
    public class MovementGameInputSignal : Signal<float, float>
    {
    }

    /// <summary>
    /// Sends a signal if the button is pressed,held down and let go
    /// </summary>
    public class GameJumpButtonSignal : Signal<bool,  bool>
    {
    }

    public class UseBooster1Signal : Signal
    {
    }
    public class UseBooster2Signal : Signal
    {
    }
}
