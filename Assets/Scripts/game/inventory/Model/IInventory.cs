﻿  
using System.Collections.Generic; 

namespace Assets.Scripts.game.inventory.Model
{

    /// <summary>
    /// interface for an inventory 
    /// </summary>
   public interface IInventory
   {
       Dictionary<string, IInventoryItem> InventoryList { get; set; }
        bool Add(IInventoryItem item);
        void Remove(IInventoryItem item);

        List<IInventoryItem> ToList();
   }
}
