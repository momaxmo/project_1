﻿ 
using System.Xml.Serialization;
using Assets.Scripts.game.player.model;
using UnityEngine;

namespace Assets.Scripts.game.inventory.Model
{
    /// <summary>
    /// Interface for inventory items
    /// </summary>
    public interface IInventoryItem 
    {

        [XmlAttribute("name")]
        bool IsUsable{ get; set; }
       

        [XmlAttribute("name")]
        string Name { get; set; }
        [XmlAttribute("description")]
        string Description { get; set; }
        [XmlAttribute("id")]
        int Id { get; set; }
        [XmlAttribute("alteration")]
        BoosterAlteration Alteration { get; set; }
        [XmlAttribute("duration")]
        BoosterDuration Duration { get; set; }
        [XmlAttribute("remainingCharges")]
        int RemainingCharges { get; set; }
        [XmlAttribute("cooldownTimer")]
        float CoolDownTimer { get; set; }
        [XmlAttribute("previousTimeUsed")]
        float PreviousTimeUsed { get; set; }
        [XmlAttribute("alignment")]
        BoosterAlignment Alignment { get; set; }
        [XmlAttribute("bonusstats")]
        BonusStatsMod BonusStats { get; set; }

        Sprite BoosterSprite { get; set; }
        string SpritePath { get; set; }
    }

    /// <summary>
    /// What does the booster affect? 
    /// </summary>
    public enum BoosterAlteration
    {
        [XmlEnum(Name = "PlayerStats")]
        PlayerStats,
        [XmlEnum(Name = "OtherEffect")]
        OtherEffect // other effect such as invincibility, hits causes enemies to get stunned, create an AOE effect

    }

    /// <summary>
    /// 
    /// </summary>
    public enum BoosterDuration
    {
        [XmlEnum(Name = "Cooldown")]

        Cooldown,
        [XmlEnum(Name = "OneTimeUsage")]

        OneTimeUsage,
        [XmlEnum(Name = "PermanentEffect")]
        PermanentEffect
    }

    public enum BoosterAlignment
    {
        [XmlEnum(Name = "Light")]

        Light,
        [XmlEnum(Name = "Shadow")]

        Shadow
    }
 

}
