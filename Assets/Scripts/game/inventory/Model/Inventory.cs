﻿ 
using System.Collections.Generic;
using System.Linq; 
using UnityEngine;

namespace Assets.Scripts.game.inventory.Model
{
    public class Inventory: IInventory
    {
        private Dictionary<string, IInventoryItem> _inventoryList;
        public Inventory()
        {
            _inventoryList = new Dictionary<string, IInventoryItem>(15);
        }

        public Dictionary<string, IInventoryItem> InventoryList
        {
            get { return _inventoryList; }
            set { _inventoryList = value; }
        }

        // ReSharper disable once ConvertToAutoProperty
      
        /// <summary>
        /// Add an Iinventory item
        /// </summary>
        /// <param name="item"></param>
        public bool Add(IInventoryItem item)
        {
            //check if item exists in game
            //what type of item it is (stackable or not?)
            if (InventoryList.ContainsKey(item.Id + ""))
            {
                if (item.Duration == BoosterDuration.Cooldown || item.Duration == BoosterDuration.OneTimeUsage)
                {
                    //increase the count
                    item.RemainingCharges++;
                }
                return true;
            }
            else
            {
                InventoryList.Add(item.Id+"",item);
                return false;
            }
            
        }
        /// <summary>
        /// Remove an inventory item
        /// </summary>
        /// <param name="item"></param>
        public void Remove(IInventoryItem item)
        {
            if (!InventoryList.ContainsKey(item.Id + ""))
            {
                Debug.Log("tried to remove an item that doesn't  exist from the inventory");
            }
            InventoryList.Remove(item.Id + "");
        }

        public List<IInventoryItem> ToList()
        {
            return InventoryList.Values.ToList() ;
        }
    }
}
