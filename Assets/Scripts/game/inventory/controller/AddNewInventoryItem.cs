﻿
using Assets.Scripts.common.config;
using Assets.Scripts.game.boosters;
using Assets.Scripts.game.boosters.model;
using Assets.Scripts.game.inventory.Model;
using strange.extensions.command.impl;
using UnityEngine;

namespace Assets.Scripts.game.inventory.controller
{
   public  class AddNewInventoryItem: Command
   {
       [Inject]
       public string ItemKey { get; set; }
       [Inject]
       public Inventory Inventory { get; set; }
       [Inject]
       public BoosterDb BoosterDatabase { get; set; }

       public override void Execute()
       {
           AddButtonForInventory signal = injectionBinder.GetInstance<AddButtonForInventory>();
           IInventoryItem item = BoosterDatabase[ItemKey];
           if (!Inventory.Add(item)) 
           {
               Debug.Log("dispatching new button to be added in " + GetType());
               signal.Dispatch(item);
           }
       }
   }
}
