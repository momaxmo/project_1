﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Assets.Scripts.game.inventory.view
{
   public  class ButtonRow  
   {
       private List<IterableButton> _row;
       public List<IterableButton> Row
       {
           get
           {
               if (_row == null)
               {
                   _row = new List<IterableButton>(1);
               }
               return _row; 
           }
       }

       public IterableButton this[int i]
       {
           get { return Row[i]; }
           set { Row[i] = value; }
       }

      

       public IterableButton RemoveAt(int index)
       {
           if (index >= Row.Count || index < 0)
           {
               throw new ArgumentOutOfRangeException("Tried to access index "+index +" in row number "+RowId);
           }
           IterableButton returner = Row[index];
           Row.RemoveAt(index);
           return returner;
       }

       public void InsertAt(int index,IterableButton button)
       {
           Row.Insert(index,button);
       }
       public IterableButton RemoveButton(int index)
       {
           Row[index].Deactivate();
           IterableButton returner = Row[index];
          // Row[index] = null;
           Row.RemoveAt(index);
           return returner;
       }
       public int RowId { get; set; }
       public int Count { get { return Row.Count; } }

       public void Add(IterableButton newIterableButton)
       {
           Row.Add(newIterableButton);
       }

       public ButtonRow(int startCapacity)
       {
           _row = new List<IterableButton>(startCapacity);
       }
       public ButtonRow(Button button, int buttonRow, int buttonColumn, int rowId)
       {
           IterableButton newButtonStructure = new IterableButton()
           {
               Button = button,
               ColumnIndex = buttonColumn,
               RowIndex = buttonRow
           };
           Row.Add(newButtonStructure);
           RowId = rowId;
       }

       public ButtonRow(IterableButton buttonStructure, int rowId)
       {
           Row.Add(buttonStructure);
           RowId = rowId;
       }

       public void Remove(IterableButton b)
       {
           if (Count > 0)
           {
               
           }
       }
   }
}
