﻿  
using Assets.Scripts.game.inventory.Model;

namespace Assets.Scripts.game.inventory.view
{
   public  class InventoryButtonStructure
   {
       private IInventoryItem _model;

       public IInventoryItem InventoryItem
       {
           get { return _model; }
           set
           {
               _model = value;
               Descriptor = value.Description;
           }
       }
       public string Descriptor { get; set; }
   }
}
