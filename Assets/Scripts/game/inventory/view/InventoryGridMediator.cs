﻿using System.Collections.Generic;
using Assets.Scripts.common.config; 
using Assets.Scripts.game.inventory.Model;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.game.inventory.view
{
    public class InventoryGridMediator : Mediator
    {

        [Inject]
        public BuildInventory BuildInventoryFinishedLoading { get; set; }
        [Inject]
        public InventoryGrid InventoryGrid { get; set; }
        [Inject]
        public AddButtonForInventory AddButtonForInventoryItem
        {
            get;
            set;
        }

        [Inject]
        public EnableBoosterModalSwitchView EnableModalWindow { get; set; }
        [Inject]
        public EnableGridInteractivity EnableGrid { get; set; }

        private bool _inventoryLoaded;//was the inventory loaded yet?


        public override void OnRegister()
        {
            InventoryGrid.Init();
            BuildInventoryFinishedLoading.AddListener(StartLoadingInventory);
            EnableGrid.AddListener(EnableButtonInteractions);
            AddButtonForInventoryItem.AddListener(AddNewButton);
        }

        public override void OnRemove()
        {
            BuildInventoryFinishedLoading.RemoveListener(StartLoadingInventory);
            AddButtonForInventoryItem.RemoveListener(AddNewButton);
            EnableGrid.RemoveListener(EnableButtonInteractions);
        }

        private void StartLoadingInventory( Inventory inventory)
        {
            if (inventory != null && !_inventoryLoaded)
            {
                _inventoryLoaded = true;
                var inventoryValues = inventory.ToList();
                List<IterableButton> buttons = InventoryGrid.InsertMultipleButtons(inventoryValues);
                List<IInventoryItem> inventoryItems = inventoryValues;
                for (int i = 0; i < buttons.Count; i++)
                {
                    var i1 = i;
                    buttons[i].UpdateCallback(() => BoosterChangeButtonClick(inventoryItems[i1], buttons[i1]));
                    buttons[i].SetInteractivity(inventoryItems[i].IsUsable);
                }
            }

        }

        private void AddNewButton(IInventoryItem item)
        {
            IterableButton button = InventoryGrid.InsertNewButton(item);
            button.UpdateCallback(() => BoosterChangeButtonClick(item, button));
            Debug.Log("adding button in "+GetType());
        }
        private void EnableButtonInteractions()
        {
            InventoryGrid.EnableButtonInteractivity(true);
        }
        private void BoosterChangeButtonClick(IInventoryItem item, IterableButton button)
        {
            InventoryGrid.EnableButtonInteractivity(false);
            EnableModalWindow.Dispatch(button, item);

            Debug.Log("reminder: escape or unpause button goes back a step");
        }
    }
}
