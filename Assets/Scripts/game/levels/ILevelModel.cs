﻿ 
using System.Collections.Generic; 
using UnityEngine;

namespace Assets.Scripts.game.levels
{
    public interface ILevelModel
    {
        Dictionary<string,GameObject> CheckPoints { get; set; }
        int Score { get; set; }

        string LevelName { get; set; }
    }
}
