﻿  
using Assets.Scripts.game.config;
using strange.extensions.context.impl;
using UnityEngine;


namespace Assets.Scripts.game.levels.bootstraps
{
    public class Level1Bootstrap : ContextView{
       /* [Inject(GameSettings.MusicVolume)]
        public ISettings<float> MusicVolume { get; set; }*/
        private void Awake()
        {
            context = new Level1Context(this);
  
       //     UnityEngine.Debug.Log("in " + GetType().Name + " value of music vol is "+MusicVolume.Value);
        }
    }
}
