﻿ 
using Assets.Scripts.game.config;
using strange.extensions.context.impl;

namespace Assets.Scripts.game.levels.bootstraps
{
    public class Level1bBootstrap: ContextView
    {
         private void Awake()
        {
            context = new Level1bContext(this);
  //
       //     UnityEngine.Debug.Log("in " + GetType().Name + " value of music vol is "+MusicVolume.Value);
        }
    }
}
