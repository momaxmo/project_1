﻿ 

using Assets.Scripts.game.levels.checkpoints.model;
using strange.extensions.signal.impl;

namespace Assets.Scripts.game.levels.checkpoints
{
    public class AddCheckpointSignal: Signal<ICheckpoint>
    {
    }
}
