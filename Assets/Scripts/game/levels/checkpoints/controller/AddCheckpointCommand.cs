﻿
using Assets.Scripts.game.levels.checkpoints.model;
using Assets.Scripts.game.model;
using strange.extensions.command.impl;

namespace Assets.Scripts.game.levels.checkpoints.controller
{
    public class AddCheckpointCommand: Command
    {
        [Inject]
        public ICheckpoint Checkpoint { get; set; }
        [Inject]
        public LevelStateModel CurrentLevelModel { get; set; }

        public override void Execute()
        {
            CurrentLevelModel.AddCheckpoint(Checkpoint);
        }
    }
}
