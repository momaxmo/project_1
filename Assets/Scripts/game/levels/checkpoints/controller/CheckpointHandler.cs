﻿ 

using Assets.Scripts.game.levels.checkpoints.model;
using Assets.Scripts.game.model;
using strange.extensions.command.impl;

namespace Assets.Scripts.game.levels.checkpoints.controller
{
    /// <summary>
    /// When a checkpoint is triggered, this command gets called. It then disables the checkpoints gameobject
    /// </summary>
   public  class CheckpointHandler : Command
    {
       [Inject]
       public ICheckpoint Checkpoint { get; set; }
       [Inject]
       public LevelStateModel CurrentLevelModel { get; set; }

       public override void Execute()
       {
           //deactivate the check points game object
           Checkpoint.Transform.gameObject.SetActive(false);
           CurrentLevelModel.PreviousCheckpoint = Checkpoint;
       }
    }
}
