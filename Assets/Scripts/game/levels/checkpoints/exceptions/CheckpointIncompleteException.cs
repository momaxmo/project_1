﻿using System;
 
namespace Assets.Scripts.game.levels.checkpoints.exceptions
{
    public class CheckpointIncompleteException: Exception
    {
        public CheckpointIncompleteException()
        {
        }

        public CheckpointIncompleteException(string message) : base(message)
        {
        }
    }
 
}
