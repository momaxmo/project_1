﻿
using Assets.Scripts.game.levels.checkpoints.exceptions;
using Assets.Scripts.tools;
using UnityEngine;

namespace Assets.Scripts.game.levels.checkpoints.model
{
    public class ConcreteCheckpoint : ICheckpoint
    {
        private string _name;
        public ConcreteCheckpoint(string name, Transform transform,Collider2D collider2D)
        {
            Name = name;
            Transform = transform; 
            Collider2D = collider2D;
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                if (ExitTheVoidTools.StringIgnoreCaseContains(_name, "start"))
                {
                    CheckpointType = CheckpointType.StartLevel;
                }
                if (ExitTheVoidTools.StringIgnoreCaseContains(_name, "finish"))
                {
                    CheckpointType = CheckpointType.FinishLevel;
                }
            }
        }

        public Transform Transform { get; set; }

        public bool PlayerPassedBy { get; set; }

        public CheckpointType CheckpointType
        {
            get;

            set;
        }

        public Collider2D Collider2D { get; set; }
        /// <summary>
        /// Checks to see if any of the fields are null 
        /// </summary>
        /// <returns></returns>
        public void  ValidateCheckpoint()
        {
            if (Collider2D == null || Name == null || Name.Length == 0 || Transform == null)
            {
                throw new CheckpointIncompleteException();
                
            }
        }
    }
}
