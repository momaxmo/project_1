﻿

using UnityEngine;

namespace Assets.Scripts.game.levels.checkpoints.model
{
    public interface ICheckpoint
    {
        string Name { get; set; }
        Transform Transform { get; set; }
        bool PlayerPassedBy { get; set; }
        CheckpointType CheckpointType { get; set; }

        Collider2D Collider2D { get; set; }

        void ValidateCheckpoint();
    }
    /// <summary>
    /// Describes the type of checkpoint
    /// </summary>
    public enum CheckpointType
    {
        Regular,
        StartLevel,
        FinishLevel
        
    }
}
