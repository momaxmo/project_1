﻿
using Assets.Scripts.game.levels.checkpoints.model;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Assets.Scripts.game.levels.checkpoints.view
{
    public class CheckpointView: View
    {
        private ConcreteCheckpoint _checkpoint;
        public Signal<ICheckpoint> AddNewCheckpointSignal = new Signal<ICheckpoint>();
        public void Init()
        {
            _checkpoint = new ConcreteCheckpoint(name,transform,GetComponent<Collider2D>());
            AddNewCheckpointSignal.Dispatch(_checkpoint);
        }
    }
}
