﻿  
 
using Assets.Scripts.game.levels.checkpoints.model;
using strange.extensions.mediation.impl;

namespace Assets.Scripts.game.levels.checkpoints.view
{
    /// <summary>
    /// Responsibilities of this class: on view initialization, dispatch a signal to add the checkpoint to the level model
    /// on trigger hit: dispatch a signal to the context that a PlayerModel has hit the checkpoint
    /// </summary>
   public  class CheckpointViewMediator: Mediator
    {
       [Inject]
       public CheckpointView CheckpointView { get; set; }
       [Inject]
       public AddCheckpointSignal AddCheckpointSignal { get; set; }

       public override void OnRegister()
       {
           CheckpointView.AddNewCheckpointSignal.AddListener(DispatchNewCheckpointSignal);
           CheckpointView.Init();
           
       }

       public override void OnRemove()
       {
           CheckpointView.AddNewCheckpointSignal.RemoveListener(DispatchNewCheckpointSignal);
       }

       private void DispatchNewCheckpointSignal(ICheckpoint checkpoint)
       {
           AddCheckpointSignal.Dispatch(checkpoint);
       }
    }
}
