﻿ 
using Assets.Scripts.common.config;
using Assets.Scripts.common.view.camera;
using Assets.Scripts.game.input.controller;
using Assets.Scripts.game.model;
using Assets.Scripts.game.model.levels;
using Assets.Scripts.game.view.environment; 
using Assets.Scripts.tools;
using strange.extensions.command.impl;
using UnityEngine;

namespace Assets.Scripts.game.levels.controller.command
{
    public class Level1StartCommand: Command
    {
        [Inject]	//This injection simply instantiates the game input
        public IInput Input { get; set; }
         

        private Level1Model _level1Model;
        private LevelStateModel _levelStateModel;
        [Inject]
        public UpdateCameraFollowTarget CamFollowTargetSignal { get; set; }
            [Inject]
        public UpdateLevelBoundsSignal UpdateLevelBoundsSignal { get; set; }
        
        public override void Execute()
        {
            Debug.Log("execution in "+GetType());

            if (injectionBinder.GetBinding<Level1Model>() == null)
            {
                _level1Model = new Level1Model();
                injectionBinder.Bind<Level1Model>().ToValue(_level1Model).ToSingleton();
            }
           // Debug.Log(input.Equals("f"));
            if (injectionBinder.GetBinding<GameObject>(GameElement.MainCamera) == null)
            {
                GameObject cam = Camera.main.gameObject;
                _level1Model.MainPlayer = cam;
                CameraFollow camFollower = cam.GetComponent<CameraFollow>();
                if (camFollower == null)
                {
                    cam.AddComponent<CameraFollow>();
                }
                injectionBinder.Bind<GameObject>().ToValue(cam).ToName(GameElement.MainCamera);
            }
       
            if (injectionBinder.GetBinding<GameObject>(GameElement.MainPlayer) == null)
            {
                GameObject go = GameObject.FindWithTag("PlayerModel");
                injectionBinder.Bind<GameObject>().ToValue(go).ToName(GameElement.MainPlayer);
                _level1Model.MainPlayer = go;
            CamFollowTargetSignal.Dispatch(go.transform);
 
        
            }
            if (injectionBinder.GetBinding<LevelBounds>(GameElement.LevelLimits) == null)
            {
                GameObject go = GameObject.FindWithTag("LevelBounds");
                LevelBounds levelBounds = go.GetComponent<LevelBounds>();
                injectionBinder.Bind<LevelBounds>().ToValue(levelBounds).ToName(GameElement.LevelLimits);
                _level1Model.LevelLimits = levelBounds;
                UpdateLevelBoundsSignal.Dispatch(levelBounds); 
            }
            /*if (injectionBinder.GetInstance<LevelStateModel>() == null)//game model doesn't exist
            {
                LevelStateModel levelModel = new LevelStateModel() {IsPaused = false};
                injectionBinder.Bind<LevelStateModel>().ToValue(levelModel).ToSingleton();
            }*/
        }
    }
}
