﻿
using Assets.Scripts.common.config;
using Assets.Scripts.common.view.camera;
using Assets.Scripts.game.input.controller;
using Assets.Scripts.game.inventory.Model; 
using Assets.Scripts.game.model.levels; 
using Assets.Scripts.game.view.environment; 
using Assets.Scripts.tools;
using Assets.Scripts.ui.model;
using strange.extensions.command.impl;
using UnityEngine;

namespace Assets.Scripts.game.levels.controller.command
{
    // ReSharper disable once InconsistentNaming
    public class Level1bStartCommand : Command
    {
        [Inject]	//This injection simply instantiates the game input
        public IInput Input { get; set; }


        private Level1Model _level1Model; 
        [Inject]
        public UpdateCameraFollowTarget CamFollowTargetSignal { get; set; }
        [Inject]
        public UpdateLevelBoundsSignal UpdateLevelBoundsSignal { get; set; }

        [Inject]
        public Inventory Inventory { get; set; }
        [Inject]
        public BuildInventory BuildInventorySignal { get; set; }

        [Inject]
        public PauseMenuSignal PauseMenuSignal { get; set; }
        public override void Execute()
        {
            Debug.Log("execution in " + GetType());

            if (injectionBinder.GetBinding<Level1Model>() == null)
            {
                _level1Model = new Level1Model();
                injectionBinder.Bind<Level1Model>().ToValue(_level1Model).ToSingleton();
            }
            // Debug.Log(input.Equals("f"));
            if (injectionBinder.GetBinding<GameObject>(GameElement.MainCamera) == null)
            {
                GameObject cam = Camera.main.gameObject;
                _level1Model.MainPlayer = cam;
                CameraFollow camFollower = cam.GetComponent<CameraFollow>();
                if (camFollower == null)
                {
                    cam.AddComponent<CameraFollow>();
                }
                injectionBinder.Bind<GameObject>().ToValue(cam).ToName(GameElement.MainCamera);
            }

        /*    if (injectionBinder.GetBinding<GameObject>(GameElement.MainPlayerModel) == null)
            {
                GameObject go = GameObject.FindWithTag("PlayerModel");
                injectionBinder.Bind<GameObject>().ToValue(go).ToName(GameElement.MainPlayerModel);
                _level1Model.MainPlayerModel = go;
                CamFollowTargetSignal.Dispatch(go.transform);


            }*/
            if (injectionBinder.GetBinding<LevelBounds>(GameElement.LevelLimits) == null)
            {
                GameObject go = GameObject.FindWithTag("LevelBounds");
                LevelBounds levelBounds = go.GetComponent<LevelBounds>();
                injectionBinder.Bind<LevelBounds>().ToValue(levelBounds).ToName(GameElement.LevelLimits);
                _level1Model.LevelLimits = levelBounds;
                UpdateLevelBoundsSignal.Dispatch(levelBounds);
            }

            BuildInventorySignal.Dispatch(Inventory);
            PauseMenuSignal.Dispatch(PauseMenuState.Off);
            
        }
    }
}
