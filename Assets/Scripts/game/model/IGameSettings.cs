﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.game.model
{
    interface IGameSettings
    {
        float MusicVolume { get; set; }
        float SoundFxVolume { get; set; }

    }
}
