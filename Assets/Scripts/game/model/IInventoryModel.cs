﻿using System.Collections.Generic;
using Assets.Scripts.game.model.usables;

namespace Assets.Scripts.game.model
{
    public interface IInventoryModel
    {
        List<IUsableModel> BoosterList1 { get; set; }
        List<IUsableModel> BoosterList2 { get; set; }
    }
}
