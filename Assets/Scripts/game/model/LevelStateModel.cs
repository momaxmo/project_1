﻿
using System;
using System.Collections.Generic;
using Assets.Scripts.game.levels.checkpoints.model;
using Assets.Scripts.tools;
using LitJson;

namespace Assets.Scripts.game.model
{
    /// <summary>
    /// holds the model of the current game state as well as the level name... todo: consider refactoring this 
    /// </summary>
    public class LevelStateModel
    {
        private bool _isPaused;
        private float _minimumPauseTime = 0.75f;
        public string CurrentLevelName { get; set; }
        public string NextLevelName { get; set; }
        public string PreviousLevelName { get; set; }
        public int CurrentLevelNumber { get; set; }
        public bool IgnorePauseSignals { get; set; } //during certain game events, we need to ignore the pause signal
        public GameState CurrentGameState { get; set; }
        public GameState PreviousGameState { get; set; }
        [JsonIgnore]
        private List<ICheckpoint> _checkpoints;

        public ICheckpoint PreviousCheckpoint { get; set; }
        public bool IsPaused
        {
            get
            {
                return _isPaused;
            }
            set
            {
                if (value)
                {
                    PreviousGameState = CurrentGameState;
                    CurrentGameState = GameState.Paused;
                }
                else
                {
                    CurrentGameState = PreviousGameState;
                }
                _isPaused = value;
            }
        }
        /// <summary>
        /// Minimum amount of time that a PlayerModel can press pause. In otherwords, prevent mashing of the pause 
        /// button
        /// </summary>
        public float MinimumPauseTime
        {
            get { return _minimumPauseTime; }
            set { _minimumPauseTime = value; }
        }
        [JsonIgnore]
        public List<ICheckpoint> Checkpoints
        {
            get
            {
                if (_checkpoints == null)
                {
                    _checkpoints = new List<ICheckpoint>(3);
                }
                return _checkpoints;
            }
            set { _checkpoints = value; }
        }

        public void AddCheckpoint(ICheckpoint checkpoint)
        {
            checkpoint.ValidateCheckpoint();
            Checkpoints.Add(checkpoint);

        }
        /// <summary>
        /// Returns a checkpoint with the specified name
        /// </summary>
        /// <param name="check"></param>
        /// <returns></returns>
        public ICheckpoint ReturnCheckpointWithName(string check)
        {
            ICheckpoint foundItem;
            List<ICheckpoint> foundItems = Checkpoints.FindAll(x => x.Name.IndexOf(check,
                StringComparison.OrdinalIgnoreCase) >= 0);
            foundItem = foundItems.Count != 0 ? foundItems[0] : null;
                return foundItem;
        }
    }
}
