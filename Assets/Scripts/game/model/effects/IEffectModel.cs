﻿
namespace Assets.Scripts.game.model.effects
{
    /// <summary>
    /// The effects a particular item, booster has on the state of the game world or the PlayerModel
    /// Class only holds data
    /// </summary>
    public interface IEffectModel
    {
        string EffectName { get; set; }
        float Duration { get; set; }
        EffectType Type { get; set; }
        EffectDurationFlag EffectLength { get; set; }
    }

    public enum EffectType
    {
        EffectOnWorld,
        EffectOnEnemy,
        EffectOnPlayer
    }

    public enum EffectDurationFlag
    {
        Infinite,
        Limited
    }

}
