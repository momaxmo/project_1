﻿ 

namespace Assets.Scripts.game.model.effects
{
    /// <summary>
    /// Temporary speed effect, affecting the PlayerModel
    /// </summary>
    public class ShortSpeedModifierEffect: IEffectModel
    {
        public string EffectName
        {
            get { return "Speed boost"; }
            set {   }
        }

        public float Duration
        {
            get { return 3f; }
            set
            {
                
            }
        }
         
        public EffectType Type
        {
            get { return EffectType.EffectOnPlayer; }
            set
            {
            
            }
        }


        public EffectDurationFlag EffectLength
        {
            get { return EffectDurationFlag.Limited; }
            set
            {
             
            }
        }
    }
}
