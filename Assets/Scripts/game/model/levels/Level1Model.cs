﻿ 
using Assets.Scripts.game.view.environment;
using UnityEngine;

namespace Assets.Scripts.game.model.levels
{
   public  class Level1Model
    {
       public GameObject MainPlayer { get; set; }
       public Camera MainCamera { get; set; }

       public LevelBounds LevelLimits { get; set; }
    }
}
