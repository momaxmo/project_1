﻿

using Assets.Scripts.game.model.effects;

namespace Assets.Scripts.game.model.usables
{
   public interface IUsableModel
   {
       IEffectModel Effect { get; set; }

   }
}
