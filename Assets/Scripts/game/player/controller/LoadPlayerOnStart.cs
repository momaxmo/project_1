﻿ 
using Assets.Scripts.common.utilities;
using Assets.Scripts.game.model;
using Assets.Scripts.game.player.exceptions;
using Assets.Scripts.game.player.model;
using strange.extensions.command.impl;
using System.Collections;
using Assets.Scripts.common.config; 
using Assets.Scripts.Debugging.signals;
using Assets.Scripts.game.levels.checkpoints.model;
using Assets.Scripts.game.player.signals;
using Assets.Scripts.game.view.player;
using Assets.Scripts.tools;
using UnityEngine; 

namespace Assets.Scripts.game.player.controller
{
    /// <summary>
    /// Attempts to load a PlayerModel on load start, and associate the views to the player model.  The signal associated to this command is dispatched on level start
    /// </summary>
    public class LoadPlayerOnStart: Command
    {
        private float _maxTimeToLoad = 45f; //45 seconds to load
        [Inject]
        public IRoutineRunner RoutineRunner { get; set; }
        [Inject]
        public LevelStateModel LevelStateModel { get; set; }
        [Inject]
        public IPlayerModel MainPlayerModel { get; set; }
        [Inject]
        public UpdateCameraFollowTarget UpdateCameraFollowTarget { get; set; }
        [Inject]
        public SpawnPlayerSignal SpawnPlayerSignal { get; set; }

        [Inject]
        public AddPlayerToDebugView AddPlayerToDebugView { get; set; } 
        private ICheckpoint _checkPointToSpawnOn;

        public override void Execute()
        { 
            _checkPointToSpawnOn = LevelStateModel.PreviousCheckpoint;
            
            if (_checkPointToSpawnOn == null) //on load, the level state
            {
                RoutineRunner.StartCoroutine(Fetch());
            }
            else
            {
                Vector2 position = _checkPointToSpawnOn.Transform.position;
                InstantiatePlayer(position);
                SpawnPlayerSignal.Dispatch(_checkPointToSpawnOn.Transform.position);
            }
            
        }
        /// <summary>
        /// private routine called if the loaded level state model's previous check point is null. It attempts to locate a starting check point in the scene. Once found, instantiates a new PlayerModel 
        /// </summary>
        /// <returns></returns>
        private IEnumerator Fetch()
        {
            float startTime = Time.time;
            
            while (true)
            {
                if (Time.time - startTime > _maxTimeToLoad)
                {
                    throw new StartCheckpointNotFoundInSceneException(); 
                }
                else
                {
                    _checkPointToSpawnOn = LevelStateModel.ReturnCheckpointWithName("start");
                   
                    if (_checkPointToSpawnOn != null)
                    {
                        Vector2 position = _checkPointToSpawnOn.Transform.position;
                        InstantiatePlayer(position);
                        yield break;
                    }
                }
                yield return null;
            }
           
        }

        private void  InstantiatePlayer(Vector2 position)
        {
            GameObject go = Object.Instantiate(UnityEngine.Resources.Load(ExitTheVoidFilePathRefs.PlayerPrefabPath)) as GameObject;
            if (go == null)
            {
                throw new PlayerPrefabNotFoundException();
            }

            go.SetActive(false); 
            MainPlayerModel.PlayerTransform = go.GetComponent<Transform>();
           

            GameObject level1BGameObject = GameObject.Find("Level1b"); //todo: fix level names
            //set the players parent's transform
            MainPlayerModel.PlayerTransform.parent = level1BGameObject.transform;
            MainPlayerModel.PlayerBehaviourView = go.GetComponent<IPlayerBehaviourView>();
            MainPlayerModel.PlayerControllerView = go.GetComponent<IPlayerControllerView>(); 
            
            SpawnPlayerSignal.Dispatch(position);
            AddPlayerToDebugView.Dispatch(MainPlayerModel as MainPlayerModel);
 
             
        }
    }
}
