﻿using Assets.Scripts.common.config;
using Assets.Scripts.game.player.model;
using Assets.Scripts.game.view.player;
using strange.extensions.command.impl;
using UnityEngine;

namespace Assets.Scripts.game.player.controller
{
    /// <summary>
    /// spawns a PlayerModel at the targeted position,  activates the PlayerModel and places him in the targetted position
    /// </summary>
    public class SpawnPlayer : Command
    {
        [Inject]public Vector2 TargettedPosition{ get; set; }

        [Inject]
        public IPlayerModel PlayerModel { get; set; }
        [Inject]
        public UpdateCameraFollowTarget UpdateCameraFollowTarget { get; set; }

        public override void Execute()
        { 
            
            PlayerModel.PlayerTransform.position = TargettedPosition;
            //PlayerModel.View = PlayerModel.PlayerTransform.gameObject.AddComponent<View>();
           //  PlayerModel.PlayerBehaviourView = PlayerModel.PlayerTransform.gameObject.AddComponent<PlayerBehaviourView>();

            PlayerModel.PlayerTransform.gameObject.SetActive(true);

            UpdateCameraFollowTarget.Dispatch(PlayerModel.PlayerTransform);
        }
    }
}
