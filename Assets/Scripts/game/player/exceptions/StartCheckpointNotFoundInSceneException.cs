﻿using System;


namespace Assets.Scripts.game.player.exceptions
{
    /// <summary>
    /// Exception should be thrown if a starting check point isn't located
    /// </summary>
    public class StartCheckpointNotFoundInSceneException: Exception
    {
        public StartCheckpointNotFoundInSceneException()
        {
        }

        public StartCheckpointNotFoundInSceneException(string message) : base(message)
        {
        }
    }

    public class PlayerPrefabNotFoundException : Exception
    {
        public PlayerPrefabNotFoundException()
        {
        }

        public PlayerPrefabNotFoundException(string message) : base(message)
        {
        }
    }
}
