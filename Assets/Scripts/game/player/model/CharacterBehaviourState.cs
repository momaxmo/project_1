﻿

using System;
using Assets.Scripts.Debugging;
using Assets.Scripts.tools;
using LitJson;

namespace Assets.Scripts.game.player.model
{
    public class CharacterBehaviourState : IPlayerBehaviourState, IDebuggable
    {
        
 
        public bool CanJump { get; set; }
        public bool CanDash { get; set; }
        public bool CanDive { get; set; }
        private bool _canUseBooster;
        private bool _previousBooster1State, _previousBooster2State;
        public bool CanUseBoosters
        {
            get
            {
                return _canUseBooster;
            }
            set
            {
                if (!value)
                {
                    _previousBooster1State = CanUseBooster1;
                    _previousBooster2State = CanUseBooster2;
                    CanUseBooster2 = CanUseBooster1 = false;
                }
                else
                {
                    CanUseBooster1 = _previousBooster1State;
                    CanUseBooster2 = _previousBooster2State;
                }
                _canUseBooster = value;
            }
        }

        public bool CanUseBooster1 { get; set; }
        public bool CanUseBooster2 { get; set; }
        public bool CanMelee { get; set; }
        public bool IsMeleeing { get; set; }
        public bool CanRanged { get; set; }
        public bool CanMoveFreely { get; set; }
        public bool IsDead { get; set; }
        public int NumberOfJumpsLeft { get; set; }
        public bool IsRunning { get; set; }
        public bool PreviouslyCrouching { get; set; }
        public bool IsCrouching { get; set; }
        public bool LookingUp { get; set; }
        public bool IsUsingBooster1 { get; set; }
        public bool StoppedUsingBooster1 { get; set; }
        public bool StoppedUsingBooster2 { get; set; }
        public bool IsUsingBooster2 { get; set; }

        public bool IsCollidingWithLadder { get; set; }
        public bool IsCollidingWithLadderTop { get; set; }
        public bool IsClimbingLadder { get; set; }
        public float Booster1DurationLeft { get; set; }
        public float Booster2DurationLeft { get; set; }
        public bool InDialogArea { get; set; }

        public bool IsDashing { get;set; }
         

        [JsonIgnore]
        private string _id;
            [JsonIgnore]
        public string Id
        {
            get
            {
                if (string.IsNullOrEmpty(_id))
                {

                    _id = Guid.NewGuid().ToString();
                }
                return _id;
            } 
        }

        public void Initialize()
        {
            CanMoveFreely = CanJump = CanUseBoosters = CanMelee = CanRanged = true;
            IsRunning = IsCrouching = PreviouslyCrouching =
                LookingUp =
                    IsClimbingLadder =
                        IsCollidingWithLadder = IsCollidingWithLadderTop = IsUsingBooster1 = IsUsingBooster2
                            =
                            InDialogArea =
                                IsMeleeing = CanRanged = IsDead = StoppedUsingBooster1 = StoppedUsingBooster2 = false;
        }

  

        

        public string ViewDebugText()
        {
            const string format = ExitTheVoidStringFormatting.DebugFormatTwoParams;
            const string floatFormat = ExitTheVoidStringFormatting.DebugFloatFormatTwoParams;
            string output = "Character Behaviour State +\n";
            output += String.Format(format, "CanUseBooster1? ", ExitTheVoidStringFormatting.BoolToYesNo(CanUseBooster1));
            output += "\n" + String.Format(format, "IsUsingBooster1? ", ExitTheVoidStringFormatting.BoolToYesNo(IsUsingBooster1));
            output += "\n" + String.Format(format, "StoppedUsingBooster1? ", ExitTheVoidStringFormatting.BoolToYesNo(StoppedUsingBooster1));

            output += "\n" + String.Format(format, "CanUseBooster2? ", ExitTheVoidStringFormatting.BoolToYesNo(CanUseBooster2));
            output += "\n" + String.Format(format, "IsUsingBooster2? ", ExitTheVoidStringFormatting.BoolToYesNo(IsUsingBooster2));
            output += "\n" + String.Format(format, "StoppedUsingBooster2? ", ExitTheVoidStringFormatting.BoolToYesNo(StoppedUsingBooster2));
            output += "\n" + String.Format(format, "IsCrouching? ", ExitTheVoidStringFormatting.BoolToYesNo(IsMeleeing));
            
            output += "\n" + String.Format(format, "IsMeleeing? ", ExitTheVoidStringFormatting.BoolToYesNo(IsMeleeing));
            output += "\n" + String.Format(format, "CanRanged? ", ExitTheVoidStringFormatting.BoolToYesNo(CanRanged));
            output += "\n" + String.Format(format, "CanMoveFreely? ", ExitTheVoidStringFormatting.BoolToYesNo(CanMoveFreely));

            output += "\n" + String.Format(format, "IsDead? ", ExitTheVoidStringFormatting.BoolToYesNo(IsDead));
            output += "\n" + String.Format(format, "IsRunning? ", ExitTheVoidStringFormatting.BoolToYesNo(IsRunning));
            output += "\n" + String.Format(format, "PreviouslyCrouching? ", ExitTheVoidStringFormatting.BoolToYesNo(PreviouslyCrouching));
            output += "\n" + String.Format(format, "LookingUp? ", ExitTheVoidStringFormatting.BoolToYesNo(LookingUp));
            output += "\n" + String.Format(format, "IsCollidingWithLadder? ", ExitTheVoidStringFormatting.BoolToYesNo(IsCollidingWithLadder));
            output += "\n" + String.Format(format, "IsCollidingWithLadderTop? ", ExitTheVoidStringFormatting.BoolToYesNo(IsCollidingWithLadderTop));
            output += "\n" + String.Format(format, "IsClimbingLadder? ", ExitTheVoidStringFormatting.BoolToYesNo(IsClimbingLadder));
            output += "\n" + String.Format(format, "InDialogArea? ", ExitTheVoidStringFormatting.BoolToYesNo(InDialogArea));


            output += "\n" + String.Format(format, "NumberOfJumpsLeft? ", NumberOfJumpsLeft);
            output += "\n" + String.Format(floatFormat, "Booster1DurationLeft? ", Booster1DurationLeft);
            output += "\n" + String.Format(floatFormat, "Booster2DurationLeft? ", Booster2DurationLeft);
            output += "\n" + String.Format(format, "CanMoveFreely? ", ExitTheVoidStringFormatting.BoolToYesNo(CanMoveFreely));
            output += "\n" + String.Format(format, "CanMoveFreely? ", ExitTheVoidStringFormatting.BoolToYesNo(CanMoveFreely));


            return output;
        }
        protected bool Equals(CharacterBehaviourState other)
        {
            return string.Equals(Id, other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((CharacterBehaviourState)obj);
        }

        public override int GetHashCode()
        {
            int hash = 0;
            if (Id != null)
            {
                hash = Id.GetHashCode();
            }
            return hash;
        }

        public static bool operator ==(CharacterBehaviourState left, CharacterBehaviourState right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(CharacterBehaviourState left, CharacterBehaviourState right)
        {
            return !Equals(left, right);
        }
      

    }
}
