﻿using System;
using System.Xml.Serialization;
using UnityEngine;

namespace Assets.Scripts.game.player.model
{
    public interface ICharacterStatsAndAttributes
    {
        string Name { get; set; }

        BonusStatsMod BonusStats { get; set; }
        #region offensive Attributes

        
          int Level { get;set; }
          int BaseAttackPower { get; set; }
         // int BonusAttackPower { get; set; }
          float BaseAccuracy { get; set; }
     //   float BonusAccuracy { get; set; }
 
          float BaseCritChance { get; set; }
       //   float BonusCritChance { get; set; }
          float BaseChanceToStun { get; set; }//on stun , flash of light
        //  float BonusChanceToStun { get; set; }
        #endregion

        #region Defensive attributes

          int BaseHealthPool { get; set; }
     
          int BaseAgility { get; set; }
      
           float BaseDefence { get; set; }
        //   float BonusDefence { get; set; }
        #endregion
           int CurrentDamageTaken { get; set; }
        /// <summary>
        /// What is the current health  of the player? healthpool - currentdamage taken
        /// 
        /// </summary>
           int CurrentHealth { get; }
        /// <summary>
        /// Resets the damage taken to 0
        /// </summary>
        void ResetDamage();

    }
    [Serializable]
    public class BonusStatsMod
    {
        [XmlEnum(Name = "BonusAttackPower"), SerializeField]

        public int BonusAttackPower;
        [XmlEnum(Name = "BonusAccuracy"), SerializeField]

        public float BonusAccuracy;
        [XmlEnum(Name = "BonusCritChance"), SerializeField]

        public float BonusCritChance;
        [XmlEnum(Name = "BonusChanceToStun"), SerializeField]

        public float BonusChanceToStun;
        [XmlEnum(Name = "BonusHealthPool"), SerializeField]

        public int BonusHealthPool;
        [XmlEnum(Name = "BonusAgility"), SerializeField]

        public int BonusAgility;
        [XmlEnum(Name = "BonusDefence"),SerializeField]

        public float BonusDefence;

        public BonusStatsMod()
        {
            Reset();
        }
        /// <summary>
        /// Resets all fields to zero
        /// </summary>
        public void Reset()
        {
            BonusAttackPower = BonusHealthPool = BonusAgility = 0;
            BonusAccuracy = BonusCritChance = BonusChanceToStun = BonusDefence = 0;

        }
    }
}
