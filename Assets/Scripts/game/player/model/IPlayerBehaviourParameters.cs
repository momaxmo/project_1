﻿ 

namespace Assets.Scripts.game.player.model
{
   /// <summary>
   /// IPlayerbehaviour parameters: these parameters dictact how the character behaves in game.
   /// </summary>
    public interface IPlayerBehaviourParameters
    {
        JumpingRules JumpRules { get; set; }
        DashingRules DashingRules { get; set; }
        bool SmoothMovement { get; set; }
        float JumpHeight { get; set; }
        float MinimumJumpAirTime { get; set; }
        float JumpAirTime { get; set; }
        int ExtraJumps { get; set; }
        int ExtraJumpModifier { get; set; }

        //This afffects the speed of movement, crouching and walking
        float GlobalMovementSpeedModifier { get; set; }
        float MovementSpeed { get; set; }
        float MovementSpeedModifier { get; set; }
        float CrouchMovementSpeed { get; set; }
        float CrouchMovementSpeedModifier { get; set; }
        float WalkSpeed { get; set; }
        
        float RunSpeed { get; set; }
        float RunSpeedModifier { get; set; }
        float LadderClimbSpeed { get; set; }
        float LadderClimSpeedModifier { get; set; }

        float DashingDuration { get; set; }
        float DashingSpeedModifier { get; set; }
        float DashForce { get; set; }
        float DashForceModifier { get; set; }
       
        float DashCooldown { get; set; }
        float DashCooldownModifier { get; set; }
 

        bool JumpProportionalToPressTime { get; set; }
    }

    public enum JumpingRules
    {
        CanJumpOnGround,
        CanJumpAnyWhere,
        CantJump,
        CanJumpAnyWhereAnynumberOfTimes
    }

    public enum DashingRules
    {
        CantDash,
        CanDashOnlyOnGround,
        CanDashAnywhere
    }
}
