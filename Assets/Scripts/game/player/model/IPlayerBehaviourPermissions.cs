﻿ 
namespace Assets.Scripts.game.player.model
{
    public interface IPlayerBehaviourPermissions
    {
        bool CanRun { get; set; }
        bool CanDash { get; set; }
        bool CanCrouch { get; set; }
        bool CanJump { get; set; }
        bool CanUseRanged { get; set; }
        bool CanMeleeAttack { get; set; }
        bool CanUseBoosters { get; set; }
        bool CanUseBooster1 { get; set; }
        bool CanUseBooster2 { get; set; }

       string ToString();
    }
}
