﻿ 
namespace Assets.Scripts.game.player.model
{
    public interface IPlayerBehaviourState
    {
          bool CanJump { get; set; }


        bool CanUseBoosters { get; set; }
          bool CanDive { get; set; }
          bool CanUseBooster1 { get; set; }
          bool CanUseBooster2 { get; set; }
          bool CanMelee { get; set; }
          bool IsMeleeing { get; set; }
          bool CanRanged { get; set; }
          bool CanMoveFreely { get; set; }
          bool IsDead { get; set; }
          int NumberOfJumpsLeft { get; set; }
          bool IsRunning { get; set; }
          bool PreviouslyCrouching { get; set; }
          bool IsCrouching { get; set; }
          bool LookingUp { get; set; }
          bool IsUsingBooster1 { get; set; }
          bool StoppedUsingBooster1 { get; set; }
          bool StoppedUsingBooster2 { get; set; }
          bool IsUsingBooster2 { get; set; }

          bool IsCollidingWithLadder { get; set; }
          bool IsCollidingWithLadderTop { get; set; }
          bool IsClimbingLadder { get; set; }
          float Booster1DurationLeft { get; set; }
          float Booster2DurationLeft { get; set; }
          bool InDialogArea { get; set; }
          bool IsDashing { get; set; }
        bool CanDash { get; set; }
        void Initialize();

    }
}
