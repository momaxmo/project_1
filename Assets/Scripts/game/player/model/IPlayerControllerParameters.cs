﻿ 
using UnityEngine;

namespace Assets.Scripts.game.player.model{
    public interface IPlayerControllerParameters
    {
        Vector2 MaxVelocity { get; set; }
        float MaxSpeed { get; set; }

        float MaximumSlopeAngle { get; set; }
        float Gravity { get; set; }
        float GroundAcceleration { get; set; }
        float AirAcceleration { get; set; }
        float MaxSpeedModifier { get; set; }
        int ExtraJumps { get; set; }
        int ExtraJumpModifier { get; set; }
    }
}
