﻿ 

namespace Assets.Scripts.game.player.model
{
   public interface IPlayerControllerState
    {
      
          bool IsCollidingLeft { get; set; }
        bool IsCollidingRight { get; set; }
          bool IsCollidingAbove { get; set; }
          bool IsCollidingBelow { get; set; }
 
          bool IsMovingDownSlope { get; set; }
          bool IsMovingUpSlope { get; set; }
          bool SlopeAngleOK { get; set; }
          float SlopeAngle { get; set; }

        /// <summary>
        /// is the character grounded?
        /// </summary>
          bool IsGrounded { get; }

          bool WasGroundedLastFrame { get; set; }
          bool JustGotGrounded { get; set; }
          bool IsFalling { get; set; }

       void Reset(); 
    }
}
