﻿ 

using Assets.Scripts.game.inventory.Model; 
using Assets.Scripts.game.view.player;
using Assets.Scripts.game.view.player.interactions;
using UnityEngine;

namespace Assets.Scripts.game.player.model
{
    /// <summary>
    /// interface for a PlayerModel model
    /// </summary>
    public interface IPlayerModel 
    {
        int Health { get; set; }
       Inventory Inventory { get; set; }
        IPlayerControllerView PlayerControllerView { get; set; }
        IPlayerBehaviourView PlayerBehaviourView { get; set; }
        IPlayerBehaviourState  PlayerBehaviourState { get;set; }
        IPlayerBehaviourParameters PlayerBehaviourParameters{get; set; }
        IPlayerBehaviourPermissions PlayerBehaviourPermissions{get; set; }
        IPlayerControllerState PlayerControllerState { get; set; }
        IPlayerControllerParameters PlayerControllerParameters { get; set; } 
          Transform PlayerTransform { get; set; }

          ICharacterStatsAndAttributes PlayerStats { get; set; } 
         
          string Name { get; set; }

          IBooster Booster1 { get; set; }
          IBooster Booster2 { get; set; }
    }
}
