﻿

using Assets.Scripts.game.boosters.model;
using Assets.Scripts.game.inventory.Model;
using Assets.Scripts.game.view.player;
using Assets.Scripts.game.view.player.interactions;
using UnityEngine;

namespace Assets.Scripts.game.player.model
{
    /// <summary>
    /// model for holding PlayerModel data
    /// </summary>
    public class MainPlayerModel : IPlayerModel
    {
        private Inventory _inventory;
        private ICharacterStatsAndAttributes _playerStats;
        private string _name = "";
        private IBooster _booster1, _booster2;
        private IPlayerBehaviourState _playerBehaviourState;
        private IPlayerBehaviourParameters _playerBehaviourParameters;
        private IPlayerBehaviourPermissions _playerBehaviourPermissions;
        private IPlayerControllerState _playerControllerState;
        private IPlayerControllerParameters _playerCnControllerParameters;
        public IPlayerControllerView PlayerControllerView { get; set; }
        public IPlayerBehaviourView PlayerBehaviourView { get; set; }
        public MainPlayerModel()
        { }

        public MainPlayerModel(MainPlayerModel referenceModel)
        {
            _inventory = referenceModel.Inventory;
            _playerStats = referenceModel.PlayerStats;
            _booster1 = referenceModel.Booster1;
            _booster2 = referenceModel.Booster2;
            _playerBehaviourState = referenceModel.PlayerBehaviourState;
            _playerBehaviourPermissions = referenceModel.PlayerBehaviourPermissions;
            _playerControllerState = referenceModel.PlayerControllerState;
            _playerCnControllerParameters = referenceModel.PlayerControllerParameters;
            PlayerControllerView = referenceModel.PlayerControllerView;
            PlayerBehaviourView = referenceModel.PlayerBehaviourView;
            _playerBehaviourParameters = referenceModel.PlayerBehaviourParameters;
        }
        public MainPlayerModel(BoosterDb db)
        {
            _booster1 = new Booster(db);
            _booster2 = new Booster(db);
        }
        public int Health { get; set; }

        public Inventory Inventory
        {
            get { return _inventory ?? (_inventory = new Inventory()); }
            set { _inventory = value; }
        }

     

        public IPlayerBehaviourState PlayerBehaviourState
        {
            get
            {
                if (_playerBehaviourState == null)
                {
                    _playerBehaviourState = new CharacterBehaviourState();
                    _playerBehaviourState.Initialize();
                }
                return _playerBehaviourState;
            }
            set { _playerBehaviourState = value; }
        }


        public IPlayerBehaviourParameters PlayerBehaviourParameters
        {
            get
            {
                if (_playerBehaviourParameters == null)
                {
                    _playerBehaviourParameters = new PlayerBehaviourParameters();
                }
                return _playerBehaviourParameters;
            }
            set { _playerBehaviourParameters = value; }
        }
       
        public IPlayerBehaviourPermissions PlayerBehaviourPermissions
        {
            get
            {
                if (_playerBehaviourPermissions == null)
                {
                    _playerBehaviourPermissions = new PlayerBehaviourPermissions();
                }
                return _playerBehaviourPermissions;
            }
            set { _playerBehaviourPermissions = value; }
        }
        
        public IPlayerControllerState PlayerControllerState
        {
            get
            {
                if (_playerControllerState == null)
                {
                    _playerControllerState = new PlayerControllerState();
                }
                return _playerControllerState; 
            }
            set { _playerControllerState = value; }
        }

       
        public IPlayerControllerParameters PlayerControllerParameters
        {
            get
            {
                if (_playerCnControllerParameters == null)
                {
                    _playerCnControllerParameters = new PlayerControllerParameters();
                }
                return _playerCnControllerParameters;
            }
            set { _playerCnControllerParameters = value; }
        }

        public Transform PlayerTransform { get; set; }
        public ICharacterStatsAndAttributes PlayerStats
        {
            get { return _playerStats ?? (_playerStats = new PlayerAttributesAndStats()); }
            set { _playerStats = value; }
        }


        public string Name
        {
            get
            {
                if (_name.Length == 0)
                {
                    _name = "Joey";
                }
                return _name;
            }
            set { _name = value; }
        }

        public IBooster Booster1
        {
            get { return _booster1; }
            set { _booster1 = value; }
        }

        public IBooster Booster2
        {
            get { return _booster2; }
            set { _booster2 = value; }
        }

    }

}
