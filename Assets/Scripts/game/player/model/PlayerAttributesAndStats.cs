﻿using System;
using Assets.Scripts.Debugging;
using Assets.Scripts.tools;
using LitJson;
using Random = UnityEngine.Random;


namespace Assets.Scripts.game.player.model
{
  

    /// <summary>
    /// Holds the current stats of the PlayerModel
    /// </summary>
    /// 
    [Serializable]
    public class PlayerAttributesAndStats : ICharacterStatsAndAttributes, IDebuggable
    {

        public PlayerAttributesAndStats()
        {
            BaseAccuracy = 0.8f;
             
        }
        private string _name;
        private int _level;
     #region offensive Attributes
        private int _attackPower;

        private int _baseAttackPower = 2;
        private float _critVariance = 0.01f;
       //keen eye in game
        private float _baseAccuracy; 

        private float _baseCritChance=0.01f;
        private float _baseChanceToStun=0.01f;//on stun , flash of light
        [JsonIgnore,NonSerialized]
        private string _id;

        [JsonIgnore, NonSerialized] private int _currentDamageTaken;
        [JsonIgnore]
        public string Id
        {
            get
            {
                if (string.IsNullOrEmpty(_id))
                {

                    _id = Guid.NewGuid().ToString();
                }
                return _id;
            }
        }
       
        #endregion

        #region Defensive attributes

        private int _baseHealthPool=30;
        private int _baseAgility=1;
        private float _shadowStepChance ;//shadow step into the darkness for the enemies to miss you completely
        private float _baseDefence ;

        #endregion

        private float _luck;
        private BonusStatsMod _bonusStatsMod;
        public int Level
        {
            get { return _level; }
            set { _level = value; }
        }

        public int BaseAttackPower
        {
            get
            {
                float chanceToCrit = Random.Range(0,1);
                int attackpowerWithCrit = _attackPower;
                if(chanceToCrit > BaseCritChance )
                {
                    attackpowerWithCrit = attackpowerWithCrit*2 ;
                }
                return _baseAttackPower + _level + attackpowerWithCrit;
                //_attackPower; 
            }
            set { _attackPower = value; }
        }

        public float BaseAccuracy
        {
            get { return _baseAccuracy; }
            set { _baseAccuracy = value; }
        }

        public float BaseCritChance
        {
            get { return _baseCritChance; }
            set { _baseCritChance = value; }
        }

        public float BaseChanceToStun
        {
            get { return _baseChanceToStun; }
            set { _baseChanceToStun = value; }
        }

        public int BaseHealthPool
        {
            get { return _baseHealthPool  ; }
            set { _baseHealthPool = value; }
        }

        public int CurrentHealth
        {
            get { return BaseHealthPool - CurrentDamageTaken; }
        }
        public int BaseAgility
        {
            get { return _baseAgility; }
            set { _baseAgility = value; }
        }

        public float ShadowStepChance
        {
            get { return _shadowStepChance; }
            set { _shadowStepChance = value; }
        }

        public float BaseDefence
        {
            get { return _baseDefence; }
            set { _baseDefence = value; }
        }

        public float Luck
        {
            get { return _luck; }
            set { _luck = value; }
        }



        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public BonusStatsMod BonusStats
        {
            get
            {
                if (_bonusStatsMod == null)
                {
                    _bonusStatsMod= new BonusStatsMod();
                }
                return _bonusStatsMod; 
            }
            set { _bonusStatsMod = value; }
        }
        /// <summary>
        /// How much damage has the player taken? 
        /// </summary>
        public int CurrentDamageTaken
        {
            get { return _currentDamageTaken; }
            set { _currentDamageTaken = value; }
        }

        public void ResetDamage()
        {
            CurrentDamageTaken = 0;
        }

        public string ViewDebugText()
        {
            string output = "Player stats";
            output += "\n";

            const string format = ExitTheVoidStringFormatting.DebugFormatTwoParams;
            const string floatformat = ExitTheVoidStringFormatting.DebugFloatFormatTwoParams;
            output += "\n" + String.Format(format, "Name?: ", Name); 
            output += "\n" + String.Format(format, "Level?:", Level);
            output += "\n" + String.Format(format, "BaseAttackPower?:", BaseAttackPower);
            output += "\n" + String.Format(floatformat, "BaseAccuracy?: ", BaseAccuracy);
            output += "\n" + String.Format(floatformat, "BaseCritChance?: ", BaseCritChance);
            output += "\n" + String.Format(floatformat, "BaseChanceToStun?: ", BaseChanceToStun);
            output += "\n" + String.Format(format, "BaseHealthPool?: ", BaseHealthPool);
            output += "\n" + String.Format(format, "BaseAgility?: ", BaseAgility);
            output += "\n" + String.Format(floatformat, "ShadowStepChance?: ", ShadowStepChance);
            output += "\n" + String.Format(floatformat, "BaseDefence?: ", BaseDefence);
            output += "\n" + String.Format(floatformat, "Luck?: ", Luck);

            output += "\n" + "Maybe put in bonus stats?";

            return output;
        }
        protected bool Equals(PlayerAttributesAndStats other)
        {
            return string.Equals(Id, other.Id);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode() : 0);
        }

        public static bool operator ==(PlayerAttributesAndStats left, PlayerAttributesAndStats right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PlayerAttributesAndStats left, PlayerAttributesAndStats right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PlayerAttributesAndStats)obj);
        }
    }

 
    

}
