﻿using System;
using Assets.Scripts.Debugging;
using Assets.Scripts.tools;
using LitJson;
using UnityEngine;

namespace Assets.Scripts.game.player.model
{
    [Serializable]
    public class PlayerBehaviourParameters : IPlayerBehaviourParameters, IDebuggable
    {
       

        #region _fields
        [JsonIgnore]
        private string _id;
        [Header("Control Type")]
        private bool _smoothMovement = true;
        [Header("Jump")]
        private float _jumpHeight = 3.025f;

        private float _minimumJumpAirTime = 0.1f;
        [SerializeField]
        private int _extraJumps;

        [SerializeField]
        private int _extraJumpsMod;
        [SerializeField]
        private float _globalMovementSpeedModifier;

        [SerializeField]
        private float _movementSpeed = 10f;

        [SerializeField]
        private float _movementSpeedModifier;
        [SerializeField]
        private float _crouchMovementSpeed=3f;
        [SerializeField]
        private float _crouchMovementSpeedModifier;
        [SerializeField]
        private float _walkSpeed=10f;
        [SerializeField]
        private float _walkSpeedModifier;
        [SerializeField]
        private float _runSpeed=10f;
        [SerializeField]
        private float _runSpeedModifier;
        [SerializeField]
        private float _ladderClimbingSpeed=7f;
        [SerializeField]
        private float _ladderClimbingSpeedModifier;
        [SerializeField]
        private float _dashForce=15f;
        [SerializeField]
        private float _dashForceModifier;

        [SerializeField]
        private float _dashCooldown=1f;
        [SerializeField]
        private float _dashCooldownModifier;
 

        [SerializeField]
        private JumpingRules _jumpRules = JumpingRules.CanJumpAnyWhere;

        [SerializeField] private DashingRules _dashingRules = DashingRules.CanDashAnywhere;
        [SerializeField] private float _dashingDuration = 0.25f;
         [SerializeField]
        private float _dashingSpeedModifier;
        #endregion

        #region properties
        public bool SmoothMovement
        {
            get { return _smoothMovement; }
            set { _smoothMovement = value; }
        }

        public float JumpHeight
        {
            get { return _jumpHeight; }
            set { _jumpHeight = value; }
        }

        public float JumpAirTime
        {
            get { return _minimumJumpAirTime; }
            set { _minimumJumpAirTime = value; }
        }

        public int ExtraJumps
        {
            get
            {
                return _extraJumps + _extraJumpsMod;
            }
            set { _extraJumps = value; }
        }

        public int ExtraJumpModifier
        {
            get { return _extraJumpsMod; }
            set { _extraJumpsMod = value; }
        }


        /// <summary>
        /// Affects general movement speed, I.E  running, climbing, crawling
        /// </summary>
        public float GlobalMovementSpeedModifier
        {
            get { return _globalMovementSpeedModifier; }
            set { _globalMovementSpeedModifier = value; }
        }

        public float MovementSpeed
        {
            get { return _movementSpeed + MovementSpeedModifier; }
            set { _movementSpeed = value; }
        }

        public float MovementSpeedModifier
        {
            get { return _movementSpeedModifier + GlobalMovementSpeedModifier; }
            set { _movementSpeedModifier = value; }
        }

        public float CrouchMovementSpeed
        {
            get { return _crouchMovementSpeed + CrouchMovementSpeedModifier; }
            set { _crouchMovementSpeed = value; }
        }

        public float CrouchMovementSpeedModifier
        {
            get { return _crouchMovementSpeedModifier + GlobalMovementSpeedModifier; }
            set { _crouchMovementSpeedModifier = value; }
        }
        public JumpingRules JumpRules
        {
            get { return _jumpRules; }
            set { _jumpRules = value; }
        }



        public float WalkSpeed
        {
            get { return _walkSpeed + WalkSpeedModifier; }
            set { _walkSpeed = value; }
        }

        public float WalkSpeedModifier
        {
            get
            {
                return _walkSpeedModifier + GlobalMovementSpeedModifier;
            }
            set { _walkSpeedModifier = value; }
        }

        public float RunSpeed
        {
            get { return _runSpeed + RunSpeedModifier; }
            set { _runSpeed = value; }
        }

        public float RunSpeedModifier
        {
            get { return _runSpeedModifier + GlobalMovementSpeedModifier; }
            set { _runSpeedModifier = value; }
        }


        public float LadderClimbSpeed
        {
            get { return _ladderClimbingSpeed + LadderClimSpeedModifier; }
            set { _ladderClimbingSpeed = value; }
        }

        public float LadderClimSpeedModifier
        {
            get { return _ladderClimbingSpeedModifier + GlobalMovementSpeedModifier; }
            set { _ladderClimbingSpeedModifier = value; }
        }


        
         
        public float DashForce
        {
            get { return _dashForce + DashForceModifier; }
            set { _dashForce = value; }
        }

        public float DashForceModifier
        {
            get { return _dashForceModifier + GlobalMovementSpeedModifier; }
            set { _dashForceModifier = value; }
        }

        public float DashCooldown
        {
            get { return _dashCooldown + DashCooldownModifier; }
            set { _dashCooldown = value; }
        }

        public float DashCooldownModifier
        {
            get { return _dashCooldownModifier; }
            set { _dashCooldownModifier = value; }
        }

 

 


        #endregion




        public float MinimumJumpAirTime
        {
            get { return _minimumJumpAirTime; }
            set { _minimumJumpAirTime = value; }
        }

        [SerializeField]
        private bool _jumpProportionalToPressTime = true;
        public bool JumpProportionalToPressTime
        {
            get { return _jumpProportionalToPressTime; }
            set { _jumpProportionalToPressTime = value; }
        }
        [JsonIgnore]
        public string Id
        {
            get
            {
                if (string.IsNullOrEmpty(_id))
                {
                    _id = Guid.NewGuid().ToString();
                }
                return _id;
            }
           
        }

        public DashingRules DashingRules
        {
            get { return _dashingRules; }
            set { _dashingRules = value; }
        }

        public float DashingDuration
        {
            get { return _dashingDuration; }
            set { _dashingDuration = value; }
        }

        public float DashingSpeedModifier
        {
            get { return _dashingSpeedModifier; }
            set { _dashingSpeedModifier = value; }
        }

        public string ViewDebugText()
        {
            const string format = ExitTheVoidStringFormatting.DebugFormatTwoParams;
            const string floatFormat = ExitTheVoidStringFormatting.DebugFloatFormatTwoParams;
            string output = "Behaviour parameters +\n";
            output += String.Format(format,"SmoothMovement? :", ExitTheVoidStringFormatting.BoolToOnOff(SmoothMovement));
            output += "\n" + String.Format(floatFormat, "current movement speed ? ", MovementSpeed);
            output += "\n" + String.Format(format, "Jump Rules ? ", JumpRules);
            output += "\n" + String.Format(floatFormat, "MinimumJumpAirTime? ", MinimumJumpAirTime);
            output += "\n" + String.Format(format, "ExtraJumps? ", ExtraJumps);
       //     output += "\n" + String.Format(format, "ExtraJumpModifier? ", ExtraJumpModifier);

            output += "\n" + String.Format(floatFormat, "GlobalMovementSpeedModifier? ", GlobalMovementSpeedModifier);
            output += "\n" + String.Format(floatFormat, "MovementSpeedModifier? ", MovementSpeedModifier);
            output += "\n" + String.Format(floatFormat, "CrouchMovementSpeed? ", CrouchMovementSpeed);
           // output += "\n" + String.Format(floatFormat, "CrouchMovementSpeedModifier? ", CrouchMovementSpeedModifier);

            output += "\n" + String.Format(floatFormat, "WalkSpeed? ", WalkSpeed);
       //     output += "\n" + String.Format(floatFormat, "WalkSpeedModifier? ", WalkSpeedModifier);
            output += "\n" + String.Format(floatFormat, "RunSpeed? ", RunSpeed);
       //     output += "\n" + String.Format(floatFormat, "RunSpeedModifier? ", RunSpeedModifier);
            output += "\n" + String.Format(floatFormat, "LadderClimbSpeed? ", LadderClimbSpeed);
    //        output += "\n" + String.Format(floatFormat, "LadderClimSpeedModifier? ", LadderClimSpeedModifier);
            output += "\n" + String.Format(floatFormat, "DashForce? ", DashForce);
        //    output += "\n" + String.Format(floatFormat, "DashForceModifier? ", DashForceModifier);
            output += "\n" + String.Format(format, "Dash rules? ", DashingRules);
            output += "\n" + String.Format(format, "Dash duration? ", DashingDuration);
            output += "\n" + String.Format(floatFormat, "DashCooldown? ", DashCooldown); 
           // output += "\n" + String.Format(floatFormat, "DashCooldownModifier? ", DashCooldownModifier);
 
            return output; 
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PlayerBehaviourParameters)obj);
        }
        protected bool Equals(PlayerBehaviourParameters other)
        {
            return string.Equals(Id, other.Id);
        }



        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode() : 0);
        }

        public static bool operator ==(PlayerBehaviourParameters left, PlayerBehaviourParameters right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PlayerBehaviourParameters left, PlayerBehaviourParameters right)
        {
            return !Equals(left, right);
        }
    }
}
