﻿using System;
using Assets.Scripts.Debugging;
using Assets.Scripts.tools;
using LitJson;
using UnityEngine;

namespace Assets.Scripts.game.player.model
{
    [Serializable]

    public class PlayerBehaviourPermissions : IPlayerBehaviourPermissions, IDebuggable
    {
       

        #region fields
        [JsonIgnore]
        private string _id;
        
 
        [SerializeField]
        private bool _canRun = true;
        [SerializeField]
        private bool _canDash = true;
        [SerializeField]
        private bool _canCrouch = true;
        [SerializeField]
        private bool _canJump = true;

        [SerializeField] private bool _canUseRanged;
        [SerializeField] private bool _canMeleeAttack = true;
        [SerializeField] private bool _canUseBoosters;
        [SerializeField] private bool _canUseBooster1;
        [SerializeField] private bool _canUseBooster2;

        #endregion

        #region properties
         
        public bool CanRun
        {
            get { return _canRun; }
            set { _canRun = value; }
        }

        public bool CanDash
        {
            get { return _canDash; }
            set { _canDash = value; }
        }

        public bool CanCrouch
        {
            get { return _canCrouch; }
            set { _canCrouch = value; }
        }

        public bool CanUseRanged
        {
            get { return _canUseRanged; }
            set { _canUseRanged = value; }
        }

        public bool CanMeleeAttack
        {
            get { return _canMeleeAttack; }
            set { _canMeleeAttack = value; }
        }

        public bool CanUseBoosters
        {
            get { return _canUseBoosters; }
            set { _canUseBoosters = value; }
        }

        public bool CanUseBooster1
        {
            get { return _canUseBooster1; }
            set { _canUseBooster1 = value; }
        }

        public bool CanUseBooster2
        {
            get { return _canUseBooster2; }
            set { _canUseBooster2 = value; }
        }

        public bool CanJump
        {
            get { return _canJump; }
            set { _canJump = value; }
        }
        [JsonIgnore]
        public string Id
        {
            get
            {
                if (String.IsNullOrEmpty(_id))
                {
                    _id = Guid.NewGuid().ToString();
                }
                return _id;
            } 
        }

        #endregion

        public override string ToString()
        {
            string output = "";
           
            output += "Can Run?: ";
            output += CanRun ? "yes" : "no"  ;
            output += "\n";
            output += "Can Dash?: ";
            output += CanDash ? "yes" : "no";
            output += "\n";
            output += "Can Crouch?: ";
            output += CanCrouch ? "yes" : "no";
            output += "\n";
            output += "Can use ranged?: ";
            output += CanUseRanged ? "yes" : "no";
            output += "\n";
            output += "Can Melee?: ";
            output += CanMeleeAttack ? "yes" : "no";
            output += "\n";
            output += "Can use any boosters?: ";
            output += CanUseBoosters ? "yes" : "no";
            output += "\n";
            output += "Can use booster1?: ";
            output += CanUseBooster1 ? "yes" : "no";
            output += "\n";
            output += "Can use booster2??: ";
            output += CanUseBooster2 ? "yes" : "no";
            output += "\n";
            output += "Can jump?: ";
            output += CanJump ? "yes" : "no";
            output += "\n";

            return output;
        }

        public string ViewDebugText()
        {
            string output = "Behaviour Permissions +\n";
              const string format = ExitTheVoidStringFormatting.DebugFormatTwoParams;
            output += String.Format(format, "Can Run?:", ExitTheVoidStringFormatting.BoolToYesNo(CanRun));
            output += "\n" + String.Format(format, "Can Dash?:", ExitTheVoidStringFormatting.BoolToYesNo(CanDash));
            output += "\n" + String.Format(format, "Can Crouch?: ", ExitTheVoidStringFormatting.BoolToYesNo(CanCrouch));
            output += "\n" + String.Format(format, "Can use ranged?:", ExitTheVoidStringFormatting.BoolToYesNo(CanUseRanged));
            output += "\n" + String.Format(format, "Can use any boosters?:", ExitTheVoidStringFormatting.BoolToYesNo(CanUseBoosters));
            output += "\n" + String.Format(format, "Can use booster1?:", ExitTheVoidStringFormatting.BoolToYesNo(CanUseBooster1));
            output += "\n" + String.Format(format, "Can use booster2?:", ExitTheVoidStringFormatting.BoolToYesNo(CanUseBooster2));
            output += "\n" + String.Format(format, "Can jump?:", ExitTheVoidStringFormatting.BoolToYesNo(CanJump));
 
            return output; 
        }


        protected bool Equals(PlayerBehaviourPermissions other)
        {
            return string.Equals(Id, other._id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PlayerBehaviourPermissions)obj);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode() : 0);
        }

        public static bool operator ==(PlayerBehaviourPermissions left, PlayerBehaviourPermissions right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PlayerBehaviourPermissions left, PlayerBehaviourPermissions right)
        {
            return !Equals(left, right);
        }
    }
}
