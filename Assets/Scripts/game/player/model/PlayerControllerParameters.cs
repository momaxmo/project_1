﻿using System;
using Assets.Scripts.Debugging;
using Assets.Scripts.tools;
using LitJson;
using UnityEngine;

namespace Assets.Scripts.game.player.model
{
    /// <summary>
    /// this class will be used to determine certain parameters for the PlayerModel. The physics system
    /// </summary>
    [Serializable]
    public class PlayerControllerParameters : IPlayerControllerParameters, IDebuggable
    {
       
        private float _maxspeed;
        private float _maxSpeedModifier;
        private int _extraJumps;
        private int _extraJumpModifier;
        [Range(0, 90)] private float _maximumSlopeAngle = 45;
        private float _gravity = -15f;
        private float _groundAcceleration = 20f;
        private float _airAcceleration = 10f;
        private Vector2 _maxVelocity= new Vector2(200f,200f);


        [JsonIgnore]
        private string _id;
        [JsonIgnore]
        public string Id
        {
            get
            {
                if (String.IsNullOrEmpty(_id))
                {
                    _id = Guid.NewGuid().ToString();
                }
                return _id;
            }
        }
        public Vector2 MaxVelocity
        {
            get { return _maxVelocity; }
            set { _maxVelocity = value; }
        }
       public  float  MaxSpeed
        {
            get { return _maxspeed + MaxSpeedModifier; }
            set { _maxspeed = value; }
        }

        public float MaximumSlopeAngle
        {
            get { return _maximumSlopeAngle; }
            set { _maximumSlopeAngle = value; }
        }

       public  float Gravity
        {
            get
            {
                return _gravity;
            }
            set { _gravity = value; }
        }

       public float GroundAcceleration
        {
            get
            {
                return _groundAcceleration;
            }
            set { _groundAcceleration = value; }
        }

       public float AirAcceleration
        {
            get { return _airAcceleration; }
            set { _airAcceleration = value; }
        }


       public float MaxSpeedModifier
       {
           get
           {
               return _maxSpeedModifier;
               
           }
           set { _maxSpeedModifier = value; }
       }

       public int ExtraJumps
       {
           get { return _extraJumps + ExtraJumpModifier; }
           set { _extraJumps = value; }
       }

       public int ExtraJumpModifier
       {
           get
           {
               return _extraJumpModifier;
           }
           set { _extraJumpModifier = value; }
       }

        public string ViewDebugText()
        {

            string output = "Player controller parameters";
            output += "\n";

            const string format = ExitTheVoidStringFormatting.DebugFormatTwoParams;
            const string floatformat = ExitTheVoidStringFormatting.DebugFloatFormatTwoParams;

            output += "\n" + String.Format(format, "MaxVelocity?:", ExitTheVoidStringFormatting.Vector2ToString(MaxVelocity));
            output += "\n" + String.Format(floatformat, "MaxSpeed?: ", MaxSpeed);
            output += "\n" + String.Format(floatformat, "MaximumSlopeAngle?: ", MaximumSlopeAngle);
            output += "\n" + String.Format(floatformat, "Gravity?: ", Gravity);
            output += "\n" + String.Format(floatformat, "GroundAcceleration?: ", GroundAcceleration);
            output += "\n" + String.Format(floatformat, "AirAcceleration?: ", AirAcceleration);
            output += "\n" + String.Format(floatformat, "MaxSpeedModifier?: ", MaxSpeedModifier);
            output += "\n" + String.Format(format, "ExtraJumps?:", ExtraJumps);
            output += "\n" + String.Format(format, "ExtraJumpModifier?:", ExtraJumpModifier); 
            return output;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PlayerControllerParameters) obj);
        }
        protected bool Equals(PlayerControllerParameters other)
        {
            return string.Equals(Id, other.Id);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode() : 0);
        }

        public static bool operator ==(PlayerControllerParameters left, PlayerControllerParameters right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PlayerControllerParameters left, PlayerControllerParameters right)
        {
            return !Equals(left, right);
        }

    }
}
