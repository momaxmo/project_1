﻿
using System;
using Assets.Scripts.Debugging;
using Assets.Scripts.tools;
using LitJson;

namespace Assets.Scripts.game.player.model
{
    public class PlayerControllerState : IPlayerControllerState, IDebuggable
    {


        public bool IsCollidingLeft { get; set; }
        public bool IsCollidingRight { get; set; }
        public bool IsCollidingAbove { get; set; }
        public bool IsCollidingBelow { get; set; }

        public bool IsMovingDownSlope { get; set; }
        public bool IsMovingUpSlope { get; set; }
        public bool SlopeAngleOK { get; set; }
        public float SlopeAngle { get; set; }
        [JsonIgnore]
        private string _id;
        [JsonIgnore]
        public string Id
        {
            get
            {
                if (String.IsNullOrEmpty(_id))
                {
                    _id = Guid.NewGuid().ToString();
                }
                return _id;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PlayerControllerState)obj);
        }

        /// <summary>
        /// is the character grounded?
        /// </summary>
        public bool IsGrounded { get { return IsCollidingBelow; } }

        public bool WasGroundedLastFrame { get; set; }
        public bool IsFalling { get; set; }

        public void Reset()
        {
            IsMovingUpSlope = IsMovingDownSlope = IsCollidingLeft = IsCollidingRight = IsCollidingAbove = SlopeAngleOK = JustGotGrounded =
               false;
            IsFalling = true;
            SlopeAngle = 0;

        }


        public bool JustGotGrounded { get; set; }
        public string ViewDebugText()
        {
            string output = "Player controller state";
            output += "\n";

            const string format = ExitTheVoidStringFormatting.DebugFormatTwoParams;
            const string floatformat = ExitTheVoidStringFormatting.DebugFloatFormatTwoParams;
            output += "\n" + String.Format(format, "Is falling? ?:", ExitTheVoidStringFormatting.BoolToYesNo(IsFalling));
            output += "\n" +
                      String.Format(format, "Was grounded last frame? ",
                          ExitTheVoidStringFormatting.BoolToYesNo(WasGroundedLastFrame));
            output += "\n" + String.Format(format, " Is grounded? ",
                          ExitTheVoidStringFormatting.BoolToYesNo(IsGrounded));
            output += "\n" + String.Format(format, "Is Colliding Left?:", ExitTheVoidStringFormatting.BoolToYesNo(IsCollidingLeft));
            output += "\n" + String.Format(format, "Is Colliding Right?: ", ExitTheVoidStringFormatting.BoolToYesNo(IsCollidingRight));
            output += "\n" + String.Format(format, "Is Colliding Above?:", ExitTheVoidStringFormatting.BoolToYesNo(IsCollidingAbove));
            output += "\n" + String.Format(format, "Is Colliding Below?:", ExitTheVoidStringFormatting.BoolToYesNo(IsCollidingBelow));
            output += "\n" + String.Format(format, "Is Moving Down Slope?:", ExitTheVoidStringFormatting.BoolToYesNo(IsMovingDownSlope));
            output += "\n" + String.Format(format, "Is Moving Up Slope?:", ExitTheVoidStringFormatting.BoolToYesNo(IsMovingUpSlope));
            output += "\n" + String.Format(format, "Slope Angle OK?:", ExitTheVoidStringFormatting.BoolToYesNo(SlopeAngleOK));
            output += "\n" + String.Format(floatformat, "Slope Angle ?:", SlopeAngle);

            return output;
        }
        protected bool Equals(PlayerControllerState other)
        {
            return string.Equals(Id, other.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public static bool operator ==(PlayerControllerState left, PlayerControllerState right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PlayerControllerState left, PlayerControllerState right)
        {
            return !Equals(left, right);
        }
    }
}
