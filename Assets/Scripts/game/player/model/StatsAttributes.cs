﻿ 

using System;

namespace Assets.Scripts.game.player.model 
{
    /// <summary>
    /// Simple class that holds data of a character's stats
    /// </summary>
    [Serializable]
   public  class StatsAttributes
   {
       #region offensive Attributes

        public int Level;
        public int AttackPower;
       //keen eye in game
        public float Accuracy;

        public float CritChance;
        public float ChanceToStun;//on stun , flash of light

        #endregion

        #region Defensive attributes

        public int HealthPool;
        public int Agility;
        public float ShadowStepChance;//shadow step into the darkness for the enemies to miss you completely
        public float Defence;

        #endregion

        public float Luck;

   }
}
