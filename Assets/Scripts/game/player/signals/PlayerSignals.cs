﻿
using strange.extensions.signal.impl;
using UnityEngine;

namespace Assets.Scripts.game.player.signals
{
    class PlayerSignals
    {
    }
    /// <summary>
    /// sends a signal to spawn a PlayerModel at the targetted position
    /// </summary>
    public class SpawnPlayerSignal : Signal<Vector2>
    {
    }

    public class LoadPlayerOnStartSignal : Signal
    {
        
    }
}
