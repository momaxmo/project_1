﻿ 

using Assets.Scripts.game.player.model;
using Assets.Scripts.game.view.environment;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.game.player
{
    public class PlayerBounds : View
    {
        public enum BoundsType
        {
            Constrain,
            Kill,
            Nothing

        }

        public BoundsType Above;
        public BoundsType Below;
        public BoundsType Left;
        public BoundsType Right;

        public BoxCollider2D _levelBounds;
        private IPlayerModel _mainPlayer;
        private BoxCollider2D _boxCollider;


        public void Init(IPlayerModel playerModel)
        {
            _mainPlayer = playerModel;
            _boxCollider = GetComponent<BoxCollider2D>();
            _levelBounds = GameObject.FindGameObjectWithTag("LevelBounds").GetComponent<BoxCollider2D>();

        }

        void Update()
        {

            if (!_mainPlayer.PlayerBehaviourState.IsDead)
            {
                var colliderSize = Vector2.zero;
                colliderSize.x = _boxCollider.size.x*Mathf.Abs(transform.localScale.x);
                colliderSize.y = _boxCollider.size.y*Mathf.Abs(transform.localScale.y) /2;
                // when the player reaches a bound, we apply the specified bound behavior
                if (Above != BoundsType.Nothing && transform.position.y + colliderSize.y > _levelBounds.bounds.max.y)
                    ApplyConstrainedBehaviour(Above, new Vector2(transform.position.x, _levelBounds.bounds.max.y - colliderSize.y));

                if (Below != BoundsType.Nothing && transform.position.y - colliderSize.y < _levelBounds.bounds.min.y)
                    ApplyConstrainedBehaviour(Below, new Vector2(transform.position.x, _levelBounds.bounds.min.y + colliderSize.y));

                if (Right != BoundsType.Nothing && transform.position.x + colliderSize.x > _levelBounds.bounds.max.x)
                    ApplyConstrainedBehaviour(Right, new Vector2(_levelBounds.bounds.max.x - colliderSize.x, transform.position.y));

                if (Left != BoundsType.Nothing && transform.position.x - colliderSize.x < _levelBounds.bounds.min.x)
                    ApplyConstrainedBehaviour(Left, new Vector2(_levelBounds.bounds.min.x + colliderSize.x, transform.position.y));

            }
        }

        private void ApplyConstrainedBehaviour(BoundsType type, Vector2 constrainedPosition)
        {
            if (type == BoundsType.Kill)
            {
                Debug.Log("Kill player");
            }
            transform.position = constrainedPosition;
        }
    }
}
