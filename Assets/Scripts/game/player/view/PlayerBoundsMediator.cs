﻿

using Assets.Scripts.game.player.model;
using strange.extensions.mediation.impl;

namespace Assets.Scripts.game.player.view
{
    public class PlayerBoundsMediator: Mediator
    {
        [Inject]
        public PlayerBounds PlayerBounds { get; set; }
        [Inject]
        public IPlayerModel PlayerModel { get; set; }

        public override void OnRegister()
        {
            PlayerBounds.Init(PlayerModel);
        }
    }
}
