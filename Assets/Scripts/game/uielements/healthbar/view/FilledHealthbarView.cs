﻿

using System;
using System.Collections;
using Assets.Scripts.game.player.model;
using Assets.Scripts.tools;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.game.uielements.healthbar.view
{
    /// <summary>
    /// View for the character's health with an image that is filled as a percentage
    /// </summary>
    public class FilledHealthbarView : View, IHealthbarView
    {
        private Image _healthBarForegroundImage;
        private IPlayerModel _mainPlayerModel;
        [SerializeField]
        private float _updateHealthSpeed = 1f;
        private IEnumerator _previousHealthUpdateRoutine;


        private bool _isAnimating;
        public float UpdateHealthViewSpeed
        {
            get { return _updateHealthSpeed; }
        }

        public void Init(IPlayerModel playerModel)
        {
            Component potentialHealthbarImage = ExitTheVoidTools.FindObjectByName(transform, "HealthBarForeground");
            if (potentialHealthbarImage != null)
            {
                _healthBarForegroundImage = potentialHealthbarImage.GetComponent<Image>();

            }
            _mainPlayerModel = playerModel;
        }


        /// <summary>
        /// Animates the health of the health bar to a new fill amount
        /// </summary>
        public void UpdateHealth()
        {
            float newFillAmount = Mathf.Clamp01((float)_mainPlayerModel.PlayerStats.CurrentHealth / (float)_mainPlayerModel.PlayerStats.BaseHealthPool);
            if (_previousHealthUpdateRoutine != null)
            {
                StopCoroutine(_previousHealthUpdateRoutine);
            }
            _previousHealthUpdateRoutine = UpdateFillAmount(newFillAmount);
            StartCoroutine(_previousHealthUpdateRoutine);
        }

        private IEnumerator UpdateFillAmount(float targetFill)
        {

            float timeStartLerp = Time.time;
            float startPos = _healthBarForegroundImage.fillAmount;
            float distance = Mathf.Abs(targetFill - startPos);
            float timeToTakeDuringLerp = distance / UpdateHealthViewSpeed;

            while (true)
            {

                float timeSinceStartdLerping = Time.time - timeStartLerp;
                float percentComplete = timeSinceStartdLerping / timeToTakeDuringLerp;
           
                // percentComplete = percentComplete*percentComplete;
                float newFillAmount = Mathf.Lerp(startPos, targetFill, percentComplete);
                if (float.IsNaN(newFillAmount))
                {
                    Debug.Log("nan");
                    _healthBarForegroundImage.fillAmount = targetFill;
                    yield break;
                }
                _healthBarForegroundImage.fillAmount = newFillAmount;
                if (percentComplete >= 1.0f)
                {
                    yield break;

                }
                yield return null;
            }
        }
    }
}
