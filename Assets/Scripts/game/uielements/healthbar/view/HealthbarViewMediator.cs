﻿ 

using Assets.Scripts.game.player.model;
using Assets.Scripts.game.uielements.healthbar.signal;
using strange.extensions.mediation.impl;

namespace Assets.Scripts.game.uielements.healthbar.view
{
    public class HealthbarViewMediator : Mediator
    {
        [Inject]
        public FilledHealthbarView HealthBar { get; set; }

        [Inject]
        public IPlayerModel Player { get; set; }

        [Inject]
        public UpdateHealthBarSignal UpdateHealthBarSignal { get; set; }

        public override void OnRegister()
        {
            HealthBar.Init(Player);
            UpdateHealthBarSignal.AddListener(UpdateHealth);
        }

        public override void OnRemove()
        {
            UpdateHealthBarSignal.RemoveListener(UpdateHealth);
        }

        private void UpdateHealth()
        {
            HealthBar.UpdateHealth();
        }
    }
}
