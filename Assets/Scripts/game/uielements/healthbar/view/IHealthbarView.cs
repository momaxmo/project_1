﻿ 

using Assets.Scripts.game.player.model;

namespace Assets.Scripts.game.uielements.healthbar.view
{
    public interface IHealthbarView
    {
        void Init(IPlayerModel playermodel);
        float UpdateHealthViewSpeed { get; }

        void UpdateHealth();
    }
}
