﻿ 

using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.game.view.environment
{
    public class LevelBounds: View
    {
        public float LeftLimit;
        public float RightLimit;
        public float BottomLimit;
        public float UpperLimit;

        private BoxCollider2D _collider2D;

        public void Init()
        {
            _collider2D = GetComponent<BoxCollider2D>();
            LeftLimit = _collider2D.bounds.min.x;
            RightLimit = _collider2D.bounds.max.x;
            BottomLimit = _collider2D.bounds.min.y;
            UpperLimit = _collider2D.bounds.max.y;

             
        }
    }
}
