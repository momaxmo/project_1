﻿ 
using Assets.Scripts.common.config;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.game.view.environment
{
    public class LevelBoundsMediator: Mediator
    {
        [Inject]
        public LevelBounds LBounds { get; set; }

        [Inject]
        public UpdateLevelBoundsSignal UpdateLevelBoundsSignal { get; set; }
        public override void OnRegister()
        {
            LBounds.Init();
            UpdateLevelBoundsSignal.Dispatch(LBounds); 
        }
        public override void OnRemove()
        {

        }

     

    }
}
