﻿ 

namespace Assets.Scripts.game.view.player
{
    public interface IPlayerBehaviourView
    {
        void UpdateHorizontalAndVerticalAxisComponents(float h, float v);
        void UpdateJumpInput(bool buttonPress , bool buttonReleased);
        //void Init();
    }
}
