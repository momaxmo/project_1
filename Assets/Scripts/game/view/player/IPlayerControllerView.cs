﻿using System.Collections; 
using strange.extensions.signal.impl;
using UnityEngine;

namespace Assets.Scripts.game.view.player
{
    public interface IPlayerControllerView
    {
/*        IPlayerControllerState ControllerState { get; set; }
        IPlayerControllerParameters ControllerParameters { get; set; }*/
        IEnumerator DisableCollisions(float time);
    //    Signal<Vector2> UpdateStatsSignal { get; set; }
        /// <summary>
        /// The layermask of the platforms in this level
        /// </summary>
        LayerMask PlatformMask { get; set; }

        LayerMask MovingPlatformMask { get; set; }
        LayerMask OneWayPlatformMask { get; set; }
        GameObject StandingOnGameObject { get; set; }
        bool IsPaused { get; set; }
        Vector2 Velocity { get;   }

        #region Raycasting params

        int NumberOfHorizontalRays { get; set; }
        int NumberOfVerticalRays { get; set; }
        float RayOffset { get; set; }

        #endregion

        Vector2 ColliderCenter { get;  } 
        Vector2 ColliderSize { get;  } 
        Vector2 ColliderPositon { get;  } 
        Vector2  BottomPosition { get;  }

        Transform PlayerTranform { get; }

        //void Init();
        void AddForce(Vector2 v);
        void SetHorizontalForce(float p);
        void SetVerticalForce(float p);

        void SetRayParams();

        bool Enabled { get; set; }
    }
}

