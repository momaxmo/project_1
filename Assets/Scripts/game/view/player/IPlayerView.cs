﻿ 
namespace Assets.Scripts.game.view
{
    public interface IPlayerView: IPausable

    {
        string Name { get; set; }
        bool JumpButtonIsPressed { get; set; }
        bool IsGrounded { get; }
        float MoveSpeed { get; set; }


       // PlayerBehaviourState CharacterState { get; }
        bool IsLookingRight { get; set; }
        void SetAction(float h, float v, int evt);

        void SetJumpAction(bool jumpPress, bool jumpHeld,bool jumpRelease);
    }
}
