﻿ 
using System.Collections; 
using Assets.Scripts.Debugging;
using Assets.Scripts.exceptions;
using Assets.Scripts.game.player.model;
using Assets.Scripts.game.sfx;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine; 

namespace Assets.Scripts.game.view.player
{
    public class PlayerBehaviourView : View, IPlayerBehaviourView, IDebuggable
    {
        #region fields
        [SerializeField]
        private BoxCollider2D _headCollider;
        [SerializeField]
        private ParticleSystem _touchTheGroundEffect;
        [SerializeField]
        private ParticleSystem _hurteffectParticleSystem;

        [SerializeField]
        private AudioClip _playerJumpSfx;
        [SerializeField]
        private AudioClip _playerHitSfx;

        private bool _jumpAuthorized;
        private bool _jumpButtonIsPressed;
        private bool _jumpButtonReleased;

        private bool _isFacingRight; //true if facing right

        private float _horizontalAxisInput;
        private float _verticalAxisInput;
        private float _normalizedHorizontalSpeed;
        private float _originalGravity;
        private float _totalJumpPressTime;
        private GhostEffect _ghostEffect;


        public Color Lightcolor;
        public Color ShadowyColor;
        private Animator _animationController;
        #endregion
        #region properties
        public int Health { get; set; }
        public ParticleSystem TouchTheGroundEffect
        {
            get { return _touchTheGroundEffect; }
            set { _touchTheGroundEffect = value; }
        }

        public ParticleSystem HurtEffect
        {
            get { return _hurteffectParticleSystem; }
            set { _hurteffectParticleSystem = value; }
        }

        public BoxCollider2D HeadCollider
        {
            get
            {
                if (_headCollider == null)
                {
                    _headCollider = transform.Find("Head").GetComponent<BoxCollider2D>();
                    if (!_headCollider)
                    {
                        throw new NoObjectAttachedException("PlayerModel head collider has not been assigned");
                    }
                }
                return _headCollider;
            }
            set { _headCollider = value; }
        }
        #endregion

        #region injections


       [Inject]
       public IPlayerModel MainPlayer { get; set; }
        //private MainPlayerModel MainPlayer;
        //public Signal<IPlayerBehaviourPermissions> PlayerBehaviourUpdateSignal = new Signal<IPlayerBehaviourPermissions>();


        public AudioClip PlayerJumpSfx
        {
            get { return _playerJumpSfx; }
            set { _playerJumpSfx = value; }
        }

        public AudioClip PlayerHitSfx
        {
            get { return _playerHitSfx; }
            set { _playerHitSfx = value; }
        }

        public bool JumpAuthorized
        {
            get
            {
                if (MainPlayer.PlayerBehaviourParameters.JumpRules == JumpingRules.CanJumpAnyWhere)
                {
                    _jumpAuthorized = true;
                }
                else if (MainPlayer.PlayerBehaviourParameters.JumpRules == JumpingRules.CanJumpOnGround)
                {
                    _jumpAuthorized = MainPlayer.PlayerControllerState.IsGrounded;
                }
                else
                {
                    _jumpAuthorized = false;
                }
                return _jumpAuthorized;
            }

        }

        #endregion


        public void Init(IPlayerModel playerModel)
        {
            _isFacingRight = transform.localScale.x > 0;
            _animationController = GetComponent<Animator>();
            RegisterPlayer(playerModel);
        }

        public void RegisterPlayer(IPlayerModel playerModel)
        {
           // MainPlayer = new MainPlayerModel(playerModel as MainPlayerModel);
            _originalGravity = MainPlayer.PlayerControllerParameters.Gravity;
            MainPlayer.PlayerBehaviourState.NumberOfJumpsLeft = MainPlayer.PlayerBehaviourParameters.ExtraJumps;
            MainPlayer.PlayerBehaviourState.CanJump = true;
            Vector2 newMaxVelocity = MainPlayer.PlayerControllerParameters.MaxVelocity;
            newMaxVelocity.y = 50f;
            MainPlayer.PlayerControllerParameters.MaxVelocity = newMaxVelocity;
        }

        // ReSharper disable once UnusedMember.Local
        private void Update()
        {
            UpdateAnimation();
            if (!MainPlayer.PlayerBehaviourState.IsDead)
            {
                ActivateGravity(true);
                HorizontalMovement();
                VerticalMovement();
                JumpRoutine(); 
            }
            else
            {
                MainPlayer.PlayerControllerView.SetHorizontalForce(0);
            }
            
        }

        // ReSharper disable once UnusedMember.Local
        void LateUpdate()
        {
            if (MainPlayer.PlayerControllerState.JustGotGrounded)
            {
                MainPlayer.PlayerBehaviourState.NumberOfJumpsLeft = MainPlayer.PlayerBehaviourParameters.ExtraJumps;
            }
#if UNITY_EDITOR

            //PlayerBehaviourUpdateSignal.Dispatch(MainPlayer.PlayerBehaviourPermissions);
#endif
        }

        public string ouptu;
        void JumpRoutine()
        {


            if (JumpAuthorized)
            {
                ouptu = "";
                //apply a down force on the character once he stopped pressing the jump button
                bool jumpHasBeenInitiated = _totalJumpPressTime != 0;//
                bool airHangingTooLong = Time.time - _totalJumpPressTime >= MainPlayer.PlayerBehaviourParameters.MinimumJumpAirTime;
                bool velocityFasterThanGravity = MainPlayer.PlayerControllerView.Velocity.y >
                         Mathf.Sqrt(Mathf.Abs(MainPlayer.PlayerControllerParameters.Gravity));
                ouptu += "airhanging too long " + airHangingTooLong;
                bool tally = jumpHasBeenInitiated && airHangingTooLong && velocityFasterThanGravity &&
                             !_jumpButtonIsPressed && _jumpButtonReleased;
                if (tally)
                {
                    _jumpButtonIsPressed = false;
                    if (MainPlayer.PlayerBehaviourParameters.JumpProportionalToPressTime)
                    {
                        MainPlayer.PlayerControllerView.AddForce(new Vector2(0,
                            12 * -Mathf.Abs(MainPlayer.PlayerControllerParameters.Gravity) * Time.deltaTime));

                    }
                }
            }
        }
        /// <summary>
        /// Called during update, handlies the vertical movement
        /// </summary>
        private void VerticalMovement()
        {
            if ((_verticalAxisInput > 0) && (MainPlayer.PlayerControllerState.IsGrounded))
            {
                MainPlayer.PlayerBehaviourState.LookingUp = true;
              //  todo Debug.Log("Change camera view to look up with character");

            }
            else
            {
                MainPlayer.PlayerBehaviourState.LookingUp = false;
//               todo Debug.Log("Reset camera view to look back to character level");
            }
            if (MainPlayer.PlayerControllerState.JustGotGrounded)
            {
             //  todo Debug.Log("Instantiate ground particle effects");
            }
           
            if (!MainPlayer.PlayerBehaviourState.CanMoveFreely)
            {
                return;
            }
            //Lets check if the character is grounded and the verticalaxis input is <-0.1
            if ((_verticalAxisInput < -0.1f) && (MainPlayer.PlayerControllerState.IsGrounded) &&
                (MainPlayer.PlayerBehaviourPermissions.CanCrouch))
            {
                MainPlayer.PlayerBehaviourState.IsCrouching = true;
             //   MainPlayer.PlayerBehaviourParameters.MovementSpeed = MainPlayer.PlayerBehaviourParameters.CrouchMovementSpeed;
                MainPlayer.PlayerBehaviourState.IsRunning = false;
                // todo Debug.Log("tell camera to look down");

            }
            else
            {
                if (MainPlayer.PlayerBehaviourState.IsCrouching)
                {
                    if (HeadCollider == null)
                    {
                        MainPlayer.PlayerBehaviourState.IsCrouching = false;
                        return;
                    }
                    bool headChecker = Physics2D.OverlapCircle(HeadCollider.transform.position, HeadCollider.size.x / 2,
                        MainPlayer.PlayerControllerView.PlatformMask);
                    if (!headChecker)
                    {
                        if (!MainPlayer.PlayerBehaviourState.IsRunning)
                        {
                            /*MainPlayer.PlayerBehaviourParameters.MovementSpeed = MainPlayer.PlayerBehaviourParameters.WalkSpeed;*/
                            MainPlayer.PlayerBehaviourParameters.MovementSpeed = MainPlayer.PlayerBehaviourParameters.RunSpeed;
                            MainPlayer.PlayerBehaviourState.IsCrouching = false;
                            MainPlayer.PlayerBehaviourState.CanJump = true;
                        }

                    }
                    else
                    {
                        MainPlayer.PlayerBehaviourState.CanJump = true;
                    }
                }
            }

            if (MainPlayer.PlayerBehaviourState.PreviouslyCrouching != MainPlayer.PlayerBehaviourState.IsCrouching)
            {
                Invoke("RecalculateRays", Time.deltaTime * 10);
            }
            MainPlayer.PlayerBehaviourState.PreviouslyCrouching = MainPlayer.PlayerBehaviourState.IsCrouching;
        }

        public void RecalculateRays()
        {
            MainPlayer.PlayerControllerView.SetRayParams();
        }

        /// <summary>
        /// Called during update, updates the horizontal movements according to the current state of the PlayerModel
        /// </summary>
        private void HorizontalMovement()
        {

            if (!MainPlayer.PlayerBehaviourState.CanMoveFreely)
            {
                return;
            }
            if (_horizontalAxisInput > 0.1) //if the input is greater than zero, then the PlayerModel is facing right
            {
                if (!_isFacingRight)
                    Flip();
                _normalizedHorizontalSpeed = _horizontalAxisInput;
            }
            else if (_horizontalAxisInput < -0.1f)
            {
                if (_isFacingRight)
                    Flip();
                _normalizedHorizontalSpeed = _horizontalAxisInput;
            }
            else
            {
                _normalizedHorizontalSpeed = 0;
            }

            //pass it to the controller. If he's on the ground then he must accelerate according to the ground acceleration
            //else must be in the air acceleration
            float movementFactor = MainPlayer.PlayerControllerState.IsGrounded
                ? MainPlayer.PlayerControllerParameters.GroundAcceleration
                : MainPlayer.PlayerControllerParameters.AirAcceleration;
         
            if (MainPlayer.PlayerBehaviourParameters.SmoothMovement)
            { 
                 MainPlayer.PlayerControllerView.SetHorizontalForce(Mathf.Lerp(MainPlayer.PlayerControllerView.Velocity.x,
                    _normalizedHorizontalSpeed * MainPlayer.PlayerBehaviourParameters.MovementSpeed, Time.deltaTime * movementFactor));  
                MainPlayer.PlayerControllerView.SetHorizontalForce(MainPlayer.PlayerBehaviourParameters.MovementSpeed * _normalizedHorizontalSpeed);
            }
            else
            {
                MainPlayer.PlayerControllerView.SetHorizontalForce(MainPlayer.PlayerBehaviourParameters.MovementSpeed * _normalizedHorizontalSpeed);
            }

        }
        /// <summary>
        /// Causes the character to start running 
        /// </summary>
        public void BeginRunning()
        {
            if (!MainPlayer.PlayerBehaviourPermissions.CanRun || !MainPlayer.PlayerBehaviourState.CanMoveFreely)
            {
                return;
            }
            if (MainPlayer.PlayerControllerState.IsGrounded && !MainPlayer.PlayerBehaviourState.IsRunning)
            {
                MainPlayer.PlayerBehaviourParameters.MovementSpeed = MainPlayer.PlayerBehaviourParameters.RunSpeed;
                MainPlayer.PlayerBehaviourState.IsRunning = true;
            }

        }

        /// <summary>
        /// causes the character to stop running
        /// </summary>
        public void EndRun()
        {

        }
        /// <summary>
        /// causes the character to start jumping
        /// </summary>
        public void JumpStart()
        {

            if (!MainPlayer.PlayerBehaviourPermissions.CanJump || !JumpAuthorized || MainPlayer.PlayerBehaviourState.IsDead)
            {
                return;
            }
            if (MainPlayer.PlayerControllerState.IsGrounded || MainPlayer.PlayerBehaviourState.IsClimbingLadder
                || MainPlayer.PlayerBehaviourState.NumberOfJumpsLeft > 0)
            {
                MainPlayer.PlayerBehaviourState.CanJump = true;
            }
            else
            {
                MainPlayer.PlayerBehaviourState.CanJump = false;
            }
            if (!MainPlayer.PlayerBehaviourState.CanJump &&
                (MainPlayer.PlayerBehaviourParameters.JumpRules != JumpingRules.CanJumpAnyWhereAnynumberOfTimes))
            {
                return;

            }
            if (_verticalAxisInput < 0 && MainPlayer.PlayerControllerState.IsGrounded)
            {
                int movingPlatformlayer = LayerMask.NameToLayer("MovingPlatform");
                if (MainPlayer.PlayerControllerView.StandingOnGameObject.layer == movingPlatformlayer)
                {
                   // Debug.Log("on moving platform");
                    //if the character is standing ontop of a moving platform, we'll move the PlayerModel slightly down and disable the colliders for tiny bit
                    MainPlayer.PlayerControllerView.PlayerTranform.position = new Vector2(transform.position.x, transform.position.y - (0.1f * transform.localScale.y));
                    StartCoroutine(MainPlayer.PlayerControllerView.DisableCollisions(0.3f));
                    return;
                }
            }
            //reduce the number of jumps left
            MainPlayer.PlayerBehaviourState.NumberOfJumpsLeft--;
            MainPlayer.PlayerBehaviourState.IsClimbingLadder = false;
            MainPlayer.PlayerBehaviourState.CanMoveFreely = true;
            ActivateGravity(true);

            _totalJumpPressTime = Time.time;
            _jumpButtonIsPressed = true;
            _jumpButtonReleased = false;
            float verticalForce =
                Mathf.Sqrt(2f*MainPlayer.PlayerBehaviourParameters.JumpHeight*
                           Mathf.Abs(MainPlayer.PlayerControllerParameters.Gravity)); 
            MainPlayer.PlayerControllerView.SetVerticalForce(verticalForce);

           
            if (PlayerJumpSfx != null)
            {
                //Debug.Log("play jump sound");
            }

        }


        /// <summary>
        /// causes the character to stop jumping
        /// </summary>
        public void JumpStop()
        {

            _jumpButtonIsPressed = false;
            _jumpButtonReleased = true;
        }

        public void ClimbLadders()
        {
        }

        public IEnumerator Flicker(Color initialColor, Color flickerColor, float flickerSpeed)
        {
            Renderer characterRenderer = GetComponent<Renderer>();
            if (characterRenderer)
            {
                for (int i = 0; i < 10; i++)
                {
                    characterRenderer.material.color = initialColor;
                    yield return new WaitForSeconds(flickerSpeed);
                    characterRenderer.material.color = flickerColor;
                    yield return new WaitForSeconds(flickerSpeed);
                }
                characterRenderer.material.color = initialColor;
            }
        }
        /// <summary>
        /// Kills the character
        /// </summary>
        public void Kill()
        {

        }

        /// <summary>
        /// Disables the PlayerModel(at the end, or on pause for example)
        /// it won't move and respond to inputs
        /// </summary>
        public void Disable()
        {
            enabled = false;
            MainPlayer.PlayerControllerView.Enabled = false;
            GetComponent<Collider2D>().enabled = false;
        }

        /// <summary>
        /// Respawns at a target location passed in parameters
        /// </summary>
        /// <param name="spawnPoint"></param>
        public void RespawnAtLocation(Transform spawnPoint)
        {

        }
        /// <summary>
        /// Called when the PlayerModel takes damage
        /// </summary>
        /// <param name="damage">Damage applied value</param>
        /// <param name="instigator">who caused the damage?</param>
        public void TakeDamage(int damage, GameObject instigator)
        {

        }
        /// <summary>
        /// Called when the PlayerModel is healed
        /// </summary>
        /// <param name="health">how much health is given</param>
        /// <param name="insitigator">who or what gave the PlayerModel health?</param>
        public void GetHealth(int health, GameObject insitigator)
        {

        }
        /// <summary>
        /// flips the character and its dependencies horizontally
        /// </summary>
        private void Flip()
        {
            Vector2 newScale = transform.localScale;
            newScale.x = -newScale.x;
            transform.localScale = newScale;
            _isFacingRight = transform.localScale.x > 0;
        }

        // ReSharper disable once UnusedMember.Local
        private void OnTriggerEnter2D(Collider2D col)
        {
           /* if (col.gameObject != gameObject)
                Debug.Log(col.name + " has entered collision with " + gameObject.name);*/
        }

        // ReSharper disable once UnusedMember.Local
        private void OnTriggerStay2D(Collider2D col)
        {
          /*  if (col.gameObject != gameObject)
                Debug.Log(col.name + " is staying collided with  " + gameObject.name);*/
        }

        // ReSharper disable once UnusedMember.Local
        private void OnTriggerExit2D(Collider2D col)
        {
           /* if (col.gameObject != gameObject)
                Debug.Log(col.name + " is staying collided with  " + gameObject.name);*/
        }
        private void ActivateGravity(bool status)
        {
            if (status)
            {

                //if (MathTools.InclusiveRange(-0.0001f, 0.0001f, MainPlayer.PlayerControllerParameters.Gravity))
                //{
                MainPlayer.PlayerControllerParameters.Gravity = _originalGravity;
                // }
            }
            else
            {
                /*if (!MathTools.InclusiveRange(-0.0001f, 0.0001f, MainPlayer.PlayerControllerParameters.Gravity))
                {*/
                _originalGravity = MainPlayer.PlayerControllerParameters.Gravity;
                //}
                MainPlayer.PlayerControllerParameters.Gravity = 0;
            }

        }
        /// <summary>
        /// updates the horizontal and vertical axis components of this class
        /// </summary>
        /// <param name="h">horizontal axis</param>
        /// <param name="v">vertical axis</param>
        public void UpdateHorizontalAndVerticalAxisComponents(float h, float v)
        {

            _horizontalAxisInput = h;
            _verticalAxisInput = v;
        }




        public void UpdateJumpInput(bool buttonPress , bool buttonReleased)
        {
            if (buttonPress)
            {
                JumpStart();
            }
            if (buttonReleased)
            {
                JumpStop();
            }
            /*_jumpButtonIsPressed =buttonPress;
            _jumpButtonReleased = buttonReleased;*/
        }

        public string ViewDebugText()
        {
            string output = "PlayerBehaviour View";
            output += "\n" + "normalized input" + _normalizedHorizontalSpeed;
            output += "\n " + "JumpAuthorized? " + JumpAuthorized;  
            output += "\n" + "  hor Axis " + _horizontalAxisInput;
            
            return output;
        }

        public string Id
        {
            get { return gameObject.GetInstanceID().ToString(); }
        }

        public GhostEffect GhostEffect
        {
            get
            {
                if (_ghostEffect == null)
                {
                    _ghostEffect = GetComponent<GhostEffect>();
                    if (_ghostEffect == null)
                    {
                        gameObject.AddComponent<GhostEffect>();
                    }
                }
                return _ghostEffect;
            } 
        }

        /// <summary>
        /// Dash the player forward on input received
        /// </summary>
        public void StartDashing()
        {

            //is the player allow to dash?
            bool canDash = MainPlayer.PlayerBehaviourPermissions.CanDash &&
                           MainPlayer.PlayerBehaviourParameters.DashingRules != DashingRules.CantDash &&
                           MainPlayer.PlayerBehaviourState.CanMoveFreely &&
                           !MainPlayer.PlayerBehaviourState.IsDead;
            bool diveInstead = _verticalAxisInput < -0.8f;
            //check what are the rules for dashing   
            if (canDash && !diveInstead)
            {
                MainPlayer.PlayerBehaviourState.IsDashing = true;
                float playerforward = _isFacingRight ? 1f : -1f;
                float horizontalDashForce = playerforward * MainPlayer.PlayerBehaviourParameters.DashForce;
                // float verticalDashForce = _verticalAxisInput * MainPlayer.PlayerBehaviourParameters.DashForce;
                Vector2 dashDirection = new Vector2(horizontalDashForce, 0);
                //if ( !diveInstead)
                //    {
              
                dashDirection.y = 0;
                StartCoroutine(Dash(MainPlayer.PlayerBehaviourParameters.DashingDuration, MainPlayer.PlayerBehaviourParameters.DashCooldown, dashDirection));
                //  }
                /*else if (!diveInstead) //if he's not grounded, it must be the case that he is in the air
                {
                    dashDirection.x = _horizontalAxisInput;
                    dashDirection = dashDirection.normalized * MainPlayer.PlayerBehaviourParameters.DashForce;
                    MainPlayer.PlayerBehaviourState.CanDash = false;

                    StartCoroutine(Dash(MainPlayer.PlayerBehaviourParameters.DashingDuration, MainPlayer.PlayerBehaviourParameters.DashCooldown, dashDirection));
                    // ReSharper disable once CompareOfFloatsByEqualityOperator
                    //bool isAiming = _horizontalAxisInput * _verticalAxisInput != 0;
                    /*if (!isAiming)
                    {
                        Debug.Log("is aiming");
                        MainPlayer.PlayerBehaviourState.CanDash = false;
                        dashDirection.y = 0;
                        StartCoroutine(Dash(MainPlayer.PlayerBehaviourParameters.DashingDuration, MainPlayer.PlayerBehaviourParameters.DashCooldown, dashDirection));
                    }
                    else
                    {
                        Debug.Log("isnt aiming");

                        dashDirection.x = _horizontalAxisInput;
                        dashDirection = dashDirection.normalized*MainPlayer.PlayerBehaviourParameters.DashForce;
                        MainPlayer.PlayerBehaviourState.CanDash = false;
                        StartCoroutine(Dash(MainPlayer.PlayerBehaviourParameters.DashingDuration, MainPlayer.PlayerBehaviourParameters.DashCooldown, dashDirection));
                    }
                     #1#
                    //if he is in the air, check which direction the player is aiming
                    //no direction? no problem, just get the current forward and dash in that direction
                }*/
            }
            else if (diveInstead)
            {
               // StartCoroutine(Dive());
                StartDiving();
            }

            //check where is the position of the player. I.e: is he in the air?

        }

      

        private IEnumerator Dash(float dashDuration, float dashCooldown, Vector2 dashDirection)
        {
            GhostEffect.StartEffect(8,0.01f,GetComponent<SpriteRenderer>() ,ShadowyColor);
            MainPlayer.PlayerBehaviourPermissions.CanDash =MainPlayer.PlayerBehaviourState.CanDash = false;
            //turn off gravity
            ActivateGravity(false);
            float duration = 0;

            while (duration < dashDuration)
            {
                duration += Time.deltaTime;
                MainPlayer.PlayerControllerView.AddForce(dashDirection);

                yield return null;
            }
            GhostEffect.StopEffect();
            ActivateGravity(true);
           // Debug.Log("dash cooldown "+dashCooldown);
            yield return new WaitForSeconds(dashCooldown);
            MainPlayer.PlayerBehaviourPermissions.CanDash =MainPlayer.PlayerBehaviourState.CanDash = true;
        }

        private void StartDiving()
        {
            
        }

        private void UpdateAnimation()
        {

            _animationController.SetFloat("Speed",Mathf.Abs(_horizontalAxisInput));
            _animationController.SetBool("Grounded", MainPlayer.PlayerControllerState.IsGrounded);
            _animationController.SetBool("MeleeAttacking", MainPlayer.PlayerBehaviourState.IsMeleeing);
            _animationController.SetFloat("verticalSpeed", MainPlayer.PlayerControllerView.Velocity.y); 
        }
    }
}
