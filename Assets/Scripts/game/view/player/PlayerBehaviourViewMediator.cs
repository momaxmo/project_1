﻿
using Assets.Scripts.common.config;
using Assets.Scripts.game.input.signals;
using Assets.Scripts.game.model;
using Assets.Scripts.game.player.model; 
using strange.extensions.mediation.impl;


namespace Assets.Scripts.game.view.player
{
    public class PlayerBehaviourViewMediator : Mediator
    {
        #region injections
        [Inject]
        public PlayerBehaviourView View { get; set; }

        [Inject]
        public MovementGameInputSignal InputSignal { get; set; }

        [Inject]
        public GameJumpButtonSignal JumpInputSignal { get; set; }

        [Inject]
        public GamePausedSignal PauseSignal { get; set; }
        [Inject]
        public LevelStateModel LevelState { get; set; }
        [Inject]
        public DashInputSignal DashinuInputSignal { get; set; }
        [Inject]

        public IPlayerModel PlayerModel { get; set; } 

     
        #endregion
        
        public override void OnRegister()
        {
            PauseSignal.AddListener(Pause);
            InputSignal.AddListener(OnHorizontalAndVerticalAxisInput);
            JumpInputSignal.AddListener(OnJumpInput);
            DashinuInputSignal.AddListener(DashInputSignal);
            View.Init(PlayerModel );
             
 
        }

        public override void OnRemove()
        {
            PauseSignal.RemoveListener(Pause);
            InputSignal.RemoveListener(OnHorizontalAndVerticalAxisInput);
            JumpInputSignal.RemoveListener(OnJumpInput);
            DashinuInputSignal.RemoveListener(DashInputSignal);
        }


        private void OnHorizontalAndVerticalAxisInput(float h, float v)
        {

            if (!LevelState.IsPaused)
                View.UpdateHorizontalAndVerticalAxisComponents(h, v);

        }

        private void OnJumpInput(bool jumpbuttonPressed   , bool jumpbuttonReleased)
        {
            if (!LevelState.IsPaused)
                View.UpdateJumpInput(jumpbuttonPressed , jumpbuttonReleased);

        }
        /// <summary>
        /// Disables the playerbeviour view when the game is paused. 
        /// </summary>
        /// <param name="pause"></param>
        private void Pause(bool pause)
        {
            View.enabled = !pause;
        }

        private void DashInputSignal()
        {
            View.StartDashing();
        }
        

    }
}
