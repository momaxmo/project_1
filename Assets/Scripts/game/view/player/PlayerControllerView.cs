﻿
using System.Collections;
using Assets.Scripts.Debugging;
using Assets.Scripts.exceptions;
using Assets.Scripts.game.player.model;
using Assets.Scripts.game.view.environment;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Assets.Scripts.game.view.player
{
    /// <summary>
    /// This class will only be used for a PlayerModel otherwise an exception is thrown. This class is a representation of the PlayerModel in world space
    /// and as such, contains methods, states that determine his/her current position. Furthermore, to respect StrangeIoC's framework and to keep 
    /// the view seperated as much as possible from the rest of the app, a signal is dispatched to update the current players speed during
    /// every Update
    /// </summary>
    public class PlayerControllerView : View, IPlayerControllerView, IDebuggable
    {
        #region fields
        [Space(10)]
        [Header("Collision Masks")]
        [SerializeField]
        private LayerMask _platformMask = LayerMask.NameToLayer("Ground");

        [SerializeField]
        private LayerMask _movingPlatformMask;

        [SerializeField]
        private LayerMask _oneWayPlatformMask;
        [SerializeField]
        private LayerMask _talliedMasks;

        private Vector2 _externalForces;

        [Space(10)]
        [Header("Physics")]
        [SerializeField]
        private Vector2 _velocity;

        [SerializeField]
        private Vector2 _nextPosition;
        [SerializeField]
        private BoxCollider2D _boxCollider;
        [SerializeField]

#pragma warning disable 169
        private GameObject _lastGameObjectWasStandingOn;
#pragma warning restore 169


        [SerializeField]
        private Rect _rayboundsRect;
        [SerializeField]
        private int _numberOfHorizontalRays;



        [SerializeField]
        private int _numberOfVerticalRays;

        [SerializeField]
        private float _rayOffset;

        [SerializeField]
#pragma warning disable 649
        private const float ObstacleHeightTolerance = 0.05f;
#pragma warning restore 649

        private const float SmallValue = 0.00001f;
        //private Signal<Vector2> _updateStatsSignal = new Signal<Vector2>();

        private bool _isPaused;
        #endregion
        #region injections

      //  private IPlayerModel _mainPlayer;
        [Inject] public IPlayerModel MainPlayer { get; set; }

        /*   public IPlayerControllerState ControllerState { get; set; }

         
           public IPlayerControllerParameters ControllerParameters { get; set; }
           */

        #endregion
        #region properties
        public BoxCollider2D BoxCollider
        {
            get
            {
                if (_boxCollider == null)
                {
                    _boxCollider = GetComponent<BoxCollider2D>();
                    if (_boxCollider == null)
                    {
                        throw new NoObjectAttachedException("A BoxCollider2D hasn't been attached to " + gameObject.name);
                    }
                }
                return _boxCollider;

            }
        }
        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        private Vector2 _velocityPreviousToBeingPaused;
        public bool IsPaused
        {
            get
            {
                return _isPaused;
            }
            set
            {
                if (value)
                {
                    _velocityPreviousToBeingPaused = Velocity;
                   
                    _velocity = Vector3.zero;
                }
                else
                {

                    _velocity = _velocityPreviousToBeingPaused;
 
                }

                _isPaused = value;
            }
        }
        public LayerMask PlatformMask
        {
            get { return _platformMask; }
            set { _platformMask = value; }
        }

        public LayerMask MovingPlatformMask
        {
            get { return _movingPlatformMask; }
            set { _movingPlatformMask = value; }
        }

        public LayerMask OneWayPlatformMask
        {
            get { return _oneWayPlatformMask; }
            set { _oneWayPlatformMask = value; }
        }

        public GameObject StandingOnGameObject { get; set; }

        public Vector2 Velocity
        {
            get { return _velocity; } 
        }

        public int NumberOfHorizontalRays
        {
            get
            {
                return _numberOfHorizontalRays;
            }
            set { _numberOfHorizontalRays = value; }
        }

        public int NumberOfVerticalRays
        {
            get { return _numberOfVerticalRays; }
            set { _numberOfVerticalRays = value; }
        }

        public float RayOffset
        {
            get { return _rayOffset; }
            set { _rayOffset = value; }
        }

        public Vector2 ColliderCenter
        {
            get { return Vector2.Scale(transform.localScale, _boxCollider.offset); }
        }

        public Vector2 ColliderSize
        {
            get { return Vector2.Scale(transform.localScale, _boxCollider.size); }
        }

        public Vector2 ColliderPositon
        {
            get
            {
                Vector2 returner = new Vector2(PlayerTranform.position.x + ColliderCenter.x, PlayerTranform.position.y + ColliderCenter.y);
                return returner;
            }

        }

        public Vector2 BottomPosition
        {
            get
            {
                Vector2 returner = new Vector2(ColliderPositon.x, ColliderPositon.y - (ColliderSize.y / 2));
                return returner;
            }

        }
        public Transform PlayerTranform
        {
            get
            {
                return transform;

            }
        }

     /*   public Signal<Vector2> UpdateStatsSignal
        {
            get
            { return _updateStatsSignal; }
            set { _updateStatsSignal = value; }
        }*/

        public LayerMask TalliedMasks
        {
            get { return _talliedMasks; }
            set { _talliedMasks = value; }
        }

        #endregion


        // ReSharper disable once UnusedMember.Local
        void  LateUpdate()
        {
           
            if (!IsPaused)
            { 
                UpdateVelocity();
                //UpdateStatsSignal.Dispatch(Velocity);
            }
        }
        public void Init(IPlayerModel playerModel)
        {
            IsPaused = false;


            _talliedMasks |= PlatformMask;
            _talliedMasks |= MovingPlatformMask;
            _talliedMasks |= OneWayPlatformMask;

            SetRayParams();/*
            
            _rayboundsRect = new Rect(BoxCollider.bounds.min.x, BoxCollider.bounds.min.y,
                BoxCollider.bounds.size.x, BoxCollider.bounds.size.y);*/
           // RegisterPlayer(playerModel);

        }


        public void RegisterPlayer(IPlayerModel player)
        {
            //_mainPlayer = new MainPlayerModel(player as MainPlayerModel);
          //  _mainPlayer.PlayerControllerState.Reset();
        }

        public IEnumerator DisableCollisions(float time)
        {
            TurnOffCollsions();
            yield return new WaitForSeconds(time);
            TurnOnCollisions();
        }
        public void TurnOnCollisions()
        {
            _talliedMasks |= PlatformMask;
            _talliedMasks |= MovingPlatformMask;
            _talliedMasks |= OneWayPlatformMask;
        }

        public void TurnOffCollsions()
        {
            _talliedMasks = 0;
        }

        private void UpdateVelocity()
        {

            _velocity.y += MainPlayer.PlayerControllerParameters.Gravity * Time.deltaTime;

            _nextPosition = Velocity * Time.deltaTime;

            MainPlayer.PlayerControllerState.WasGroundedLastFrame = MainPlayer.PlayerControllerState.IsCollidingBelow;
            MainPlayer.PlayerControllerState.Reset();

            CastRaysToTheSide();
            CastRaysBelow();
            CastRaysAbove();

            PlayerTranform.Translate(_nextPosition, Space.World);

            SetRayParams();

            if (Time.deltaTime > 0)
            {
                _velocity = _nextPosition / Time.deltaTime;   
            }
            _externalForces = Vector2.zero;

            _velocity.x = Mathf.Min(_velocity.x, MainPlayer.PlayerControllerParameters.MaxVelocity.x);
            _velocity.y = Mathf.Min(_velocity.y, MainPlayer.PlayerControllerParameters.MaxVelocity.y);

            if (!MainPlayer.PlayerControllerState.WasGroundedLastFrame && MainPlayer.PlayerControllerState.IsCollidingBelow)
            {
                MainPlayer.PlayerControllerState.JustGotGrounded = true;
            }
        }

        public void SetRayParams()
        {
            _rayboundsRect = new Rect(BoxCollider.bounds.min.x,
                BoxCollider.bounds.min.y,
                BoxCollider.bounds.size.x,
                BoxCollider.bounds.size.y);
            Debug.DrawLine(new Vector2(_rayboundsRect.center.x, _rayboundsRect.yMin), new Vector2(_rayboundsRect.center.x, _rayboundsRect.yMax), Color.red);
            Debug.DrawLine(new Vector2(_rayboundsRect.xMin, _rayboundsRect.center.y), new Vector2(_rayboundsRect.xMax, _rayboundsRect.center.y), Color.red);
        }

        private void CastRaysAbove()
        {
            if (MainPlayer.PlayerControllerState.IsGrounded)
            {
                return;
            }

            // ReSharper disable once NotAccessedVariable
            var rayLength = MainPlayer.PlayerControllerState.IsGrounded ? RayOffset : _nextPosition.y * Time.deltaTime;

            // ReSharper disable once RedundantAssignment
            rayLength += _rayboundsRect.height / 2;

            bool rayHitConnected = false;
            int hitConnectionIndex = 0;

            // ReSharper disable once UnusedVariable
            Vector2 verticalRaycastStart = new Vector2(_rayboundsRect.xMin + _nextPosition.x,
                _rayboundsRect.center.y);

            // ReSharper disable once UnusedVariable
            Vector2 verticalRaycastEnd = new Vector2(_rayboundsRect.xMax + _nextPosition.x,
                _rayboundsRect.center.y);

            RaycastHit2D[] hits = new RaycastHit2D[NumberOfVerticalRays];

            for (int i = 0; i < NumberOfVerticalRays; i++)
            {
                Vector2 rayOriginPoint = Vector2.Lerp(verticalRaycastStart, verticalRaycastEnd,
                   (float)i / (float)(NumberOfVerticalRays - 1));
                Debug.DrawRay(rayOriginPoint, Vector2.up * rayLength, Color.cyan);
                hits[i] = Physics2D.Raycast(rayOriginPoint, Vector2.up, rayLength, PlatformMask & ~OneWayPlatformMask);
                if (hits[i])
                {
                    rayHitConnected = true;
                    hitConnectionIndex = i;
                    break;
                }
            }

            if (rayHitConnected)
            {
               
                _velocity.y = 0;
              //  Debug.Log(" cast ray above condition  1  ");

                _nextPosition.y = hits[hitConnectionIndex].distance - _rayboundsRect.height / 2;
                MainPlayer.PlayerControllerState.IsCollidingAbove = true;
            }
        }

        private void CastRaysBelow()
        { 
            MainPlayer.PlayerControllerState.IsFalling = (_nextPosition.y < 0);

            if ((MainPlayer.PlayerControllerParameters.Gravity > 0) && (!MainPlayer.PlayerControllerState.IsFalling)) //in an area where he should be free floating
            {
                return;
            }

            float rayLength = (_rayboundsRect.height / 2) + RayOffset;
            if (_nextPosition.y < 0)
            {
                rayLength += Mathf.Abs(_nextPosition.y);
            }


            Vector2 verticalRayCastFromLeft = new Vector2(_rayboundsRect.xMin + _nextPosition.x,
                _rayboundsRect.center.y + RayOffset);
            Vector2 verticalRaycastToRight = new Vector2(_rayboundsRect.xMax + _nextPosition.x,
                _rayboundsRect.center.y + RayOffset);

            RaycastHit2D[] hits = new RaycastHit2D[NumberOfVerticalRays];
            float smallestDistance = 500000f;//float.MaxValue;
            int smallestDistanceIndex = 0;
            bool hitConnected = false;

            for (int i = 0; i < NumberOfVerticalRays; i++)
            {
                Vector2 rayStartPoint = Vector2.Lerp(verticalRayCastFromLeft, verticalRaycastToRight,
                    (float)i / (float)(NumberOfVerticalRays - 1));

                //the problem is over here. according to my observations, when the player is above a ground collider where the left or right edge is lining up with the end of the collider, it starts to flip WasGroundedLastFrame multiple time
                if ((_nextPosition.y > 0) && (!MainPlayer.PlayerControllerState.WasGroundedLastFrame))
                {
                    hits[i] = Physics2D.Raycast(rayStartPoint, -Vector2.up, rayLength,
                        PlatformMask & ~OneWayPlatformMask);
                   // Debug.DrawRay(rayStartPoint, -Vector2.up * rayLength, Color.magenta);
                }
                else
                {
                    hits[i] = Physics2D.Raycast(rayStartPoint, -Vector2.up, rayLength,
                       PlatformMask);
                   // Debug.DrawRay(rayStartPoint, -Vector2.up * rayLength, Color.magenta);
                }
                if ((Mathf.Abs(hits[smallestDistanceIndex].point.y - verticalRayCastFromLeft.y)) < SmallValue)
                {
                    break;
                }

                if (hits[i])
                {
                    hitConnected = true;

                    if (hits[i].distance < smallestDistance)
                    {
                        smallestDistanceIndex = i;
                        smallestDistance = hits[i].distance;
                    }
                }
            }
            if (hitConnected)
            { 
                MainPlayer.PlayerControllerState.IsFalling = false;
                MainPlayer.PlayerControllerState.IsCollidingBelow = true;

                _nextPosition.y = -Mathf.Abs(hits[smallestDistanceIndex].point.y - verticalRayCastFromLeft.y)
                    + _rayboundsRect.height / 2f + RayOffset;
               // Debug.Log(" cast ray below precondition    ");

                if (_externalForces.y > 0)
                {
                    _nextPosition.y += _velocity.y * Time.deltaTime;
                    MainPlayer.PlayerControllerState.IsCollidingBelow = false;
                  //  Debug.Log(" cast ray below condition 1    ");
                }

                if (!MainPlayer.PlayerControllerState.WasGroundedLastFrame && _velocity.y > 0)
                {
                    _nextPosition.y += _velocity.y * Time.deltaTime;
                   // Debug.Log(" cast ray below condition  2   ");

                }

                if (Mathf.Abs(_nextPosition.y) < SmallValue)
                {
                    _nextPosition.y = 0;
                  //  Debug.Log(" cast ray below condition  3   ");

                }
                StandingOnGameObject = hits[smallestDistanceIndex].collider.gameObject;
                /* if (StandingOnGameObject)
                 {*/
                MovingPlatformView movingPlatform = StandingOnGameObject.GetComponent<MovingPlatformView>();
                if (movingPlatform != null)
                {
                   // Debug.Log(" cast ray below condition 4   ");

                    float newPosY = movingPlatform.BoxCollider.bounds.max.y + ColliderSize.y / 2 +
                                    RayOffset * transform.localScale.y -
                                    _boxCollider.offset.y * PlayerTranform.localScale.y;
                    PlayerTranform.position = new Vector2(PlayerTranform.position.x, newPosY);
                    _nextPosition.y = 0;
                    
                }

            }
            else
            {
                MainPlayer.PlayerControllerState.IsCollidingBelow = false;
            }

        }

        private float _castRayToSideDirection =1;
        /// <summary>
        /// Cast rays from the side of the PlayerModel to determine if he is ok to run 
        /// </summary>
        private void CastRaysToTheSide()
        {
            float direction = Mathf.Sign(transform.localScale.x);
           /*// float direction = 1;
            if (_velocity.x < 0 || _externalForces.x < 0)
            {
             
                direction = -1;
            }*/

            //cast a ray proportional to the current velocity
            float horizontalRayLength = Mathf.Abs(_velocity.x * Time.deltaTime) + _rayboundsRect.width / 2 + RayOffset * 2;


            Vector2 horizontalRayCastFromBottom = new Vector2(_rayboundsRect.center.x, _rayboundsRect.yMin + ObstacleHeightTolerance);

            Vector2 horizontalRayCastToTop = new Vector2(_rayboundsRect.center.x, _rayboundsRect.yMax - ObstacleHeightTolerance);

            RaycastHit2D[] hitsStorage = new RaycastHit2D[NumberOfHorizontalRays];

            //iterate through the raycasts and try to detect a collision

            for (int i = 0; i < NumberOfHorizontalRays; i++)
            {
                Vector2 startingPoint = Vector2.Lerp(horizontalRayCastFromBottom, horizontalRayCastToTop,
                  (float)i / (float)(NumberOfHorizontalRays - 1));

                if (MainPlayer.PlayerControllerState.WasGroundedLastFrame && i == 0)
                {
                    hitsStorage[i] = Physics2D.Raycast(startingPoint, direction * Vector2.right, horizontalRayLength, PlatformMask);

                    Debug.DrawRay(startingPoint, direction * Vector2.right * horizontalRayLength, Color.green);
                }
                else
                {
                    Debug.DrawRay(startingPoint, direction * Vector2.right * horizontalRayLength, Color.green);

                    hitsStorage[i] = Physics2D.Raycast(startingPoint, direction * Vector2.right, horizontalRayLength, PlatformMask & ~OneWayPlatformMask);
                }

                if (hitsStorage[i].distance > 0)
                {

                    float hitAngle = Mathf.Abs(Vector2.Angle(hitsStorage[i].normal, Vector2.up));  //get the angle between hit
                    if (hitAngle > MainPlayer.PlayerControllerParameters.MaximumSlopeAngle)
                    {
                        if (direction < 0) //going left?
                        {
                            MainPlayer.PlayerControllerState.IsCollidingLeft = true;
                          //  Debug.Log(" cast ray to the side condition 1 ");
                        }
                        else
                        {
                            MainPlayer.PlayerControllerState.IsCollidingRight = true;
                            //Debug.Log(" cast ray to the side condition 2 ");
                        }
                        MainPlayer.PlayerControllerState.SlopeAngleOK = false;

                        if (direction <  0)
                        {
                            _nextPosition.x = -Mathf.Abs(hitsStorage[i].point.x - horizontalRayCastFromBottom.x)
                                + _rayboundsRect.width / 2
                                + _rayOffset * 2;
                            //Debug.Log(" cast ray to the side condition 3");
                        }
                        else
                        {
                            _nextPosition.x = Mathf.Abs(hitsStorage[i].point.x - horizontalRayCastFromBottom.x)
                                - _rayboundsRect.width / 2
                                - _rayOffset * 2;
                           // Debug.Log(" cast ray to the side condition 4 ");
                        }
                        _velocity = new Vector2(0, _velocity.y);//prevent the player from moving through to the side.  
                        break;
                    }
                }

            }
        }





        public void AddForce(Vector2 v)
        {
            _velocity += v;
            _externalForces += v;
        }

        /// <summary>
        /// Sets the horizontal force to be applied to the character
        /// </summary>
        /// <param name="p"></param>
        public void SetHorizontalForce(float p)
        { 
            _velocity.x = p;
            _externalForces.x = p;  
        }
        public void SetVerticalForce(float p)
        { 
            _velocity.y = p;
            _externalForces.y = p; 
 
        }
        public void PauseUnpause(bool pauseValue)
        {
            IsPaused = pauseValue;
        }

        public string ViewDebugText()
        {
            string output = " Player controller view";
            output += "\n velocity applied : " + _velocity;
            output += "\n force applied : " + _externalForces;
           // output += "\n hit angle " + hitAngle;
            output += "\n next position: " + _nextPosition;
            return output;

        }

        public string Id
        {
            get { return GetInstanceID().ToString(); }
        }
    }
}
