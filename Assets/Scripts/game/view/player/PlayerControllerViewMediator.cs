﻿ 
using Assets.Scripts.common.config;
using Assets.Scripts.game.player.model;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.game.view.player
{
   public class PlayerControllerViewMediator: Mediator
    {
       [Inject]

       public IPlayerModel PlayerModel { get; set; } 
       [Inject]
       public PlayerControllerView View { get; set; }

       [Inject]
       public PlayerVelocityUpdatedSignal UpdateSignal { get; set; }

       [Inject]
       public GamePausedSignal GamePaused { get; set; }
        
       public override void OnRegister()
       { 
           View.Init(PlayerModel); 
      //     View.UpdateStatsSignal.AddListener(UpdateVelocity);
           GamePaused.AddListener(Pause);
            
       }

       public override void OnRemove()
       {
      //     View.UpdateStatsSignal.RemoveListener(UpdateVelocity);
           GamePaused.RemoveListener(Pause);
       }

       public void UpdateVelocity(Vector2 vel)
       {
           UpdateSignal.Dispatch(vel);
          
       }

       public void Pause(bool pauseStatus)
       {
           View.Enabled = !pauseStatus;
       
       }
    }
}
