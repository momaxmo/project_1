﻿/*

using Assets.Scripts.common.config;
using Assets.Scripts.game.levels.controller.input;
using Assets.Scripts.game.model;
using Assets.Scripts.game.model.PlayerModel;
using strange.extensions.mediation.impl;
 
using UnityEngine;

namespace Assets.Scripts.game.view
{

    public class PlayerView : View, IPlayerView
    {
        [Inject]
        public IPlayerControllerParameters ControllerParameters { get; set; } 
        public float MaxSpeed;
        public float MaxAcceleration;

        #region  Velocity and speed parameters

        private Vector2 _externalForce;
        private Vector2 _velocity;
        #endregion


        public float GroundingCheckRadius = 0.2f;
        private bool _isPaused;
       
        private float _horizontalSpeed;
        private float _verticalSpeed;
        private int input;
        
        private Rigidbody2D _rigidbody2D;

        private Vector2 _previousFramePosition;
       
        #region jumping parameters
        public GameObject GroundChecker;
        private bool _isGrounded;
        public LayerMask GroundLayer;
       // private bool _pressJumpButton;//jump button pressed?
        public int ExtraNumberOfJumps;
        private int _extraJumps;
        private bool _canJump;
        public float JumpSpeed;
        public float MaxJumpDuration; //for variable jumping
        private float _jumpTimeDurationRemaning; //will be incremented and checked if the duration is longer than MaxJumpDuration

        #endregion
        public string output;
        public UnityEngine.UI.Text text;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public bool JumpButtonIsPressed { get; set; }
        public bool JumpButtonHeldDown { get; set; }
        public bool JumpButtonIsReleased { get; set; }
        public bool IsGrounded
        {
            get
            {
                bool val = false;
                Collider2D[] colliders = Physics2D.OverlapCircleAll(GroundChecker.transform.position,
                  GroundingCheckRadius, GroundLayer);
                foreach (Collider2D col in colliders) //check if any ground collider is overlapping the area
                {
                    if (col.gameObject != gameObject)
                    {
                        val = true;
                    }
                }
                return val;
            }

           
        }
        public float MoveSpeed
        {
            get { return MoveSpeed; }
            set { MoveSpeed = value; }
        }

  
        public bool IsPaused
        {
            get
            { return _isPaused; }
            set { _isPaused = value; }
        }

        public Rigidbody2D PlayerRigidbody2D
        {
            get
            {
                if (_rigidbody2D == null)
                {
                    _rigidbody2D = GetComponent<Rigidbody2D>();
                }
                return _rigidbody2D;
            }
        }
        public bool IsLookingRight { get; set; }
        public void SetAction(float h, float v,  int evt)
        {
            _horizontalSpeed = h;
            _verticalSpeed = v;
            
            input = evt;
        }

        
        private void FixedUpdate()
        {
            ValidateInputs();
            Move();
            Jump();
            
        }

        private void ValidateInputs()
        {
            bool useBooster1 = (input & GameInputEvent.UseBooster1) > 0;
            bool useBooster2 = (input & GameInputEvent.UseBooster2) > 0;

        }

        public void AddExternalForce(Vector2 externalForce)
        {
            _velocity += externalForce;
            _externalForce += externalForce;
        }

        /// <summary>
    /// Variable jump with the option to do multiple jumps by changing the class property "ExtraNumberOfJumps"
    /// Checks for 2 inputs: jump button press, jump button held. The longer the button is held, the higher the jump.
    /// 
    /// </summary>
        private void Jump()
        {
            bool isGrounded = IsGrounded;
            Vector2 newJumpVelocity = _rigidbody2D.velocity;
            if (isGrounded)
            {
                _extraJumps = ExtraNumberOfJumps;
                _canJump = true;
              //  _firstJumpMade = false;
              //  _jumpCancelled = false;
                _jumpTimeDurationRemaning = 0;

                if (JumpButtonIsPressed)
                {
                    newJumpVelocity.y = JumpSpeed;
                    _rigidbody2D.velocity = newJumpVelocity;
                    _jumpTimeDurationRemaning =0;
                 //   _firstJumpMade = true; 
                } 
            }
            else if (JumpButtonIsPressed)
            {
                if (_extraJumps > 0)
                {
                    newJumpVelocity.y = JumpSpeed;
                    _rigidbody2D.velocity = newJumpVelocity;
                    _jumpTimeDurationRemaning = 0;
                    _extraJumps--;
                } 
            }
            else if (JumpButtonHeldDown)
            {
                if (_jumpTimeDurationRemaning < MaxJumpDuration /1000)
                {
                    newJumpVelocity.y = JumpSpeed;
                    _rigidbody2D.velocity = newJumpVelocity;
                }
                _jumpTimeDurationRemaning += Time.deltaTime;
            }
        }


        /// <summary>
        /// The move methods adds an acceleration component, the reason is to give a nice ramping up to the maximum speed. The longer the PlayerModel runs, the faster 
        /// he goes until he hits max speed
        /// Acceleration =  am * ((pt-pc)/ |pt-pc| )
        /// currently there is an issue with movement. when the character jumps and then lands, the 
        /// Vel = vc +a*t
        /// </summary>
        private void Move()
        {
            float xAccel = _horizontalSpeed*MaxAcceleration;
            Vector3 newVelocity = PlayerRigidbody2D.velocity;
            newVelocity.x += xAccel*Time.fixedDeltaTime;
            if (Mathf.Abs(newVelocity.x) > MaxSpeed)
            {
                newVelocity.x = MaxSpeed * Mathf.Sign(_horizontalSpeed);
            }
             
            PlayerRigidbody2D.velocity = newVelocity;           
        }

        public void SetJumpAction(bool jumpbuttonPressed, bool jumpButtonHeldDown,bool jumpbuttonReleased)
        { 
            JumpButtonIsPressed = jumpbuttonPressed;
            JumpButtonHeldDown = jumpButtonHeldDown;
            JumpButtonIsReleased = jumpbuttonReleased;
        }

        private PlayerControllerState _playerState;
        public PlayerControllerState CharacterState
        {
            get
            {
                if (_playerState == null)
                {
                    _playerState = new PlayerControllerState();
                    _playerState.Reset();
                }
                return _playerState;
            } 
        }
    }
}
*/
