﻿/*


using System.Diagnostics;
using Assets.Scripts.common.config;
using strange.extensions.mediation.impl;

namespace Assets.Scripts.game.view
{
    public class PlayerViewMediator: Mediator
    {
        [Inject]
        public PlayerView PView { get; set; }

        [Inject]
        public MovementGameInputSignal InputSignal { get; set; }

        [Inject]
        public GameJumpButtonSignal JumpInputSignal { get; set; }

        public override void OnRegister()
        { 
            InputSignal.AddListener(OnInput);
            JumpInputSignal.AddListener(OnJumpInput);
        }

        public override void OnRemove()
        {
            InputSignal.RemoveListener(OnInput);
            JumpInputSignal.RemoveListener(OnJumpInput);
        }

        private void OnInput(float h, float v , int evt)
        {
            PView.SetAction(h,v,evt);
            
        }

        private void OnJumpInput(bool jumpbuttonPressed,bool jumpbuttonHeldDown, bool jumpbuttonReleased)
        {
            PView.SetJumpAction(jumpbuttonPressed,jumpbuttonHeldDown, jumpbuttonReleased);
        }
    }
}
*/
