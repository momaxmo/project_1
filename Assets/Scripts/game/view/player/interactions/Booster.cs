﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.common.utilities; 
using Assets.Scripts.game.boosters.model;
using Assets.Scripts.game.inventory.Model;
using UnityEngine;

namespace Assets.Scripts.game.view.player.interactions
{

    /// <summary>
    /// A booster class that wraps the boostermodel. Does checks  on whether the booster can be used
    /// </summary>
    public class Booster : IBooster
    {
        private bool _canUse; 
        private float _previousTimeUsed = -1000;
        private IInventoryItem _currentBoosterModel;
        // this queue will help determine what the previous boosters were, to work around potential exploits of switching between different boosters
        private List<IInventoryItem> _previousBoosters = new List<IInventoryItem>(); 
         [Inject]
        public IRoutineRunner RoutineRunner { get; set; }
        
         public BoosterDb BoosterDatabase { get; set; }

        public Booster(BoosterDb db)
        {
            BoosterDatabase = db;
        }
        public bool CanUse
        {
            get
            {
                _canUse = false;
                if (_currentBoosterModel == null)
                {
                    _canUse = false;
                }
                else if (InventoryItem.Duration == BoosterDuration.PermanentEffect)
                {
                    _canUse = false; //todo: this needs to be false
                }
                else if (InventoryItem.Duration == BoosterDuration.OneTimeUsage)
                {
                    _canUse = InventoryItem.RemainingCharges > 0;
                }
                else if (InventoryItem.Duration == BoosterDuration.Cooldown)
                {

                    if (Time.time - _previousTimeUsed > InventoryItem.CoolDownTimer)
                    {
                        _canUse = true;
                        InventoryItem.PreviousTimeUsed= Time.time; 
                    }

                }

                return _canUse;
            }
            set { _canUse = value; }
        }

        public IInventoryItem InventoryItem
        {
            get
            {
                if (_currentBoosterModel == null)
                {
                    _currentBoosterModel = BoosterDatabase.Boosters["PlaceholderBooster"];
                }
                return _currentBoosterModel;
            }
            set { _currentBoosterModel = value; }
        }

        public void Use()
        {
            if (CanUse)
            {
                RoutineRunner.StartCoroutine(UseBoosterAndStartCountdown());
            }
        }

        public void SwitchBooster(int id)
        {
            _previousBoosters.Add(InventoryItem);
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// When a booster is used, starts a countdown and then dispatches a signal that the booster is ready to be used again
        /// </summary>
        /// <returns></returns>
        protected IEnumerator UseBoosterAndStartCountdown( )
        {
            //todo dispatch signal item has been used
            yield return new WaitForSeconds(InventoryItem.CoolDownTimer);
            //todo dispatch signal that item is ready to be used
        }

        public void ResetLimitations()
        {
            throw new System.NotImplementedException();
        }
    }

    public enum BoosterNumber
    {
        One=1,
        Two=2
    }
}
