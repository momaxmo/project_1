﻿ 
using Assets.Scripts.common.config;
using Assets.Scripts.game.input.signals;
using strange.extensions.mediation.impl;

namespace Assets.Scripts.game.view.player.interactions
{
   public  class BoosterUsageMediator: Mediator
    {
        [Inject]
        public UseBooster1Signal UseBoosterSignal1 { get; set; }
        [Inject]
        public UseBooster2Signal UseBoosterSignal2 { get; set; }

        [Inject]
        public BoosterUsageView View { get; set; }

       public override void OnRegister()
       {
           UseBoosterSignal1.AddListener(UseBooster1);
           UseBoosterSignal2.AddListener(UseBooster2);
       }

       public override void OnRemove()
       {
           UseBoosterSignal1.RemoveListener(UseBooster1);
           UseBoosterSignal2.RemoveListener(UseBooster2);
       }

       private void UseBooster1()
       {
           View.UseBooster(1);
       }

       private void UseBooster2()
       {
           View.UseBooster(2);
       }

    }
}
