﻿ 

using Assets.Scripts.game.boosters;
using Assets.Scripts.game.inventory.Model;
using strange.extensions.mediation.impl;
 
namespace Assets.Scripts.game.view.player.interactions
{
    /// <summary>
    /// Class for using boosters. This 
    /// </summary>
   public class BoosterUsageView : View
    {
       IInventoryItem Booster1 { get; set; }
       IInventoryItem Booster2 { get; set; }

       IInventoryItem[] Boosters { get; set; }
       private IInventoryItem[] _boosters = new IInventoryItem[2];
       /// <summary>
       /// nreed a booster model to be injected into this one
       /// </summary>
        public void UseBooster(int id)
        {
            if(id == 1)
                UseBooster1();
            else 
                UseBooster2();

        }

        private void UseBooster1()
        {
            //check if the booster is null, means PlayerModel doesn't have a booster attached
            if (Booster1 == null)
            {
                return;
            }
        }

        private void UseBooster2()
        {
            
        }

    }
}
