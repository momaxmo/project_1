﻿ 
using Assets.Scripts.game.inventory.Model;

namespace Assets.Scripts.game.view.player.interactions
{
   public interface IBooster
    {
        bool CanUse { get; set; }
        IInventoryItem InventoryItem { get; set; }

        void Use();

        void SwitchBooster(int id);

        void ResetLimitations();

    }
}
