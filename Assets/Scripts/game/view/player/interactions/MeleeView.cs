﻿ 

using System.Collections;
using Assets.Scripts.game.player.model;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Assets.Scripts.game.view.player.interactions
{
   public class MeleeView: View
   {
       public BoxCollider2D MeleeBoxCollider; //melee box collider
      // public PlayerBehaviourView PlayerBehaviour;
       public float AttackDuration = 0.3f;
       public Signal<GameObject> HitEnemySignal = new Signal<GameObject>();
       [Inject]
       public IPlayerModel PlayerModel { get; set; } //todo: place it in the mediator

       public void Init()
       {
           MeleeBoxCollider.enabled = false;
       }
      public  void Melee()
       {
          
           if (!PlayerModel.PlayerBehaviourPermissions.CanMeleeAttack) //PlayerModel cannot melee attack. Do nothing
           {
               return;
           }
        
           if (PlayerModel.PlayerBehaviourState.IsDead) // PlayerModel is dead... do nothing
           {
               return; 
           }
           if (!PlayerModel.PlayerBehaviourState.CanMoveFreely) // PlayerModel is in a state where he cannot move. Do nothing
           {
               return;
           }
           if (PlayerModel.PlayerBehaviourState.CanMelee)
           {
             //  Debug.Log("melee");
               PlayerModel.PlayerBehaviourState.IsMeleeing = true;
               MeleeBoxCollider.enabled = true;
               StartCoroutine(MeleeEnd());
           }
       }

       /// <summary>
       /// Melee hits on trigger
       /// </summary>
       /// <param name="col"></param>
       // ReSharper disable once UnusedMember.Local
       void OnTriggerEnter2D(Collider2D col)
       {
           if (col.tag == "Enemy")
           {
               HitEnemySignal.Dispatch(col.gameObject); //dispatch a signal on enemy hit
           }
       }

       IEnumerator MeleeEnd()
       {
           yield return new WaitForSeconds(AttackDuration);
           MeleeBoxCollider.enabled = false;
           PlayerModel.PlayerBehaviourState.IsMeleeing = false;
       }
   }
}
