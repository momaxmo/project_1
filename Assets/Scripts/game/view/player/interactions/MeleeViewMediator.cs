﻿ 
using Assets.Scripts.common.config;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.game.view.player.interactions
{
    public class MeleeViewMediator: Mediator
    {
        [Inject]
        public MeleeHitSignal HitSignal { get; set; }

        [Inject]
        public MeleeView View { get; set; }
        [Inject]
        public StartMeleeHitSignal StartHit { get; set; }
        public override void OnRegister()
        {
            View.Init();
            
            View.HitEnemySignal.AddListener(DispatchHitSignal);
            StartHit.AddListener(Hit);
        }

        public override void OnRemove()
        {
            View.HitEnemySignal.RemoveListener(DispatchHitSignal);
            StartHit.RemoveListener(Hit);
        }

        private void DispatchHitSignal(GameObject enemy)
        {
            HitSignal.Dispatch(enemy);
        }

        private void Hit()
        {
            Debug.Log("hitting in mediator");
            View.Melee();
        }
        

    }
}
