﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.pathfinding
{
    public class Heap<T> where T : IHeapItem<T>
    {
        private T[] _items;
        private int _currentItemCount;

        public int CurrentItemCount
        {
            get { return _currentItemCount; }
        }

        public Heap(int maxHeapSize)
        {
            _items = new T[maxHeapSize];
        }

        /// <summary>
        /// adds an item to the heap and then sorts up
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            item.HeapIndex = CurrentItemCount;
            _items[_currentItemCount] = item;
            SortUp(item);
            _currentItemCount++;
        }
        /// <summary>
        /// Removes the first item, sets the last item in index as first.Sort down then return the item that was removed
        /// </summary>
        /// <returns></returns>
        public T RemoveFirstItem()
        {
            T item = _items[0];
            _currentItemCount--;
            _items[0] = _items[CurrentItemCount];
            _items[0].HeapIndex = 0;
            SortDown(_items[0]);
            return item;
        }

        private void SortDown(T item)
        {
            while (true)
            {
                int leftChildIndex = (2 * item.HeapIndex) + 1;
                int rightChildIndex = (2 * item.HeapIndex) + 2;
                int swapIndex = 0;

                if (leftChildIndex < _currentItemCount)
                {
                    swapIndex = leftChildIndex;
                    if (rightChildIndex < _currentItemCount)
                    {
                        if (_items[leftChildIndex].CompareTo(_items[rightChildIndex]) < 0) //do we want to sort through the right part of the tree?
                        {
                            swapIndex = rightChildIndex;
                        }
                    }
                    if (item.CompareTo(_items[swapIndex]) < 0)//verify the cost of the current node in the tree. If the item at index (swapindex) is lower, then swap.  
                    {
                        Swap(item,_items[swapIndex]);
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
        }

        private void SortUp(T item)
        {
            //get the parent index
            int parentItemIndex = (item.HeapIndex - 1)/2;

            while (true)
            {
                T parentItem = _items[parentItemIndex];
                if (item.CompareTo(_items[parentItemIndex]) > 0)
                {
                    Swap(item, parentItem);
                }
                else
                {
                    break;
                }
            }
        }
        public void UpdateItem(T item)
        {
            SortUp(item);
        }

        public bool ContainsT(T item)
        {
            return Equals(_items[item.HeapIndex], item);
        }

        private void Swap(T itemA, T itemB)
        {
            _items[itemA.HeapIndex] = itemB;
            _items[itemB.HeapIndex] = itemA;
            int tempIndex = itemA.HeapIndex;
            itemA.HeapIndex = itemB.HeapIndex;
            itemB.HeapIndex = tempIndex;
        }
    }

    public interface IHeapItem<T> : IComparable
    {
        int HeapIndex { get; set; }
    }
}
