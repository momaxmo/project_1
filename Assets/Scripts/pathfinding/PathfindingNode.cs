﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.pathfinding
{
    public class PathfindingNode: IHeapItem<PathfindingNode>
    {
        #region fields
        private int _id;
        private Vector3 _worldPosition;
        private int _hCost;
        private int _fCost;
        private int _gCost;
        
        #endregion

        #region publicly accessible properties
        public int HeapIndex { get; set; }
        public NavigationNodeType NavNodeType { get; set; }
        public bool IsWalkable { get; set; }

        public int FCost
        {
            get { return _gCost + _hCost; }
        }
        public NodePosition Position { get; set; }

        #endregion

        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }

   

       
    }

    public enum NavigationNodeType
    {
        None, Platform,LeftEdge,RightEdge,Solo
    }
    public struct NodePosition:IEquatable<NodePosition>
    {
        public int X;
        public int Y;

        public NodePosition(int x, int y)
        {
            X = x;
            Y = y;
        }
        public bool Equals(NodePosition other)
        {
            return other.X == X && other.Y == Y;
        }

        public static bool operator ==(NodePosition first, NodePosition other)
        {
            return first.Equals(other);

        }

        public static bool operator !=(NodePosition first, NodePosition other)
        {
            return !first.Equals(other);
        }
        

    }
}
