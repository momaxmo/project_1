﻿ using System.Collections.Generic; 
using UnityEngine;

namespace Assets.Scripts.pathfinding
{
    /// <summary>
    /// holds a reference to all the plaforms in the game
    /// </summary>
   public class PlatformGroup: MonoBehaviour
    {
        private List<PlatformPointsBuilder> _platformList = new List<PlatformPointsBuilder>(3);

        void Start()
        {
            //find all platforms in the level
            GameObject[] platformObjs = GameObject.FindGameObjectsWithTag("Platform");
            for (int i = 0; i < platformObjs.Length; i++)
            {
                PlatformPointsBuilder pb = platformObjs[i].GetComponent<PlatformPointsBuilder>();
                pb.Init();
                _platformList.Add(pb);
            }
        }
        
    }
}
