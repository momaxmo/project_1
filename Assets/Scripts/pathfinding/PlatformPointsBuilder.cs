﻿ 
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.pathfinding
{
  public   class PlatformPointsBuilder: MonoBehaviour
    {
        PolygonCollider2D _col;
        public GameObject pref;
        private Vector2[] _points;
        private List<int> _pathIndex = new List<int>(1);
        private int[] _vertices;
        
        // Use this for initialization
        void Start()
        {
            _col = GetComponent<PolygonCollider2D>();
            _points = _col.points;
            //get the bottom left most vertex and the bottom right most vertex
            float bottomLeftMostX = float.MaxValue;
            float bottomRightMostX = float.MinValue;
            float bottomLeftMostY = float.MaxValue;
            float bottomRightMostY = float.MaxValue;

            int smallestYIndex = 0;
            float smallestYValue = float.MaxValue;

            int secondSmallestYIndex = 0;
            float secondSmallYValue = float.MaxValue;

            int bottomRightIndex = 0;
            int bottomLeftIndex = 0;

            for (int i = 0; i < _points.Length; i++)
            {
                if (_points[i].y < smallestYValue)
                {
                    if (secondSmallYValue > smallestYValue)
                    {
                        secondSmallYValue = smallestYValue;
                        secondSmallestYIndex = smallestYIndex;
                    }
                    smallestYValue = _points[i].y;
                    smallestYIndex = i;
                    //check if the second value has been assigned a numerical value

                    continue;

                }
                if (_points[i].y < secondSmallYValue && _points[i].y > smallestYValue)
                {
                    secondSmallYValue = _points[i].y;
                    secondSmallestYIndex = i;
                    //check if the second value has been assigned a numerical value

                    continue;

                }

            }

            _vertices = new int[_points.Length - 2];
            //  List<int> _vertices = new List<int>(_points.Length -2);
            int vertexIndex = 0;
            for (int i = 0; i < _points.Length; i++)
            {
                if (i == smallestYIndex || i == secondSmallestYIndex)
                {
                    continue;
                }
                _vertices[vertexIndex] = i;
                vertexIndex++;
                // _vertices.Add(i);
            }
            Vector2[] trimmedVertices = new Vector2[_vertices.Length];
            for (int i = 0; i < _vertices.Length; i++)
            {
                trimmedVertices[i] = _points[_vertices[i]];
            }
            Array.Sort(trimmedVertices, _vertices, Vector2XComparer.SortAscending());
 
            DrawVertices();
        }

      public void Init()
      {
          
      }
        void DrawVertices()
        {
            for (int i = 0; i < _vertices.Length; i++)
            {
                Instantiate(pref, transform.TransformPoint(_points[_vertices[i]] + _col.offset), Quaternion.identity);
            }
        }


        private class Vector2XComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                Vector2 v1 = (Vector2)x;
                Vector2 v2 = (Vector2)y;
                if (v1.x > v2.x)
                {
                    return 1;
                }
                if (v1.x < v2.x)
                {
                    return -1;
                }

                return 0;
            }


            public static IComparer SortAscending()
            {
                return new Vector2XComparer();
            }
        }
    }
}
