﻿ 
using UnityEngine;

namespace Assets.Scripts.pathfinding
{
    /// <summary>
    /// Represents the grid that with a list of all walkable points
    /// </summary>
  public class WalkableGrid:MonoBehaviour
    {
        private int _nodeCount;

        public bool ShowGrid;
        public Color WalkableColor;
        public PathfindingNode[,] Grid;
        private  float _nodeDiameter;
        public float NodeRadius;
        public float CubeOffset;
        public LayerMask PlatformLayerMask;
        public Vector2 WorldSize;
        private int _gridSizeX, _gridSizeY;
        
        private int _maxSize;

        public WalkableGrid(int gridSizeY)
        {
            _gridSizeY = gridSizeY;
        }

        public int MaxSize { get {return _gridSizeX * _gridSizeY; } }

        public void Init()
        {
            _nodeDiameter = NodeRadius*2;
            _gridSizeX = Mathf.RoundToInt(WorldSize.x / _nodeDiameter);
            _gridSizeY = Mathf.RoundToInt(WorldSize.y / _nodeDiameter);

        }

        public void CreateGrid()
        {
            bool platformStarted  ;
            int platformIndex = 0;
            Grid = new PathfindingNode[_gridSizeX,_gridSizeY];
            Vector3 bottomLeftPosition = transform.position - Vector3.right*WorldSize.x/2 -
                                         Vector3.up*WorldSize.y/2;
            int id = 0;
            for (int x = 0; x < _gridSizeX; x++)
            {
                platformStarted = false;
                for (int y = 0; y < _gridSizeY; y++)
                {
                    Vector3 worldPosition = bottomLeftPosition + Vector3.right*(x*_nodeDiameter + NodeRadius) +
                                            Vector3.up*(y*_nodeDiameter + NodeRadius);
                    if (!platformStarted)
                    {
                        //Check if the cell below this one contains a platform

                    }

                }
            }
            
        }

        public void CreateWalkableGrid()
        {
            
        }
        void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(transform.position, new Vector3(WorldSize.x, CubeOffset, WorldSize.y));

            if (Grid != null && ShowGrid)
            {

                foreach (PathfindingNode n in Grid)
                {
                    Gizmos.color = (n.IsWalkable ? WalkableColor : Color.gray);
              /*      if (n.isExitNode)
                        Gizmos.color = Color.black;*/
                    Vector3 cubePos = new Vector3(n.Position.X, n.Position.Y, 0);
                    Gizmos.DrawCube(cubePos, Vector3.one * (_nodeDiameter - .1f));
                    

                }
            }

        }
    }
}
