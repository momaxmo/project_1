﻿using System;
using UnityEngine;

namespace Assets.Scripts.pathfinding.navigation
{
    [Serializable]
    public class NavLink
    {
        [SerializeField]
        private JumpTrajectory _jumpTrajectory;
        public Vector2 DestinationCoordinates { get; set; }

        public NavigationPoint LeftNavigationPointLink
        {
            get
            {
                return _leftNavigationPointLink;
            }
            set { _leftNavigationPointLink = value; }
        }

        public NavigationPoint RightNavigationPointLink
        {
            get { return _rightNavigationPointLink; }
            set { _rightNavigationPointLink = value; }
        }

        public NavigationPoint FallNavigationPointLink
        {
            get { return _fallNavigationPointLink; }
            set { _fallNavigationPointLink = value; }
        }

        public NavigationPoint JumpNavigationPointLink
        {
            get { return _jumpNavigationPointLink; }
            set { _jumpNavigationPointLink = value; }
        }

        public JumpTrajectory Trajectory
        {
            get { return _jumpTrajectory; }
            set { _jumpTrajectory = value; }
        }

        [SerializeField]
        public int LinkScore;

        [SerializeField]
        private NavigationPoint _leftNavigationPointLink;
        [SerializeField]
        private NavigationPoint _rightNavigationPointLink;
        [SerializeField]
        private NavigationPoint _fallNavigationPointLink;
        [SerializeField]
        private NavigationPoint _jumpNavigationPointLink;



    }
    [Serializable]
    public class JumpTrajectory
    {
        public float JumpHeight;
        public float Speed;
        public float Acceleration;
        public float Gravity;
    }
}
