﻿using System; 
using System.Collections.Generic;
using Assets.Scripts.tools;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Assets.Scripts.pathfinding.navigation
{
    [Serializable]
    public class NavMap : MonoBehaviour
    {
        [HideInInspector]
        private NavMapData _tempNavMapData;

        private NavMapData _navMapData; //to be used when saving an loading map data
        [SerializeField]
        private bool _navMapLoaded;
        [SerializeField]
        private bool _navMapCreated;
        [SerializeField]
        private bool _showButtonGroup;
        public NavMapData TempNavMapData
        {
            get { return _tempNavMapData; }
            set { _tempNavMapData = value; }
        }

        public LayerMask GroundableMask
        {
            get { return TempNavMapData.PlatformLayerMask; }
            set { TempNavMapData.PlatformLayerMask = value; }

        }
        [SerializeField]
        private Vector3[] _points;
        public Vector3[] Points
        {
            get
            {

                return _tempNavMapData.Points;
            }
            set
            {
                float sizeX = Mathf.Abs(value[3].x - value[0].x);
                float sizeY = Mathf.Abs(value[3].y - value[0].y);

                SetNewSize(sizeX,sizeY);
                _points = _tempNavMapData.Points = value;
            }
        } 
        public float NodeSize
        {
            get { return _tempNavMapData.NodeSize; }
            set {   _tempNavMapData.NodeSize = value; }
        }
        [SerializeField]
        private float _sizeX;
        public float SizeX
        {
            get { return _tempNavMapData.SizeX; }
            set { _sizeX = _tempNavMapData.SizeX = value; }
        }
        [SerializeField]
        private float _sizeY;
        public float SizeY
        {
            get { return _tempNavMapData.SizeY; }
            set { _sizeY = _tempNavMapData.SizeY = value; }
        }

        public int[] Length()
        {
            int[] length = new int[2];
            length[0] = _tempNavMapData.Grid.GetLength(0);
            length[1] = _tempNavMapData.Grid.GetLength(1);
            return length;
        }
        //[HideInInspector]
        public bool Scanned;

        public int Count
        {
            get { return _tempNavMapData.NodeCount; }
            set { _tempNavMapData.NodeCount = value; }
        }

        public NavMapData MapData
        {
            get { return _navMapData; }
            set { _navMapData = value; }
        }

        public bool Load(string levelName)
        {
            string path = "";

#if UNITY_EDITOR

            path = ExitTheVoidFilePathRefs.CleanedNavigationPath(EditorApplication.currentScene);
            _tempNavMapData = AssetDatabase.LoadAssetAtPath(path, typeof(NavMapData)) as NavMapData;

            // EditorUtility.CopySerialized(_navMapData, _tempNavMapData);
            if (_navMapData == null)
            {
                _navMapData = ScriptableObject.CreateInstance<NavMapData>();
            }
            EditorUtility.CopySerialized(_tempNavMapData, _navMapData);
            Debug.Log("reloaded map data");

#else
            _tempNavMapData = (NavMapData)UnityEngine.Resources.Load(path);
              Debug.Log("reloaded map  in game  ");
#endif
            return _tempNavMapData != null;

        }

        public void ReloadData()
        {
            if (_navMapData != null)
            {
                EditorUtility.CopySerialized(_navMapData, _tempNavMapData);
            }
        }
        /// <summary>
        /// used by the editor script: navMapEditor
        /// </summary>
        /// <param name="levelName"></param>
        public void Save(string levelName)
        {
#if UNITY_EDITOR
            string path = ExitTheVoidFilePathRefs.CleanedNavigationPath(levelName);
            NavMapData navmap = AssetDatabase.LoadAssetAtPath(path, typeof(NavMapData)) as NavMapData;
            if (navmap != null)
            {
                // UnityEngine.Resources.UnloadAsset(navmap);
                EditorUtility.CopySerialized(_tempNavMapData, navmap);
                EditorUtility.CopySerialized(_tempNavMapData, _navMapData);
                //EditorUtility.CopySerialized(_navMapData,navmap);
                AssetDatabase.SaveAssets();
            }

            else
            {
                AssetDatabase.CreateAsset(_tempNavMapData, path);
                EditorUtility.CopySerialized(_tempNavMapData, _navMapData);
                AssetDatabase.SaveAssets();
            }
#endif    
        }

        public void CreateNew()
        {

        }



        public void Init(float nodeSize, float scaleFactor)
        {
            if (_tempNavMapData == null)
            {
                //first try to load it 

                //if it fails, then await instructions to load
            }


        }

        public void CreateNaviationMapData(float nodeSize, float scaleFactor)
        {
            _tempNavMapData = ScriptableObject.CreateInstance<NavMapData>();
            _tempNavMapData.NodeSize = nodeSize;
            _tempNavMapData.RescaleGridSize(scaleFactor);
            _navMapData = ScriptableObject.CreateInstance<NavMapData>();
            EditorUtility.CopySerialized(_tempNavMapData, _navMapData);

        }

         

        
        public void Scan()
        {
            Scanned = false;
            Count = 0;

            float segmentSize = _tempNavMapData.NodeSize;
            float sizeX = _tempNavMapData.SizeX / segmentSize;
            float sizeY = _tempNavMapData.SizeY / segmentSize;
            //create grid
            _tempNavMapData.Grid = new NavigationPoint[(int)sizeX, (int)sizeY];


            Vector2 startPos = _tempNavMapData.Points[0];

            for (int i = 0; i < (int)sizeX; i++)
            {
                for (int j = 0; j < (int)sizeY; j++)
                {
                    Vector2 pos = startPos;
                    pos.x += (segmentSize / 2) + (i * segmentSize);
                    pos.y -= (segmentSize / 2) + (j * segmentSize);
                    NavigationPoint navPoint = new NavigationPoint(pos, i, j);

                    _tempNavMapData[i, j] = navPoint;
                    navPoint.NodeType = NavigationType.None;

                    Count++;
                }
            }


            BuildNavTileMap();
            BuildNavigationRunLinks();
            BuildNavigationFallLinks();
        }

        public void BuildNavTileMap()
        {
            bool platformStarted = false;
            bool first = false;
            int debugCounter = 0;
            bool startDebugCounter = false;
            int startCounter = 0;



            int actualPlatformIndex = 0;

            string layer = LayerMask.LayerToName(GroundableMask);

            int xLength = Length()[0];
            int yLength = Length()[1];
            List<List<NavigationPoint>> platformList = new List<List<NavigationPoint>>(3);
            List<NavigationPoint> platformPoints = null;
            for (int y = 0; y < yLength - 1; y++)
            {

                platformStarted = false;
                for (int x = 0; x < xLength - 1; x++)
                {


                    NavigationPoint targetTile = _tempNavMapData[x, y];
                    NavigationPoint lowerTile = _tempNavMapData[x, y + 1];
                    NavigationPoint lowerRightTile = _tempNavMapData[x + 1, y + 1];
                    NavigationPoint rightTile = _tempNavMapData[y + 1, y];
                    Collider2D currentTileCol = Physics2D.OverlapCircle(targetTile.TilePosition, 0.9f * _tempNavMapData.NodeSize / 2f, 1 << LayerMask.NameToLayer(layer));
                    Collider2D lowerRightCol = Physics2D.OverlapCircle(lowerRightTile.TilePosition, 0.9f * _tempNavMapData.NodeSize / 2f, 1 << LayerMask.NameToLayer(layer));
                    Collider2D rightCol = Physics2D.OverlapCircle(rightTile.TilePosition, 0.9f * _tempNavMapData.NodeSize / 2f, 1 << LayerMask.NameToLayer(layer));
                    Collider2D lowerCol = Physics2D.OverlapCircle(lowerTile.TilePosition, 0.9f * _tempNavMapData.NodeSize / 2f, 1 << LayerMask.NameToLayer(layer));
                    if (!platformStarted)
                    {
                        if (currentTileCol == null && lowerCol != null)//lowerTile.NodeType == NavigationType.None)
                        {

                            // Collider2D col = Physics2D.OverlapCircle(lowerTile.TilePosition, _tempNavMapData.NodeSize / 2, 1 << LayerMask.NameToLayer(layer));
                            targetTile.NodeType = NavigationType.LeftEdge;
                            actualPlatformIndex++;
                            targetTile.PlatformIndex = actualPlatformIndex;
                            platformStarted = true;
                             platformPoints = new List<NavigationPoint>(3);
                            platformList.Add(platformPoints);
                            platformPoints.Add(targetTile);
                        }

                    }
                    if (platformStarted)
                    {

                        if (lowerRightCol != null && rightCol == null && targetTile.NodeType != NavigationType.LeftEdge)
                        {
                            targetTile.NodeType = NavigationType.Platform;
                            targetTile.PlatformIndex = actualPlatformIndex;
                            if (!platformPoints.Contains(targetTile))
                            {
                                platformPoints.Add(targetTile);
                            } 
                        }
                        // if (lowerRightTile.NodeType == NavigationType.None || rightCol != null)
                        if (lowerRightCol == null || rightCol != null)
                        {
                            if (targetTile.NodeType == NavigationType.LeftEdge)
                            {
                                targetTile.PlatformIndex = actualPlatformIndex;
                                targetTile.NodeType = NavigationType.Solo;
                                if (!platformPoints.Contains(targetTile))
                                {
                                    platformPoints.Add(targetTile);
                                } 
                            }
                            else
                            {
                                if (!platformPoints.Contains(targetTile))
                                {
                                    platformPoints.Add(targetTile); 
                                }
                                targetTile.PlatformIndex = actualPlatformIndex;
                                targetTile.NodeType = NavigationType.RightEdge;
                                

                            }
                            platformStarted = false;
                        }
                    }

                }
            }
            _tempNavMapData.PlatformList = platformList;
            Scanned = true;
        }

        private void  BuildNavigationRunLinks()
        {
            for (int i = 0; i < _tempNavMapData.PlatformList.Count; i++)
            { 
                for (int j = 0; j < _tempNavMapData.PlatformList[i].Count; j++)
                {
                    NavigationPoint targetPoint = _tempNavMapData.PlatformList[i][j];
                    if (targetPoint.NodeType == NavigationType.None)
                    {
                         continue;
                    }
                    else
                    {
                        if ( j == 0)
                        {
                            if (_tempNavMapData.PlatformList[i].Count != 1)
                            {
                                targetPoint.NavigationLink.RightNavigationPointLink =
                                    _tempNavMapData.PlatformList[i][j + 1];
                                targetPoint.NavigationLink.LeftNavigationPointLink =
                                    null; 
                            }
                        }
                        else if (j == _tempNavMapData.PlatformList[i].Count - 1 &&
                                 _tempNavMapData.PlatformList[i].Count > 1)
                        {
                            targetPoint.NavigationLink.LeftNavigationPointLink =
                                _tempNavMapData.PlatformList[i][j - 1];
                            targetPoint.NavigationLink.RightNavigationPointLink =
                                null; 
                        }
                        else
                        {
                            targetPoint.NavigationLink.LeftNavigationPointLink =
                                _tempNavMapData.PlatformList[i][j - 1];
                            targetPoint.NavigationLink.RightNavigationPointLink =
                                  _tempNavMapData.PlatformList[i][j + 1]; ; 
                        }

                    }
                }
            }
        }

       
        private void BuildNavigationFallLinks()
        {
            int upperCount = _tempNavMapData.PlatformList.Count;
            for (int i = 0; i < upperCount; i++)
            {
                int lowerCount = _tempNavMapData.PlatformList[i].Count;
                for (int j = 0; j < lowerCount; j++)
                {
                    int a=0;
                    int b=0;
                    NavigationPoint targetPoint = _tempNavMapData.PlatformList[i][j];
                    if (targetPoint.NodeType == NavigationType.None || targetPoint.NodeType == NavigationType.Platform)
                    {
                        continue;
                    }
                    switch (targetPoint.NodeType)
                    {
                        case NavigationType.RightEdge:
                            a = 1;
                            b = 1;
                            break;
                        case NavigationType.LeftEdge:
                            a = 0;
                            b = 0;
                            break;
                        case NavigationType.Solo:
                            a = 0;
                            b = 1;
                            break; 
                    }
                    NavigationPoint sideTile = null;
                    for (int k = a; k <= b; k ++)
                    {
                           
                        int indexX = targetPoint.PosX;
                        int indexY = targetPoint.PosY;

                        if (k == 0)
                        {
                            //check if there is a possible left point, break if there isnt
                            if (indexX == 0)
                            {
                              
                                continue;
                            }
                            else
                            {
                                sideTile = _tempNavMapData.Grid[indexX - 1,indexY]; //left tile
                           
                            }
                        }
                        else
                        {
                            if (indexX == (int)_tempNavMapData.SizeX-1) //check if we're at the right most edge of the map
                            { 
                                continue;
                            }
                            else
                            {
                                sideTile = _tempNavMapData.Grid[indexX + 1,indexY]; //right tile 
                            }
                        }
                        //check for collisions first, this has already been checked by previous scan. 
                        if (sideTile == null ||sideTile.NodeType != NavigationType.None  )
                        {
                            continue;
                        }
                        else
                        {
                            int targetRow = sideTile.PosY + 1;
                            while (targetRow < Length()[1])
                            {
                                NavigationPoint navPointToCheck  = _tempNavMapData.Grid[sideTile.PosX, targetRow];
                                 
                             
                               
                                if (navPointToCheck.NodeType != NavigationType.None)
                                {
                                    targetPoint.NavigationLink.FallNavigationPointLink = navPointToCheck; 
                                    break;
                                }
                                targetRow++;
                            }
                        }
                    }

                }
            }
        }

        private void BuildNavigationJumpLinks()
        {
            
        }

        private void CleanUpJumpLinks()
        {
            
        }

        public NavigationPoint this[int x, int y]
        {
            get { return _tempNavMapData[x, y]; }
            set { _tempNavMapData[x, y] = value; }
        }

        public void Reset()
        {
            Scanned = false;

        }

        public void SetNewSize(float sizeX, float sizeY)
        {
            SizeX = sizeX;
            SizeY = sizeY;
        }
        void OnDrawGizmos()
        { 
            if (TempNavMapData != null)
            {
                if (Count > 0)
                { 
                    if (Scanned)
                    {
                        int[] length = Length();
                        int x = length[0];

                        int y = length[1];

                        // for (int i = 0; i < (int)_navMap.SizeX; i++)
                        for (int i = 0; i < x; i++)
                        {
                            // for (int j = 0; j < (int)_navMap.SizeY; j++)
                            for (int j = 0; j < y; j++)
                            {

                                NavigationPoint point = this[i, j];
                                switch (point.NodeType)
                                {
                                    case NavigationType.None:
                                        Gizmos.color = TempNavMapData.NoneColor;
                                        break;
                                    case NavigationType.LeftEdge:
                                        Gizmos.color = TempNavMapData.LeftEdgeColor;
                                        break;
                                    case NavigationType.RightEdge:
                                        Gizmos.color = TempNavMapData.RightEdgeColor;
                                        break;
                                    case NavigationType.Solo:
                                        Gizmos.color = TempNavMapData.SoloEdgeColor;
                                        break;
                                    case NavigationType.Platform:
                                        Gizmos.color = TempNavMapData.PlatformColor;
                                        break;
                                }
                                // int controlid = point.Id;
                                if (point.Debugthis)
                                {
                                    Gizmos.color = Color.magenta;
                                }
                                Gizmos.DrawCube(point.TilePosition, Vector3.one * (NodeSize / 6));

                            }
                        }
                        DrawGrid(_tempNavMapData.Points);
                        DrawNavigationLinks();
                    }
                    //DrawNodes();
                }
            }
        }
        private void DrawGrid(Vector3[] points)
        {

            Color gridCol = Color.green;
            gridCol.a = 0.1f;
            Gizmos.color = gridCol;
            //calc vertical segments
            float vLength = _tempNavMapData.SizeY = Mathf.Abs(points[2].y - points[0].y);
            int vSegments = (int)(vLength / _tempNavMapData.NodeSize);

            //calculate horizontal segments
            float hLength = _tempNavMapData.SizeX = Mathf.Abs(points[1].x - points[0].x);
            int hSegments = (int)(hLength / _tempNavMapData.NodeSize);

            Vector2 vStart = points[0];
            Vector2 vEnd = points[2];
            Vector2 hStart = points[0];
            Vector2 hEnd = points[1];
            for (int i = 0; i <= hSegments; i++)
            {
                Vector2 startPos = vStart;
                Vector2 endPos = vEnd;
                startPos.x += i * _tempNavMapData.NodeSize;
                endPos.x += i * _tempNavMapData.NodeSize;
                Gizmos.DrawLine(startPos, endPos);
            }
            for (int i = 0; i <= vSegments; i++)
            {
                Vector2 startPos = hStart;
                Vector2 endPos = hEnd;
                startPos.y -= i * _tempNavMapData.NodeSize;
                endPos.y -= i * _tempNavMapData.NodeSize;
                Gizmos.DrawLine(startPos, endPos);
            }


        }

        private void DrawNavigationLinks()
        {
            Gizmos.color = Color.blue;
            //check if _tempnavmapdata isn't null
            if (_tempNavMapData != null)
            {
                if (_tempNavMapData.PlatformList != null)
                {
                    for (int i = 0; i < _tempNavMapData.PlatformList.Count; i++)
                    {
                        //draw run links
                        NavigationPoint firstPoint = _tempNavMapData.PlatformList[i][0];
                            NavigationPoint lastPoint = _tempNavMapData.PlatformList[i][_tempNavMapData.PlatformList[i].Count-1];
                            Gizmos.DrawLine(firstPoint.TilePosition,lastPoint.TilePosition);
                        Gizmos.DrawLine(firstPoint.TilePosition, lastPoint.TilePosition);
                        //draw jump links
                        NavigationPoint leftNavigationPoint = _tempNavMapData.PlatformList[i][0];
                        NavigationPoint rightNavigationPoint = _tempNavMapData.PlatformList[i][_tempNavMapData.PlatformList[i].Count - 1];
                        if (leftNavigationPoint.NavigationLink.FallNavigationPointLink != null)
                        {
                            NavigationPoint fallPoint = leftNavigationPoint.NavigationLink.FallNavigationPointLink;
                            Gizmos.DrawLine(leftNavigationPoint.TilePosition, fallPoint.TilePosition); 
                        }
                        if (rightNavigationPoint.NavigationLink.FallNavigationPointLink != null)
                        {
                            NavigationPoint fallPoint = rightNavigationPoint.NavigationLink.FallNavigationPointLink;
                            Gizmos.DrawLine(rightNavigationPoint.TilePosition, fallPoint.TilePosition);
                        }

                        
                    }



                }
            }
                
    }
    }

}
