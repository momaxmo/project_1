﻿ 
using UnityEngine;
using System.Collections.Generic;
namespace Assets.Scripts.pathfinding.navigation
{
     
    public class NavMapData: ScriptableObject
    {
        [SerializeField]
        private Vector3[] _points;
        [SerializeField]
        private float _nodeSize;
        [SerializeField]
        private float _gridScale;

        [SerializeField] private LayerMask _platformLayerMask;
        [HideInInspector]
        public bool ColorFoldout;
        [HideInInspector]
        public Color NoneColor;
        [HideInInspector]
        public Color PlatformColor;
        [HideInInspector]
        public Color LeftEdgeColor;
        [HideInInspector]
        public Color RightEdgeColor;
        [HideInInspector]
        public Color SoloEdgeColor;
        [HideInInspector]
        public Color TrajectoryColor;
        [HideInInspector]
        private NavigationPoint[,] _grid;
        [SerializeField]
        private float _sizeX;
        [SerializeField]
        private float _sizeY;
        [SerializeField]
        public int NodeCount;

        [SerializeField] private List<List<NavigationPoint>> _platformList;

        public Vector3[] Points
        {
            get
            {
                if (_points == null)
                {
                    _points = new Vector3[4];
                    _points[0] = Vector3.zero;
                    _points[1] = Vector3.right;
                    _points[2] = Vector3.zero;
                    _points[2].y = -1;
                    _points[3] = Vector3.zero;
                    _points[3].x =  1;
                    _points[3].y = -1;
                }
                return _points;
            }
            set { _points = value; }
        }

        public float NodeSize
        {
            get { return _nodeSize; }
            set { _nodeSize = value; }
        }

        public float SizeX
        {
            get { return _sizeX; }
            set { _sizeX = value; }
        }

        public float SizeY
        {
            get { return _sizeY; }
            set { _sizeY = value; }
        }

        public NavigationPoint[,] Grid
        {
            get
            {
                if (_grid == null)
                {
                    _grid = new NavigationPoint[(int)SizeX,(int)SizeY];
                }
                return _grid;
            }
            set { _grid = value; }
        }

        public LayerMask PlatformLayerMask
        {
            get { return _platformLayerMask; }
            set { _platformLayerMask = value; }
        }

        public List<List<NavigationPoint>> PlatformList
        {
            get
            {
                if (_platformList == null)
                {
                    _platformList = new List<List<NavigationPoint>>(3);
                }
                return _platformList;
            }
            set { _platformList = value; }
        }

        public void RescaleGridSize(float scale)
        {
            Points[0] *= scale;
            Points[1] *= scale;
            Points[2] *= scale;
            Points[3] *= scale;

        }

        void OnEnable()
        {
        }
        public NavigationPoint this[int i, int j]
        {
            get {
                return Grid[i, j];

            }
            set
            {
                Grid[i, j] = value;

            }
        }
 
    }
}
