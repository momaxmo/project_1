﻿
using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Assets.Scripts.pathfinding.navigation
{
    [Serializable]
    public class NavigationPoint : IEquatable<NavigationPoint>
    {
        public bool Debugthis;
        [SerializeField]
        private int _id;
        public NavigationPoint(Vector2 pos, int x, int y)
        {

            TilePosition = pos;
            PosX = x;
            PosY = y;
            GenerateId(x, y);
        }
        [SerializeField] 
        private int _platformIndex;
        [SerializeField]

        private int _posX;

        [SerializeField]
        private int _posY;
        [SerializeField]
        private Vector2 _tilePosition;
        private NavigationType _nodeType;

        public int Id
        {
            get { return _id; }
        }

        public int PlatformIndex
        {
            get { return _platformIndex; }
            set { _platformIndex = value; }
        }

        public int PosX
        {
            get { return _posX; }
            set { _posX = value; }
        }

        public int PosY
        {
            get { return _posY; }
            set { _posY = value; }
        }

        public Vector2 TilePosition
        {
            get { return _tilePosition; }
            set { _tilePosition = value; }
        }

        public NavigationType NodeType
        {
            get { return _nodeType; }
            set { _nodeType = value; }
        }

        public NavLink NavigationLink
        {
            get
            {
                if (_navigationLink == null)
                {
                    _navigationLink = new NavLink();
                }
                return _navigationLink;
            }
            set { _navigationLink = value; }
        }

        [SerializeField] private NavLink _navigationLink;

       
        private void GenerateId(int x, int y)
        {
            if (x == 0 && y == 0)
            {
                _id = 15485863; //some large prime
            }
            else
            {
                _id =  (x*7 + 13*y);
            }
        }

        public bool Equals(NavigationPoint other)
        {
            return other.Id == Id;
        }

        public override int GetHashCode()
        {
            return _id;
        }
         
    }

  

    public enum NavigationType
    {
        None, Platform, LeftEdge, RightEdge, Solo
    }

}
