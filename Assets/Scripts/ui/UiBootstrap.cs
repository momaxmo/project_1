﻿
using Assets.Scripts.ui.config;
using strange.extensions.context.impl;

namespace Assets.Scripts.ui
{
    public class UiBootstrap: ContextView
    {
        /// <summary>
        /// initialize ui context
        /// </summary>
        private void Awake()
        {
     
            context = new  UiContext(this);
        }
    }
}
