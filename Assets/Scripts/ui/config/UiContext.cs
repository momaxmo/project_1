﻿using Assets.Scripts.common;
using Assets.Scripts.common.config;
using Assets.Scripts.common.model;
using Assets.Scripts.filemanagement.controller.load;
using Assets.Scripts.game.controller; 
using Assets.Scripts.tools; 
using Assets.Scripts.ui.controller;
using Assets.Scripts.ui.model;
using Assets.Scripts.ui.view;
using Assets.Scripts.ui.view.ingame;
using Assets.Scripts.ui.view.pausemenu;
 using UnityEngine;

namespace Assets.Scripts.ui.config
{
    public class UiContext : SignalContext
    {
        GameSoundSetting<float> _soundVolume = new GameSoundSetting<float>();

        public UiContext(MonoBehaviour contextView)
            : base(contextView)
        {
        }
 

        protected override void mapBindings()
        {
            //We need to call mapBindings up the inheritance chain (see SignalContext)
            base.mapBindings();
            Debug.Log("in uicontext  mapping binds");
            //Whenever StartSignal is fired, UiStartCommand is executed
            commandBinder.Bind<StartSignal>().To<UiStartCommand>();
            commandBinder.Bind<LoadNextLevelSignal>().To<LoadLevelCommand>();
            //Mediation  allows the MenuView code to be separated from the rest of the game.
            mediationBinder.Bind<CrossPlatformHomeView>().To<CrossPlatformHomeViewMediator>();

            mediationBinder.Bind<MusicPlayer>().To<MusicPlayerMediator>();
           // mediationBinder.Bind<SettingsView>().To<SettingsMediator>();
            mediationBinder.Bind<MusicVolumeControlView>().To<MusicVolumeControlMediator>();
            //this check is to verify whether we are in stand alone mode, otherwise it is being called somewhere else in the game
            
           
            if (firstContext == this)
            {
                 
               // injectionBinder.Bind<LevelStartSignal>().ToSingleton().CrossContext();
              //  injectionBinder.Bind<LevelEndSignal>().ToSingleton().CrossContext();
        
           
               // injectionBinder.Bind<GameExitSignal>().ToSingleton().CrossContext();
                 
             //   injectionBinder.Bind<GameStartSignal>().ToSingleton();
                
                //inputs
               
                
                
            }
             
           /* LevelStateModel gameModel = new LevelStateModel() { IsPaused = false };
            injectionBinder.Bind<LevelStateModel>().ToValue(gameModel).ToSingleton().CrossContext();*/

            //pausing
            injectionBinder.Bind<GamePausedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<GamePauseCommandSignal>().ToSingleton().CrossContext();
            commandBinder.Bind<GamePauseCommandSignal>().To<PauseCommand>();
            //health meter
            injectionBinder.Bind<UpdateHealthSignal>().ToSingleton().CrossContext();
            //game menu
            mediationBinder.Bind<GameMenuView>().To<GameMenuViewMediator>();
            mediationBinder.Bind<InGameHud>().To<InGameHudMediator>();
            mediationBinder.Bind<PauseMenu>().To<PauseMenuMediator>();
            mediationBinder.Bind<BoosterInventoryView>().To<BoosterInventoryViewMediator>();
            mediationBinder.Bind<CharacterSheetView>().To<CharacterSheetViewMediator>();
            injectionBinder.Bind<PauseMenuSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UnpausableStateSignal>().ToSingleton().CrossContext();
           commandBinder.Bind<PauseMenuSignal>().To<PauseMenuController>();
           
            PauseMenuModel pausemenuModel = new PauseMenuModel();
            
            
            injectionBinder.Bind<PauseMenuModel>().ToValue(pausemenuModel).ToSingleton();
            injectionBinder.Bind<SwitchToPauseMenuInterface>().ToSingleton();
            injectionBinder.Bind<SwitchToCharacterSheetSignal>().ToSingleton();
            injectionBinder.Bind<SwitchToBoosterViewSignal>().ToSingleton();
            injectionBinder.Bind<ShowPauseButtonsSignal>().ToSingleton();
            injectionBinder.Bind<ShowExitGameDialogueSignal>().ToSingleton();
            injectionBinder.Bind<ShowExitToMainMenuDialogueSignal>().ToSingleton();
            injectionBinder.Bind<SwitchToSettingsAndOptionsSignal>().ToSingleton(); 

            //Music volume update
            injectionBinder.Bind<MusicVolumeUpdatedSignal<float>>().ToSingleton().CrossContext();
            injectionBinder.Bind<ISettings<float>>().ToValue(_soundVolume).ToName(GameSettings.MusicVolume).ToSingleton().CrossContext();
            //for debug purposes    
 

        }
 
        /// <summary>
        /// After bindings are completed, this function is called by Strange for any post processing procedures
        /// </summary>
        protected override void postBindings()
        {
            base.postBindings();
           
            if (firstContext != this)
            {
                /*//Disable the AudioListener
                AudioListener listener = (contextView as GameObject).GetComponentInChildren<AudioListener>();
                listener.enabled = false;

                //Disable the light
                Light[] lights = (contextView as GameObject).GetComponentsInChildren<Light>();
                foreach (Light light in lights)
                {
                    light.enabled = false;
                }*/
            }
        }
    }
}
