﻿ 
 
using Assets.Scripts.common.config;
using Assets.Scripts.game.model;
using Assets.Scripts.ui.model;
using strange.extensions.command.impl;
using UnityEngine;


namespace Assets.Scripts.ui.controller
{
    /// <summary>
    /// controls the states of the pause menu by verifying the current pause menu model
    /// </summary>
    public class PauseMenuController : Command
    {
        
        /// <summary>
        /// Get the pause menu signal's requested change state
        /// </summary>
        [Inject]
        public  PauseMenuState PausemenuSignaledState { get; set; }
        [Inject]
        public LevelStateModel LevelState { get; set; }
        [Inject]
        public GamePausedSignal GamepausedSignal { get; set; }
 
        [Inject]
        public SwitchToPauseMenuInterface SwitchToPauseMenu { get; set; }

        [Inject]
        public SwitchToCharacterSheetSignal CharacterSheetSignal { get; set; }

         [Inject]
        public SwitchToBoosterViewSignal BoosterViewSignal { get; set; }

        [Inject]
         public ShowPauseButtonsSignal PauseButtonView { get; set; }
    

        [Inject]
        public ShowExitGameDialogueSignal ExitGameSignal { get; set; }

        [Inject]
        public ShowExitToMainMenuDialogueSignal ExitToMainMenuSignal { get; set; }
        [Inject]
        public SwitchToSettingsAndOptionsSignal SettingsAndOptionsSignal { get; set; }
        [Inject]
        public PauseMenuModel PausemenuModel { get; set; }

 

        /// <summary>
        /// Models a state machine and dispatches a signal that shuts off or turns on views
        /// </summary>
        public override void Execute()
        {

            bool isValidState = PausemenuModel.IsValidState(PausemenuSignaledState);
            
            #region check if state validity
            if (isValidState)
            {
                //Check what the previous state was and dispatch signals accordingly
               
                switch (PausemenuModel.CurrentState)
                {
                    #region previous state : main pause menu
                    case PauseMenuState.MainPauseMenu:
                        if (PausemenuSignaledState == PauseMenuState.Off)
                        {
                            Debug.Log("turning off the pause menu");
                            PauseButtonView.Dispatch(false);
                            SwitchToPauseMenu.Dispatch(false);
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.BoosterChange)
                        {
                            Debug.Log("changing to booster");
                            PauseButtonView.Dispatch(false);
                            BoosterViewSignal.Dispatch(true);
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.CharacterSheet)
                        {
                            Debug.Log("changing to CharacterSheet");
                            PauseButtonView.Dispatch(false);
                            CharacterSheetSignal.Dispatch(true);
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.ExitGame)
                        {
                            Debug.Log("changing to ExitGame");
                            ExitGameSignal.Dispatch(true);
                            LevelState.IgnorePauseSignals = true;
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.ExitToMainMenu)
                        {
                            Debug.Log("changing to ExitToMainMenu");
                            ExitToMainMenuSignal.Dispatch(true);
                            LevelState.IgnorePauseSignals = true;
                            break;
                        }
                         
                        if (PausemenuSignaledState == PauseMenuState.SettingsAndOptions)
                        {
                            Debug.Log("changing to SettingsAndOptions");

                            SettingsAndOptionsSignal.Dispatch(true);
                         }
                        else
                        {
                            Debug.Log("no valid state found?");
                            
                        }

                        break;
                    #endregion
                    #region previous state : Off
                    case PauseMenuState.Off:
                        if (PausemenuSignaledState == PauseMenuState.Off)
                        {
                            SwitchToPauseMenu.Dispatch(false);
                            PauseButtonView.Dispatch(false);
                            BoosterViewSignal.Dispatch(false);
                            CharacterSheetSignal.Dispatch(false);
                            SettingsAndOptionsSignal.Dispatch(false);
                            ExitToMainMenuSignal.Dispatch(false);
                            ExitToMainMenuSignal.Dispatch(false);
                            ExitGameSignal.Dispatch(false);
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.MainPauseMenu)
                        {
                            SwitchToPauseMenu.Dispatch(true);
                            PauseButtonView.Dispatch(true); 
                            BoosterViewSignal.Dispatch(false);
                            CharacterSheetSignal.Dispatch(false);
                            SettingsAndOptionsSignal.Dispatch(false);
                            ExitToMainMenuSignal.Dispatch(false);
                            ExitToMainMenuSignal.Dispatch(false);
                            ExitGameSignal.Dispatch(false);
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.BoosterChange)
                        {
                            SwitchToPauseMenu.Dispatch(true);
                            BoosterViewSignal.Dispatch(true);
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.CharacterSheet)
                        {
                            SwitchToPauseMenu.Dispatch(true); 
                            CharacterSheetSignal.Dispatch(true);
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.ExitGame)
                        {
                            SwitchToPauseMenu.Dispatch(true);
                            ExitGameSignal.Dispatch(true);
                            LevelState.IgnorePauseSignals = true;
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.ExitToMainMenu)
                        {
                            SwitchToPauseMenu.Dispatch(true);
                            ExitToMainMenuSignal.Dispatch(true);
                            LevelState.IgnorePauseSignals = true;
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.ExitToMainMenu)
                        {
                            SwitchToPauseMenu.Dispatch(true);
                            ExitToMainMenuSignal.Dispatch(true);
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.SettingsAndOptions)
                        {
                            SwitchToPauseMenu.Dispatch(true);
                            SettingsAndOptionsSignal.Dispatch(true);
                            
                        }
                        break;

                    #endregion
                    #region previous state : BoosterChange
                    case PauseMenuState.BoosterChange:
                        if (PausemenuSignaledState == PauseMenuState.MainPauseMenu)
                        {
                            BoosterViewSignal.Dispatch(false); 
                            PauseButtonView.Dispatch(true); 
                            break;
                        } 
                        if (PausemenuSignaledState == PauseMenuState.CharacterSheet)
                        {
                            BoosterViewSignal.Dispatch(false);
                            CharacterSheetSignal.Dispatch(true);
                            break;
                        }  
                       
                        if (PausemenuSignaledState == PauseMenuState.Off)
                        {
                            BoosterViewSignal.Dispatch(false);
                            SwitchToPauseMenu.Dispatch(false); 
                         }
                        break;

                    #endregion
                    #region previous state : CharacterSheet
                    case PauseMenuState.CharacterSheet:
                        if (PausemenuSignaledState == PauseMenuState.Off)
                        {
                            CharacterSheetSignal.Dispatch(false);
                            SwitchToPauseMenu.Dispatch(false);
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.MainPauseMenu)
                        {
                            CharacterSheetSignal.Dispatch(false);
                            PauseButtonView.Dispatch(true);
                            break;
                        }
                        if (PausemenuSignaledState == PauseMenuState.BoosterChange)
                        {
                            CharacterSheetSignal.Dispatch(false);
                            BoosterViewSignal.Dispatch(true);
                            
                        } 
                        break;

                    #endregion
                    #region previous state : ExitGame
                    case PauseMenuState.ExitGame: 
                        if (PausemenuSignaledState == PauseMenuState.MainPauseMenu)
                        {
                            ExitGameSignal.Dispatch(false);
                            PauseButtonView.Dispatch(true);
                            LevelState.IgnorePauseSignals = false; //Since we're exiting the exit view state, we should now be able to once dispatch pause signals
                        }
                        break;

                    #endregion
                    #region previous state : ExitToMainMenu
                    case PauseMenuState.ExitToMainMenu: 
                        if (PausemenuSignaledState == PauseMenuState.MainPauseMenu)
                        {
                            ExitToMainMenuSignal.Dispatch(false);
                            PauseButtonView.Dispatch(true);
                            LevelState.IgnorePauseSignals = false; //Since we're exiting the exit to pause menu view state, we should now be able to once dispatch pause signals
                        }
                        break;

                    #endregion
                    #region previous state : SettingsAndOptions
                    case PauseMenuState.SettingsAndOptions:

                        if (PausemenuSignaledState == PauseMenuState.MainPauseMenu)
                        {
                            SettingsAndOptionsSignal.Dispatch(false);
                            PauseButtonView.Dispatch(true);
                            break;
                        }
                       
                         if (PausemenuSignaledState == PauseMenuState.Off)
                        {
                            SettingsAndOptionsSignal.Dispatch(false);
                            SwitchToPauseMenu.Dispatch(false);
                             
                        }
                        break;


                    #endregion
                }
                PausemenuModel.SwitchStates(PausemenuSignaledState);


            }
            #endregion
            Debug.Log("In execute (pos)  current state  is " + PausemenuModel.CurrentState + " previous state is now set to "+ PausemenuModel.PreviousState);

        }
    }
}
