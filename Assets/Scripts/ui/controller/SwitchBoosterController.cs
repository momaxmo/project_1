﻿
using Assets.Scripts.common.config;
using Assets.Scripts.game.boosters;
using Assets.Scripts.game.inventory.Model;
using Assets.Scripts.game.inventory.view;
using Assets.Scripts.game.player.model;
using Assets.Scripts.game.view.player.interactions;
using strange.extensions.command.impl;
using UnityEngine;

namespace Assets.Scripts.ui.controller
{
    public class SwitchBoosterController : Command
    {
        [Inject]
        public IPlayerModel PlayerModel { get; set; }
        
        [Inject]
        public EnableGridInteractivity EnableGridSignal { get; set; }
        [Inject]
        public int BoosterSlot { get; set; }
        [Inject]
        public IterableButton InitiatingButton { get; set; }
        [Inject]
        public IInventoryItem NewInventoryItem { get; set; }

 

        [Inject]
         public UpdateBooster UpdateInventorySignal { get; set; }
              
        public override void Execute()
        {
            IInventoryItem oldInventoryItem = NewInventoryItem;
            switch (BoosterSlot)
            {
                case 1:
                    oldInventoryItem = PlayerModel.Booster1.InventoryItem;
                    PlayerModel.Booster1.InventoryItem = NewInventoryItem;
                    break;

                case 2:
                    oldInventoryItem = PlayerModel.Booster2.InventoryItem;
                    PlayerModel.Booster2.InventoryItem = NewInventoryItem;
                    break;
            }
            //update the initiating button's value
            if (oldInventoryItem == NewInventoryItem)
            {
                Debug.Log("the new booster model passed into "+GetType()+" is the same as the old one. Shouldn't happen. ");
            }
            PlayerModel.Inventory.Remove(oldInventoryItem);
            PlayerModel.Inventory.Add(NewInventoryItem);
            InitiatingButton.UpdateInventoryContainer(oldInventoryItem);
            UpdateInventorySignal.Dispatch(NewInventoryItem,BoosterSlot);
            EnableGridSignal.Dispatch();
        }
    }
}
