﻿
using Assets.Scripts.game.model;
using strange.extensions.command.impl;

namespace Assets.Scripts.ui.controller
{
   public class UiStartCommand: Command
    {
       [Inject]
       public LevelStateModel LevelStateModel { get; set; }
       public override void Execute()
       {
           
           UnityEngine.Debug.Log("Run extra routines here after loading a scene");
           //TODO: check what scene we're in and disable main menu options and load scene specific UI options
           //TODO: example: in game we have a specific set of views enabled
          
           
       }
    }
}
