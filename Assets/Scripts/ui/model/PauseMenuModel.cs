﻿

using System.Collections.Generic;
using Assets.Scripts.ui.view.pausemenu; 

namespace Assets.Scripts.ui.model
{
    /// <summary>
    /// The PauseMenuModel holds the current state of the Pause menu view
    /// 
    /// </summary>
    public class PauseMenuModel
    {
        private PauseMenuState _currentState = PauseMenuState.Off;
        private PauseMenuState _previousState = PauseMenuState.Off;
        private Dictionary<string, IPausedSubmenuView> _pauseViews = new Dictionary<string, IPausedSubmenuView>();
        public PauseMenuState CurrentState
        {
            get { return _currentState; }
            set { _currentState = value; }
        }

        public Dictionary<string, IPausedSubmenuView> PauseViews
        {
            get { return _pauseViews; }
            set { _pauseViews = value; }
        }

        public PauseMenuState PreviousState
        {
            get { return _previousState; }
            set { _previousState = value; }
        }

        public void SwitchStates(PauseMenuState newState)
        {
            _previousState = CurrentState;
            CurrentState = newState;
        }
        /// <summary>
        /// These are valid transition states between views
        /// </summary>
        /// <param name="newState"></param>
        /// <returns></returns>
        public bool IsValidState(PauseMenuState newState)
        {
            switch (CurrentState)
            {
                case (PauseMenuState.Off):
                    {
                        return true;

                    }
                case PauseMenuState.MainPauseMenu:
                    {
                        return true;

                    }
                case PauseMenuState.CharacterSheet:
                    {
                        if (newState == PauseMenuState.MainPauseMenu || newState == PauseMenuState.Off || newState == PauseMenuState.BoosterChange)
                            return true;

                        return false;

                    }
                case PauseMenuState.ExitGame:
                    {
                        if (newState == PauseMenuState.MainPauseMenu)
                            return true;
                       
                            return false;
                         
                    }
                case PauseMenuState.ExitToMainMenu:
                    {
                        if (newState == PauseMenuState.Off)
                            return true;
                       
                        {
                            return false;
                        }
                         
                    }
                case PauseMenuState.BoosterChange:
                    {
                        if (newState == PauseMenuState.CharacterSheet || newState == PauseMenuState.MainPauseMenu ||
                            newState == PauseMenuState.Off)
                            return true;
    return false;
                    
                    }
                case PauseMenuState.SettingsAndOptions:
                    {
                        if (newState == PauseMenuState.MainPauseMenu || newState == PauseMenuState.Off)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }

            }
            return false;
        }
    }

    public enum PauseMenuState
    {
        Off,
        MainPauseMenu,
        CharacterSheet,
        BoosterChange,
        ExitToMainMenu,
        SettingsAndOptions,
        ExitGame
    }
}
