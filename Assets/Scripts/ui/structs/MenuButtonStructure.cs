﻿ 
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.ui.structs
{
  public   class MenuButtonStructure: MonoBehaviour
  {
      public Image ButtonImage { get;   set; }
      public Text ButtonText  { get; private set; }

      public Button MenuButton { get; private set; }

      void Awake()
      {
          ButtonImage = GetComponent<Image>();
          ButtonText = GetComponentInChildren<Text>();
          MenuButton = GetComponent<Button>();

      }
  }
}
