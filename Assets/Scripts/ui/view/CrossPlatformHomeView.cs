﻿ 
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;


namespace Assets.Scripts.ui.view
{
    /// <summary>
    /// Based on the platform that the game is running on, some option might appear 
    /// </summary>
    public class CrossPlatformHomeView : View
    {
        #region signals
        internal Signal ProceedSignal = new Signal();
        internal Signal SettingsButtonSignal = new Signal();
        internal Signal CreditsButtonSignal = new Signal();

#if  !(UNITY_ANDROID || UNITY_IPHONE)
        internal Signal ExitButtonSignal = new Signal();
#endif


        #endregion

        
        /* public ButtonView StartButton;
        public ButtonView SettingsButton;
        public ButtonView CreditsButton;
        public ButtonView ExitButton;  //if on a mobile platform, this button should not exist.*/

        #region homeview buttons

        public UnityEngine.UI.Button StartButton;
        public UnityEngine.UI.Button SettingsButton;
        public UnityEngine.UI.Button CreditsButton;
        public UnityEngine.UI.Button ExitButton;  //if on a mobile platform, this button should not exist.;

        #endregion

        internal void Init()
        {
            StartButton.onClick.RemoveAllListeners();
            StartButton.onClick.AddListener(OnStartPress);

            SettingsButton.onClick.RemoveAllListeners();
            SettingsButton.onClick.AddListener(OnSettingsPress);

            CreditsButton.onClick.RemoveAllListeners();
            CreditsButton.onClick.AddListener(CreditsButtonPress);
/*
            StartButton.PressSignal.AddListener(OnStartPress);
            SettingsButton.PressSignal.AddListener(OnSettingsPress);
            CreditsButton.PressSignal.AddListener(CreditsButtonPress);*/

#if  (UNITY_ANDROID || UNITY_IPHONE)
             ExitButton.gameObject.SetActive(false);
#else
           // ExitButton.PressSignal.AddListener(OnExitPress);
            ExitButton.onClick.RemoveAllListeners();
            ExitButton.onClick.AddListener(OnExitPress);
#endif

        }

        /// <summary>
        /// Dispatch a proceed signal
        /// </summary>
        private void OnStartPress()
        {
            ProceedSignal.Dispatch();
            this.gameObject.SetActive(false);
            Debug.Log("start press");
        }

        private void OnExitPress()
        {
            ExitButtonSignal.Dispatch();
        }

        private void OnSettingsPress()
        {
            SettingsButtonSignal.Dispatch();
        }

        private void CreditsButtonPress()
        {
            CreditsButtonSignal.Dispatch();
        }


    }
}
