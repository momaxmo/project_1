﻿ 
using Assets.Scripts.common.config;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.ui.view
{
    public class CrossPlatformHomeViewMediator: Mediator
    {
        [Inject]
        public CrossPlatformHomeView MenuView { get; set; }

        [Inject]
        public LoadNextLevelSignal LoadLevel1Signal { get; set; }

        [Inject] 
        public LevelStartSignal LevelStartSignal { get; set; }

        [Inject]
        public GameExitSignal GameExitSignal { get; set; }

        public override void OnRegister()
        {
            MenuView.Init();
            MenuView.ProceedSignal.AddListener(OnProceed);
            MenuView.ExitButtonSignal.AddListener(OnGameExit);
        }

        public override void OnRemove()
        {
            MenuView.ProceedSignal.RemoveListener(OnProceed);
            MenuView.ExitButtonSignal.RemoveListener(OnGameExit);
        }

        private void OnProceed()
        {
            Hide();
            LoadLevel1Signal.Dispatch();
        }

        private void OnGameExit()
        {
            Application.Quit();
        }

        private void Hide()
        {
            Debug.Log("hide in cross platform MenuView mediator");
        }
    }
}
