﻿ 
namespace Assets.Scripts.ui.view
{
    public interface IVolumeControlMediator
    {
    
        void UpdateVolume(float val);
    }
}
