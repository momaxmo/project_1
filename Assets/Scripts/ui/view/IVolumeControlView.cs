﻿ 
using UnityEngine.UI;

namespace Assets.Scripts.ui.view
{
    public interface IVolumeControlView
    {
        void UpdateVolume(float val);
        Slider VolumeSlider { get; set; }
    
    }
}
