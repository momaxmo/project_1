﻿
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.ui.view
{
    public class MusicPlayer: View
    {
        private AudioSource _audioSource;
 
        public AudioSource AudSource
        {
            get
            {
                if (_audioSource == null)
                {
                    _audioSource = gameObject.GetComponent<AudioSource>();
                    if (!_audioSource)
                    {
                        _audioSource = gameObject.AddComponent<AudioSource>();
                    }
                }
                return _audioSource;
               
            }
        }


        public void DeleteThis()
        {
             Debug.Log("Delete this method");
        }

        public void ChangeSong()
        {
        }

        public void UpdateMusicVolume(float newValue)
        {
            AudSource.volume = newValue;
        }
    }
}
