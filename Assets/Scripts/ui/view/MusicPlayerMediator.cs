﻿ 
using Assets.Scripts.common;
using Assets.Scripts.common.config;
using Assets.Scripts.tools;
using strange.extensions.mediation.impl;
 

namespace Assets.Scripts.ui.view
{
    public class MusicPlayerMediator: Mediator
    {
         [Inject ]
       public  MusicPlayer View { get; set; }
        [Inject(GameSettings.MusicVolume)]
        public ISettings<float> MusicVolume { get; set; }

        [Inject]
        public MusicVolumeUpdatedSignal<float> MusicVolumeChangeSignal { get; set; }
        public override void OnRemove()
        {
            MusicVolumeChangeSignal.RemoveListener(UpdateVolume);
        }

        public override void OnRegister()
        {
            MusicVolumeChangeSignal.AddListener(UpdateVolume);
        }

        public void UpdateVolume(float newValue)
        {
            View.UpdateMusicVolume(newValue);
           
        }
    }
}
