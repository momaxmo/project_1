﻿ 
using Assets.Scripts.common;
using Assets.Scripts.common.config;
using Assets.Scripts.tools;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.ui.view
{
    public class MusicVolumeControlMediator : Mediator,IVolumeControlMediator
    {
        [Inject]
        public MusicVolumeUpdatedSignal<float> MusicVolumeChangeSignal { get; set; }
        [Inject(GameSettings.MusicVolume)]
        public ISettings<float> MusicVolume { get; set; }
        [Inject]
        public MusicVolumeControlView MusicContView { get; set; }


        public override void OnRegister()
        {
            MusicContView.Init();
            MusicContView.MusicSliderSignal.AddListener(UpdateVolume);
        }

        public override void OnRemove()
        {
            MusicContView.MusicSliderSignal.RemoveListener(UpdateVolume);
        }

  
        public void UpdateVolume(float val)
        {
            MusicVolumeChangeSignal.Dispatch(val);
            MusicVolume.SetValue(val);
          
        }

          
    }
}
