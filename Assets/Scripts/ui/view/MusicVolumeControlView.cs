﻿ 
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
 
using UnityEngine.UI;

namespace Assets.Scripts.ui.view
{
    public class MusicVolumeControlView : View
    {
        internal Signal<float> MusicSliderSignal = new Signal<float>();
        private Slider _volumeSlider;
       
        

        public Slider VolumeSlider
        {
            get
            {
                if (_volumeSlider == null)
                {
                    _volumeSlider = GetComponent<Slider>();
                }
            
                return _volumeSlider;
            }
            set { _volumeSlider = value; }
        }
        internal void Init()
        {
             VolumeSlider.onValueChanged.RemoveAllListeners();
             VolumeSlider.onValueChanged.AddListener(UpdateVolume);
           
        }

    
        public void UpdateVolume(float val)
        {
          
            MusicSliderSignal.Dispatch(val);
        }

   
    }
}
