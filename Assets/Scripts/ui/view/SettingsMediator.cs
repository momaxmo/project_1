﻿ 
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Assets.Scripts.ui.view
{
    public class SettingsMediator: Mediator
    {
        public Signal<float> MusicVolumeChangeSignal = new Signal<float>();

        [Inject] public  SettingsView view { get; set; }

        public override void OnRegister()
        {
             view.Init();
             view.MusicSliderSignal.AddListener(UpdateMusicVolume);
        }

        public override void OnRemove()
        {
            view.MusicSliderSignal.RemoveListener(UpdateMusicVolume);
        }

        private void UpdateMusicVolume(float val)
        {

            MusicVolumeChangeSignal.Dispatch(val);
        }
    }
}
