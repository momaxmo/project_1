﻿
using System;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
 using UnityEngine.UI;

namespace Assets.Scripts.ui.view
{
    public class SettingsView: View
    {

        public  Slider MusicVolumeSlider;

        internal Signal<float> MusicSliderSignal = new Signal<float>();
        
        internal void Init()
        {
            if (MusicVolumeSlider == null)
            {
                Slider[] slider =  FindObjectsOfType<Slider>();
                
                foreach (Slider s in slider)
                {
                    if (s.name == "MusicSettingSlider")
                    { 
                        MusicVolumeSlider = s;
                        break;
                    }
                }
                if (MusicVolumeSlider == null) //check if the music settings slider was found in the scene
                {
                    throw new Exception("Could not find MusicSettingSlider in scene");
                }
                else
                {
                    MusicVolumeSlider.onValueChanged.RemoveAllListeners();
                    MusicVolumeSlider.onValueChanged.AddListener(UpdateMusicVolume);
                }
            }
        }


        private void UpdateMusicVolume(float val)
        {
            MusicSliderSignal.Dispatch(val);
        }
    }
}
