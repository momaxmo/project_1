﻿ 
using System.Collections;
 
using Assets.Scripts.tools;
 
using Assets.Scripts.ui.view.pausemenu;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.ui.view.ingame
{
    /// <summary>
    /// This view's responsibility is to ensure that the background is visible and on signaled, that the
    /// buttons associated with this view are enabled
    /// </summary>
    public class GameMenuView : View, IPausedSubmenuView
    {
         
        public  Image MenuBackgroundImage;
       // private Image _pauseImage;
        public float ShowSpeed { get; set; }
        public float TimeToDissapear;
      //  public Signal<PauseMenuState>  ViewPauseButtons= new Signal<PauseMenuState>();
       

 

       // public MenuButtonStructure[] MenuButtons;

        public bool IsActive { get; set; }
         
        public void Init( float pauseFadingSpeed)
        {
            MenuBackgroundImage = ExitTheVoidTools.FindObjectByName(transform, "UIBackground").GetComponent<Image>();
           // MenuBackgroundImage = transform.Find("UIBackground").GetComponent<Image>();
             
            ShowSpeed = pauseFadingSpeed; 
            //disable immediatly after as to not cause problems with the event system
         
        }

        

     

        /// <summary>
        /// Show the pause menu
        /// </summary>
        public void Show()
        {
            gameObject.SetActive(true);
           // PauseViewGroup.SetActive(true);
           // PauseButtonPanel.SetActive(true);
            StartCoroutine(ExitTheVoidTools.FadeImage(MenuBackgroundImage, ShowSpeed * .8f, 1f));
            //ViewPauseButtons.Dispatch(PauseMenuState.MainPauseMenu);
            //fade into the pause buttons
            
            //Dispatch a signal to message any interested listeners that the pause menu is finished with showing the background image
         //   ShowPauseButtonsSignal.Dispatch(true); 
        }
        /// <summary>
        /// Hide The pause menu
        /// </summary>
        public void Hide()
        {
            StartCoroutine(ExitTheVoidTools.FadeImage(MenuBackgroundImage, ShowSpeed * .8f, 0f));
            StartCoroutine(WaitThenDisableGameObject(ShowSpeed * 0.9f));
            Debug.Log("hiding game menu view");
            /*
              foreach (MenuButtonStructure mbs in MenuButtons)
              {
                  StartCoroutine(ExitTheVoidTools.FadeImage(mbs.ButtonImage, ShowSpeed * .8f, 0f));
                  StartCoroutine(ExitTheVoidTools.FadeText(mbs.ButtonText, ShowSpeed * .8f, 0f));
              }*/
            //  ShowPauseButtonsSignal.Dispatch(true); 
            //StartCoroutine(WaitThenDisable(ShowSpeed * 0.9f));

        }

        private IEnumerator WaitThenDisableGameObject(float time)
        {
            yield return new WaitForSeconds(time);
            gameObject.SetActive(false);
        }
        /// <summary>
        /// use this function whenever the PlayerModel switches to the game view after pressing the resume button
        /// exiting the options view or the character sheet
        /// </summary>
        public void ResetView()
        {
            throw new System.NotImplementedException();
        }
  /*      public void HideMenuButtonsAndDisable()
        {
            foreach (MenuButtonStructure mbs in MenuButtons)
            {
                StartCoroutine(ExitTheVoidTools.FadeImage(mbs.ButtonImage, ShowSpeed * .8f, 0f));
                StartCoroutine(ExitTheVoidTools.FadeText(mbs.ButtonText, ShowSpeed * .8f, 0f));
            }
        }*/

    
        public void Back()
        {
            throw new System.NotImplementedException();
        }






        public  Signal<model.PauseMenuState> SwitchViewSignal
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
