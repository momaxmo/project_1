﻿ 
using Assets.Scripts.common.config;
using Assets.Scripts.game.model;
 
using strange.extensions.mediation.impl;
 

namespace Assets.Scripts.ui.view.ingame
{
    public class GameMenuViewMediator : Mediator
    {
        [Inject]
        public GameMenuView GameMenu { get; set; }
/*        [Inject]
        public GamePausedSignal PausedSignal { get; set; }*/
        [Inject]
        public LevelStateModel GamestateModel { get; set; }

      

/*        [Inject]
        public FadeViewSignal FadeSignal { get; set; }*/
        [Inject]
        public SwitchToPauseMenuInterface SwitchToPauseMenu { get; set; }
        public override void OnRegister()
        {
           // PausedSignal.AddListener(BringUpPauseMenu);
            SwitchToPauseMenu.AddListener(BringUpPauseMenu);
            GameMenu.Init(GamestateModel.MinimumPauseTime);
          //  GameMenu.ShowPauseButtonsSignal.AddListener();
            
        }

        public override void OnRemove()
        {
            //PausedSignal.RemoveListener(BringUpPauseMenu);
            SwitchToPauseMenu.RemoveListener(BringUpPauseMenu);
        }

        public void BringUpPauseMenu(bool enable)
        {
            
            if (enable)
            {
                //check if the gameObject is active or not

                GameMenu.Show();
            }
            else
            {
                GameMenu.Hide();
                
            }
        }
 
    }
}
