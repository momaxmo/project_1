﻿

using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.game.boosters;
using Assets.Scripts.game.inventory.Model;
using Assets.Scripts.tools;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.ui.view.ingame
{
    public class InGameHud : View
    {
        private Image _healthbarBar;
        public float UpdateHealthBarSpeed;
        private Queue<HealthPercentageLifetime> _updateHealthRequests;
        public float MaxLifetimeInQueue = 0.1f;
        private bool _isProcessing;
        public  Image BoosterImage1;
        public  Image BoosterImage2;

        public float UpdateSpeed = 20f;
        public Image HealthBar
        {
            get
            {
                if (_healthbarBar == null)
                {
                    Transform t = transform.FindChild("HealthBarForeground");
                    _healthbarBar = t.GetComponent<Image>();
                }
                return _healthbarBar;
            }
        }

        private Queue<HealthPercentageLifetime> HealthBarQueueUpdates
        {
            get
            {
                if (_updateHealthRequests == null)
                {
                    _updateHealthRequests = new Queue<HealthPercentageLifetime>();
                }
                return _updateHealthRequests;
            }
        }

        public void UpdateHealth(int newHealth, int maxHealth)
        {
            float percentage = (float)newHealth / (float)maxHealth;
            percentage = Mathf.Clamp01(percentage);
            HealthBarQueueUpdates.Enqueue(new HealthPercentageLifetime(percentage));
            if (!_isProcessing)
            {
                StartCoroutine(Updatehealth());
            }

        }

        IEnumerator Updatehealth()
        {
            bool isProcessingItem = true;
            _isProcessing = true;
            HealthPercentageLifetime currentlyProcessing = HealthBarQueueUpdates.Dequeue();
            
            while (true)
            {

                //check if we currently have an item that is processing. if not dequeue it
                if (!isProcessingItem)
                {
                    //check if processed the whole queue already
                    if (HealthBarQueueUpdates.Count == 0)
                    {
                        _isProcessing = false;
                        yield break;
                    }
                    //check how long the next item has been waiting in queue for. If there is only one item left, ignore this check
                    if (HealthBarQueueUpdates.Count > 1)
                    {
                        float totalTimeInQueue = Time.time - HealthBarQueueUpdates.Peek().StartTime;
                        //if the total time in queue is above a certain threshold, discard it
                        if (totalTimeInQueue > MaxLifetimeInQueue)
                        {
                            HealthBarQueueUpdates.Dequeue();
                        }
                         
                    }
                    else
                    {
                        currentlyProcessing = HealthBarQueueUpdates.Dequeue();
                        isProcessingItem = true;
                    }

                }
                else
                {
                    HealthBar.fillAmount = Mathf.SmoothDamp(HealthBar.fillAmount, 
                        currentlyProcessing.HealthPercentage, ref UpdateSpeed, 0.1f);
                    //check if the fill amount is now close enough to the percentage of the currently processed item
                    if (MathTools.InclusiveRange(-0.01f, 0.01f,
                        currentlyProcessing.HealthPercentage - HealthBar.fillAmount))
                    {
                        isProcessingItem = false;
                    }
                }


                yield return null;
            }
        }

        protected struct HealthPercentageLifetime
        {
            public float HealthPercentage;
            public float StartTime;

            public HealthPercentageLifetime(float healthPercentage)
            {
                HealthPercentage = healthPercentage;
                StartTime = Time.time;
            }
        }
        /// <summary>
        /// updates the image and the descriptor of the currently equipped booster
        /// </summary>
        /// <param name="boosterModel"></param>
        /// <param name="index"></param>
        public void UpdateCharacterSheetBooster(IInventoryItem boosterModel, int index)
        {
            switch (index)
            {
                case 1:
                    BoosterImage1.sprite = boosterModel.BoosterSprite; 
                    break;
                case 2:
                    BoosterImage2.sprite = boosterModel.BoosterSprite; 
                    break;

            }
        }
    }
}
