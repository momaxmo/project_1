﻿ 

using Assets.Scripts.common.config; 
using Assets.Scripts.game.inventory.Model;
using strange.extensions.mediation.impl;

namespace Assets.Scripts.ui.view.ingame
{
    public class InGameHudMediator: Mediator
    {
        [Inject]
        public InGameHud Hud { get; set; }
        [Inject]
        public UpdateHealthSignal NewHealthSignal { get;set;}
        [Inject]
        public UpdateBooster UpdateBoosterSignal { get; set; }
        public override void OnRegister()
        {
            NewHealthSignal.AddListener(UpdateHealthMeter);
            UpdateBoosterSignal.AddListener(UpdateCharacterSheetBooster);
        }

        public override void OnRemove()
        {
            NewHealthSignal.RemoveListener(UpdateHealthMeter);
            UpdateBoosterSignal.RemoveListener(UpdateCharacterSheetBooster);
        }

        void UpdateHealthMeter(int currentHealth, int maxHealth)
        {
            Hud.UpdateHealth(currentHealth,maxHealth);
        }
        private void UpdateCharacterSheetBooster(IInventoryItem boosterModel, int index)
        {
            Hud.UpdateCharacterSheetBooster(boosterModel, index);
        }
    }
}
