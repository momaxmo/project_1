﻿ 
using Assets.Scripts.game.inventory.Model;
using Assets.Scripts.game.inventory.view;
using Assets.Scripts.tools;
using Assets.Scripts.ui.model;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.ui.view.pausemenu
{
    /// <summary>
    /// representation of the view of the character's currently equipped boosters. This view will allow the PlayerModel
    ///  to choose between different types of boosters that is in his/her inventory. Further more, this view 
    /// gives us information about the different types of effects a booster has on the character. 
    /// TODO: maybe display the enemy it dropped from?
    /// </summary>
    public class BoosterInventoryView : View, IPausedSubmenuView
    {
        private readonly Signal< PauseMenuState> _switchViewSignal = new Signal<PauseMenuState>();
        private InventoryGrid _grid;
        private Image _booster1Image;
        private Image _booster2Image;
        private Text _booster1Text;
        private Text _booster2Text;
        private Image _gridButtonBackGround ;
        public float FadeSpeed = 0.01f;
     
    
        private IInventoryItem _chosenBooster;
        private IterableButton _chosenButton;
        public void Init( )
        {
           _grid =  ExitTheVoidTools.FindObjectByName(this, "GridButton(booster inv)").GetComponent<InventoryGrid>();
           _booster1Image = ExitTheVoidTools.FindObjectByName(this, "InventoryBoosterPortrait1").GetComponent<Image>();
           _booster2Image = ExitTheVoidTools.FindObjectByName(this, "InventoryBoosterPortrait2").GetComponent<Image>();
           _booster1Text = ExitTheVoidTools.FindObjectByName(this, "InventoryBooster1Descriptor").GetComponent<Text>();
           _booster2Text = ExitTheVoidTools.FindObjectByName(this, "InventoryBooster2Descriptor").GetComponent<Text>();
           _gridButtonBackGround = ExitTheVoidTools.FindObjectByName(this, "GridButton(booster inv)").GetComponent<Image>(); 
        }
        public bool IsActive
        {
            get { return isActiveAndEnabled; }
            set {  enabled = value; }
        }

        public void Show()
        {

            Debug.Log("showing in boosterinventoryview");
            gameObject.SetActive(true);
            StartCoroutine(ExitTheVoidTools.FadeImage(_booster1Image, FadeSpeed, 1f));
            StartCoroutine(ExitTheVoidTools.FadeImage(_gridButtonBackGround, FadeSpeed, 1f));

            StartCoroutine(ExitTheVoidTools.FadeImage(_booster2Image, FadeSpeed, 1f));
            StartCoroutine(ExitTheVoidTools.FadeText(_booster1Text, FadeSpeed, 1f));
            StartCoroutine(ExitTheVoidTools.FadeText(_booster2Text, FadeSpeed, 1f));  
            
        }

        public void Hide()
        {
            if (gameObject.activeSelf)
            {
                StartCoroutine(ExitTheVoidTools.FadeImage(_booster1Image, FadeSpeed, 0f));
                StartCoroutine(ExitTheVoidTools.FadeImage(_booster2Image, FadeSpeed, 0f));
                StartCoroutine(ExitTheVoidTools.FadeImage(_gridButtonBackGround, FadeSpeed, 0f));

                StartCoroutine(ExitTheVoidTools.FadeText(_booster1Text, FadeSpeed, 0f));
                StartCoroutine(ExitTheVoidTools.FadeText(_booster2Text, FadeSpeed, 0f));
                StartCoroutine(ExitTheVoidTools.DisableGameObjectAfterSeconds(gameObject, FadeSpeed + 0.1f));
            }
            
        }

        public void Back()
        {
            SwitchViewSignal.Dispatch(PauseMenuState.CharacterSheet);
        }

        public float ShowSpeed
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        /// <summary>
        /// update the current invetory list
        /// </summary>
        public void UpdateInventory()
        {
            
        }
    
        public void SortInventory()
        {
            throw new System.NotImplementedException(); 
        }
        /// <summary>
        /// update the view by creating or removing buttons(with their images)
        /// </summary>
        public void UpdateInventoryView()
        {
            
        }
      

        private void EnableButtonInteraction(bool flag)
        {
             _grid.EnableButtonInteractivity(flag);
            
        }

        public Signal<PauseMenuState> SwitchViewSignal
        {
            get { return _switchViewSignal; }
             
        }
        /// <summary>
        /// updates the image and the descriptor of the currently equipped booster
        /// </summary>
        /// <param name="boosterModel"></param>
        /// <param name="index"></param>
        public void UpdateCharacterSheetBooster(IInventoryItem boosterModel, int index)
        {
            switch (index)
            {
                case 1:
                    _booster1Image.sprite = boosterModel.BoosterSprite;
                    _booster1Text.text = boosterModel.Description;
                    break;
                case 2:
                    _booster2Image.sprite = boosterModel.BoosterSprite;
                    _booster2Text.text = boosterModel.Description;
                    break;

            }
        }


    }
}
