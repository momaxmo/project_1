﻿ using Assets.Scripts.common.config; 
 using Assets.Scripts.game.inventory.Model; 
 using Assets.Scripts.ui.model;
using strange.extensions.mediation.impl;

namespace Assets.Scripts.ui.view.pausemenu
{
    public class BoosterInventoryViewMediator : Mediator
    {
        [Inject]
        public BoosterInventoryView BoosterInventoryView { get; set; }
        [Inject]
        public PauseMenuSignal SwitchPauseViewsSignal { get; set; }

        [Inject]
        public SwitchToBoosterViewSignal BoosterViewSignal { get; set; }
        [Inject]
        public UpdateBooster UpdateBoosterSignal { get; set; }
        [Inject]
        public SwitchBoosterSignal SwitchBoosterSignal { get; set; }
        
    
        
        public override void OnRegister()
        {
            BoosterInventoryView.Init();
            BoosterInventoryView.SwitchViewSignal.AddListener(SwitchViews);
          /*  BoosterInventoryView.TurnOnModalWindowSignal.AddListener();*/ 
            UpdateBoosterSignal.AddListener(UpdateCharacterSheetBooster);
            BoosterViewSignal.AddListener(ShowView);
            
        }

        public override void OnRemove()
        {
            BoosterInventoryView.SwitchViewSignal.RemoveListener(SwitchViews);
            UpdateBoosterSignal.RemoveListener(UpdateCharacterSheetBooster); 
            BoosterViewSignal.RemoveListener(ShowView);
        }
        private void SwitchViews(PauseMenuState newState)
        {
            SwitchPauseViewsSignal.Dispatch(newState);
        }

        private void ShowView(bool status)
        {
            if (status)
            {
                BoosterInventoryView.Show();
            }
            else
            {
                BoosterInventoryView.Hide();

            }
        }
        /// <summary>
        /// Updates the view's specified booster descriptor and image 
        /// </summary>
        /// <param name="boosterModel"></param>
        /// <param name="index"></param>
        private void UpdateCharacterSheetBooster(IInventoryItem boosterModel, int index)
        {
            BoosterInventoryView.UpdateCharacterSheetBooster(boosterModel, index);
        }
 
    }
}
