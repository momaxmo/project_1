﻿

using System.Collections.Generic;
using Assets.Scripts.exceptions; 
using Assets.Scripts.game.inventory.Model;
using Assets.Scripts.game.player.model; 
using Assets.Scripts.tools;
using Assets.Scripts.ui.model;
using Assets.Scripts.ui.structs;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.ui.view.pausemenu
{
    /// <summary>
    /// A representation of the character sheet view
    /// </summary>
    public class CharacterSheetView : View, IPausedSubmenuView
    {
        public Image CharacterPortrait;
        public Text CharacterStatsText;
        public Image BoosterPortrait1;
        public Text Booster1Descriptor;
        public Image BoosterPortrait2;
        public Text Booster2Descriptor; 
        public MenuButtonStructure ChangeBoosterInventoryButton;
        //private PlayerAttributesAndStats _playerStats;
        List<GameObject> _gameObjectsInView = new List<GameObject>(6);
        //private IBooster _booster1;
        //private IBooster _booster2;
        private IPlayerModel _playerModel;
        readonly private Signal<PauseMenuState> _switchViewSignal = new Signal<PauseMenuState>();
        //readonly private Signal 
        public Signal<PauseMenuState> SwitchViewSignal
        {
            get { return _switchViewSignal; }
        }
        public bool IsActive
        {
            get;
            set;
        }

        public void Init(IPlayerModel playerModel)
        {
            _playerModel = playerModel; 
            CharacterStatsText = ExitTheVoidTools.FindObjectByName(transform, "Characterstats").GetComponent<Text>(); 
            CharacterPortrait = ExitTheVoidTools.FindObjectByName(transform, "Characterportrait").GetComponent<Image>(); 
            BoosterPortrait1 = ExitTheVoidTools.FindObjectByName(transform, "BoosterPortrait1").GetComponent<Image>();
            BoosterPortrait2 = ExitTheVoidTools.FindObjectByName(transform, "BoosterPortrait2").GetComponent<Image>();
            BoosterPortrait1.sprite = playerModel.Booster1.InventoryItem.BoosterSprite;
            BoosterPortrait2.sprite = playerModel.Booster2.InventoryItem.BoosterSprite;

            Booster1Descriptor = ExitTheVoidTools.FindObjectByName(transform, "Booster1Descriptor").GetComponent<Text>();
            Booster2Descriptor = ExitTheVoidTools.FindObjectByName(transform, "Booster2Descriptor").GetComponent<Text>();
            Booster1Descriptor.text = playerModel.Booster1.InventoryItem.Description;
            Booster2Descriptor.text = playerModel.Booster1.InventoryItem.Description;
            ChangeBoosterInventoryButton =
                ExitTheVoidTools.FindObjectByName(transform, "Change Boosters").GetComponent<MenuButtonStructure>();



            _gameObjectsInView.Add(CharacterStatsText.gameObject);
            _gameObjectsInView.Add(CharacterPortrait.gameObject);
            _gameObjectsInView.Add(BoosterPortrait1.gameObject);
            _gameObjectsInView.Add(BoosterPortrait2.gameObject);
            _gameObjectsInView.Add(Booster1Descriptor.gameObject);
            _gameObjectsInView.Add(Booster2Descriptor.gameObject);
            _gameObjectsInView.Add(ChangeBoosterInventoryButton.gameObject);


            UpdateText();
        }
       
        /// <summary>
        /// shows the viewable elements associated with this view
        /// </summary>
        public void Show()
        {
            StartCoroutine(ExitTheVoidTools.EnableGameObjectsAfterNSeconds(_gameObjectsInView, 0f));

            UpdateText();
            StartCoroutine(ExitTheVoidTools.FadeImage(CharacterPortrait, 0.2f, 1f));
            StartCoroutine(ExitTheVoidTools.FadeImage(BoosterPortrait1, 0.2f, 1f));
            StartCoroutine(ExitTheVoidTools.FadeImage(BoosterPortrait2, 0.2f, 1f));
            StartCoroutine(ExitTheVoidTools.FadeImage(ChangeBoosterInventoryButton.ButtonImage, 0.2f, 1f));


            StartCoroutine(ExitTheVoidTools.FadeText(CharacterStatsText, 0.2f, 1f));
            StartCoroutine(ExitTheVoidTools.FadeText(Booster1Descriptor, 0.2f, 1f));
            StartCoroutine(ExitTheVoidTools.FadeText(Booster2Descriptor, 0.2f, 1f));
            StartCoroutine(ExitTheVoidTools.FadeText(ChangeBoosterInventoryButton.ButtonText, 0.2f, 1f));
            gameObject.SetActive(true);


        }
        /// <summary>
        /// Hides the game  elements associated with this view
        /// </summary>
        public void Hide()
        {
            if (gameObject.activeSelf)
            {
                StartCoroutine(ExitTheVoidTools.FadeImage(CharacterPortrait, 0.2f, 0f));
                StartCoroutine(ExitTheVoidTools.FadeImage(BoosterPortrait1, 0.2f, 0f));
                StartCoroutine(ExitTheVoidTools.FadeImage(BoosterPortrait2, 0.2f, 0f));
                StartCoroutine(ExitTheVoidTools.FadeImage(ChangeBoosterInventoryButton.ButtonImage, 0.2f, 0f));


                StartCoroutine(ExitTheVoidTools.FadeText(CharacterStatsText, 0.2f, 0f));
                StartCoroutine(ExitTheVoidTools.FadeText(Booster1Descriptor, 0.2f, 0f));
                StartCoroutine(ExitTheVoidTools.FadeText(Booster2Descriptor, 0.2f, 0f));
                StartCoroutine(ExitTheVoidTools.FadeText(ChangeBoosterInventoryButton.ButtonText, 0.2f, 0f));
                gameObject.SetActive(false);
            }
           

        }

        public void Back()
        {
            SwitchViewSignal.Dispatch(PauseMenuState.MainPauseMenu);
        }

        public void ChangeBoosters()
        {
            SwitchViewSignal.Dispatch(PauseMenuState.BoosterChange);
        }


        /// <summary>
        /// Updates characterStatsText with the PlayerModel's stats
        /// 
        /// </summary>
        private void UpdateText()
        {
            if (CharacterStatsText == null)
            {
                throw new NoObjectAttachedException(transform, CharacterStatsText, this, gameObject);
            }
            string stats = "";
            stats += "Health Pool(HP): " + _playerModel.PlayerStats.BaseHealthPool + "\n";
            stats += "Attack Power" + _playerModel.PlayerStats.BaseAttackPower + "\n";
            stats += "fill out the rest of the stats, this is just a test";
            CharacterStatsText.text = stats;
        }

        public float ShowSpeed
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }
        /// <summary>
        /// updates the image and the descriptor of the currently equipped booster
        /// </summary>
        /// <param name="boosterModel"></param>
        /// <param name="index"></param>
        public void UpdateCharacterSheetBooster(IInventoryItem boosterModel, int index)
        {
            switch (index)
            {
                case 1 :
                    BoosterPortrait1.sprite = boosterModel.BoosterSprite;
                    Booster1Descriptor.text = boosterModel.Description;
                    break;
                case 2:
                    BoosterPortrait2.sprite = boosterModel.BoosterSprite;
                    Booster2Descriptor.text = boosterModel.Description;
                    break;
                    
            }
        }
    }
}
