﻿using Assets.Scripts.common.config; 
using Assets.Scripts.game.inventory.Model;
using Assets.Scripts.game.player.model; 
using Assets.Scripts.ui.model;
using strange.extensions.mediation.impl;


namespace Assets.Scripts.ui.view.pausemenu
{
    public class CharacterSheetViewMediator : Mediator
    {
        [Inject]
        public PauseMenuSignal SwitchPauseViewsSignal { get; set; }

        [Inject]
        public CharacterSheetView CharacterSheet { get; set; }

       

        [Inject]
        public SwitchToCharacterSheetSignal CharacterSheetSignal { get; set; }
        [Inject]
        public UpdateBooster UpdateBoosterSignal { get; set; }

      [Inject]
        public IPlayerModel PlayerModel { get; set; }
        public override void OnRegister()
        {
            CharacterSheet.Init(PlayerModel);
            CharacterSheet.SwitchViewSignal.AddListener(SwitchViews);
            UpdateBoosterSignal.AddListener(UpdateCharacterSheetBooster);
            CharacterSheetSignal.AddListener(ShowView);
            UpdateBoosterSignal.AddListener(UpdateCharacterSheetBooster);

        }

        public override void OnRemove()
        {
            CharacterSheet.SwitchViewSignal.RemoveListener(SwitchViews);
            UpdateBoosterSignal.RemoveListener(UpdateCharacterSheetBooster);
            CharacterSheetSignal.RemoveListener(ShowView);
            UpdateBoosterSignal.RemoveListener(UpdateCharacterSheetBooster);

        }

        private void SwitchViews(PauseMenuState newState)
        {
            SwitchPauseViewsSignal.Dispatch(newState);
        }


        private void ShowView(bool state)
        {
            if (state)
            {
                CharacterSheet.gameObject.SetActive(enabled);
                CharacterSheet.Show();

            }
            else
            {
                CharacterSheet.Hide();
            }
        }
        /// <summary>
        /// Updates the view's specified booster descriptor and image 
        /// </summary>
        /// <param name="boosterModel"></param>
        /// <param name="index"></param>
        private void UpdateCharacterSheetBooster(IInventoryItem boosterModel, int index)
        {
            CharacterSheet.UpdateCharacterSheetBooster(boosterModel, index);
        }
    }
}
