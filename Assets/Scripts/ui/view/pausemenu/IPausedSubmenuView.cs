﻿ 

using Assets.Scripts.ui.model;
using strange.extensions.signal.impl;

namespace Assets.Scripts.ui.view.pausemenu
{
    /// <summary>
    /// This interface is a representation of a pause view. 
    /// </summary>
    public interface IPausedSubmenuView
    {
        bool IsActive { get; set; }
        void Show();
        void Hide();
        void Back();
        Signal<PauseMenuState> SwitchViewSignal { get;  }
        
          float ShowSpeed { get; set; }
 
    }
}
