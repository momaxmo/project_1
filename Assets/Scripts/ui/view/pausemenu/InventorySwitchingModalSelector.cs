﻿ 
using Assets.Scripts.tools; 
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.ui.view.pausemenu
{
    public class InventorySwitchingModalSelector : View
    {
        public Image BackgroundImage;
        public Text LabelText;
        public Button Selection1;
        public Button Selection2;
        public Button Cancel;
        public float FadeInSpeed = 0.01f;
        public Signal<int> SelectionMade = new Signal<int>();
        public Signal EnableGrid = new Signal();
        public GameObject PreventativeMeasuresPanel; //used to prevent clicking outside this modal window

        public void EquipBoosterToSlot(int index)
        {
            SelectionMade.Dispatch(index);
            Hide();
        }
        public void Show()
        {
            gameObject.SetActive(true);
            PreventativeMeasuresPanel.SetActive(true);
            int currentObjectIndex = transform.GetSiblingIndex(); //get the current transform index
            PreventativeMeasuresPanel.transform.SetSiblingIndex(currentObjectIndex-1);//set the preventative panel index to be one right above the modal window
            StartCoroutine(ExitTheVoidTools.FadeImage(BackgroundImage, FadeInSpeed, 1f));
            StartCoroutine(ExitTheVoidTools.FadeImage(Selection1.image, FadeInSpeed, 1));
            StartCoroutine(ExitTheVoidTools.FadeImage(Selection2.image, FadeInSpeed, 1));
            StartCoroutine(ExitTheVoidTools.FadeImage(Cancel.image, FadeInSpeed, 1));

            StartCoroutine(ExitTheVoidTools.FadeText(LabelText, FadeInSpeed, 1));
            StartCoroutine(ExitTheVoidTools.FadeText(Selection1.GetComponentInChildren<Text>(), FadeInSpeed, 1));
            StartCoroutine(ExitTheVoidTools.FadeText(Selection2.GetComponentInChildren<Text>(), FadeInSpeed, 1));
            StartCoroutine(ExitTheVoidTools.FadeText(Cancel.GetComponentInChildren<Text>(), FadeInSpeed, 1));

        }


        public void CancelSelection()
        {
            Hide();
        }
        public bool IsActive
        {
            get { return gameObject.activeSelf; }
            set { }
        }



        public void Show(Vector2 coordinates)
        {
            Show();
            transform.position = coordinates;
        }

        public void Hide()
        {
            if (gameObject.activeSelf)
            {
                //PreventativeMeasuresPanel.SetActive(false);
                int currentObjectIndex = transform.GetSiblingIndex(); //get the current transform index
                PreventativeMeasuresPanel.transform.SetSiblingIndex(currentObjectIndex - 2);//set the preventative panel index to be two right above the modal window
                StartCoroutine(ExitTheVoidTools.FadeImage(BackgroundImage, FadeInSpeed, 0));
                StartCoroutine(ExitTheVoidTools.FadeImage(Selection1.image, FadeInSpeed, 0));
                StartCoroutine(ExitTheVoidTools.FadeImage(Selection2.image, FadeInSpeed, 0));
                StartCoroutine(ExitTheVoidTools.FadeImage(Cancel.image, FadeInSpeed, 0));

                StartCoroutine(ExitTheVoidTools.FadeText(LabelText, FadeInSpeed, 0));
                StartCoroutine(ExitTheVoidTools.FadeText(Selection1.GetComponentInChildren<Text>(), FadeInSpeed, 0));
                StartCoroutine(ExitTheVoidTools.FadeText(Selection2.GetComponentInChildren<Text>(), FadeInSpeed, 0));
                StartCoroutine(ExitTheVoidTools.FadeText(Cancel.GetComponentInChildren<Text>(), FadeInSpeed, 0));
                StartCoroutine(ExitTheVoidTools.DisableGameObjectAfterSeconds(gameObject, FadeInSpeed + 0.001f));
            }
           
           


        }

        public void Back()
        {

        }


        public void SetPositon(Vector2 position)
        {
            GetComponent<RectTransform>().position = position;
        }
    }
}
