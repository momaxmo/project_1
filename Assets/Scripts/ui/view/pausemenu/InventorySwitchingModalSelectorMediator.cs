﻿ 
using Assets.Scripts.common.config; 
using Assets.Scripts.game.inventory.Model;
using Assets.Scripts.game.inventory.view;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.ui.view.pausemenu
{
    public class InventorySwitchingModalSelectorMediator: Mediator
    {
        [Inject]
        public InventorySwitchingModalSelector ModalWindow { get; set; }
        
        private IInventoryItem _itemToSwitch;
        private IterableButton _buttonWithOldItemValue;
        [Inject]
        public EnableBoosterModalSwitchView EnableModalWindow { get; set; }
        [Inject]
        public SwitchBoosterSignal SwitchBoosters { get; set; }
        public override void OnRegister()
        {
            EnableModalWindow.AddListener(EnableWindowAndHoldItem);
            ModalWindow.SelectionMade.AddListener(DispatchSwitchSignal);
        }

        public override void OnRemove()
        {
            EnableModalWindow.RemoveListener(EnableWindowAndHoldItem);
            ModalWindow.SelectionMade.RemoveListener(DispatchSwitchSignal);
        }

        private void EnableWindowAndHoldItem(IterableButton button,IInventoryItem item)
        {
            _itemToSwitch = item;
            _buttonWithOldItemValue = button;
            ModalWindow.SetPositon(button.Position);
            ModalWindow.Show();
        }

        private void DispatchSwitchSignal(int index)
        {
            SwitchBoosters.Dispatch(_itemToSwitch,index,_buttonWithOldItemValue);
           Debug.Log("in "+GetType()+" dispatching a signal to switch");
        }

        
    }
}
