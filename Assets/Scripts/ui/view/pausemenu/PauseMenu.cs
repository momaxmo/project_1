﻿ 
using Assets.Scripts.tools;
using Assets.Scripts.ui.model;
using Assets.Scripts.ui.structs;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;

namespace Assets.Scripts.ui.view.pausemenu
{
    /// <summary>
    /// Pause menu: shows the various buttons that a user is capable of interacting with 
    /// </summary>
    public class PauseMenu: View, IPausedSubmenuView
    {
        readonly private Signal<PauseMenuState> _switchViewSignal = new Signal<PauseMenuState>();
        public MenuButtonStructure[] MenuButtons;

        public Signal<PauseMenuState> SwitchViewSignal
        {
            get { return _switchViewSignal; }
        }
        public Signal UnpauseGame = new Signal();

        public float ShowSpeed { get; set; }
        public void Init()
        {
            
        }
        /// <summary>
        /// Called by the resume button in the scene
        /// </summary>
        public void ResumeGame()
        {
            //SwitchViewSignal.Dispatch(PauseMenuState.Off);
            UnpauseGame.Dispatch();
            //throw new NotImplementedException();
        }
        public bool IsActive { get; set; }

      

        public void SwitchToCharacterSheetView()
        {
            SwitchViewSignal.Dispatch(PauseMenuState.CharacterSheet);
        }

        public void SwitchToOptionsAndSettingsView()
        {
            SwitchViewSignal.Dispatch(PauseMenuState.SettingsAndOptions);
        }

        public void SwitchToExitToMenuView()
        {
            SwitchViewSignal.Dispatch(PauseMenuState.ExitToMainMenu);
        }

        public void ExitGame()
        {
            SwitchViewSignal.Dispatch(PauseMenuState.ExitToMainMenu);
        }

        public void Show()
        {
            gameObject.SetActive(true);
            foreach (MenuButtonStructure mbs in MenuButtons)
            {
                StartCoroutine(ExitTheVoidTools.FadeImage(mbs.ButtonImage, ShowSpeed*.8f, 1f));
                StartCoroutine(ExitTheVoidTools.FadeText(mbs.ButtonText, ShowSpeed*.8f, 1f));
            }
        }

        /// <summary>
        /// Hide buttons then disable the view
        /// </summary>
        public void Hide()
        {
            foreach (MenuButtonStructure mbs in MenuButtons)
            {
                StartCoroutine(ExitTheVoidTools.FadeImage(mbs.ButtonImage, ShowSpeed * .6f, 0f));
                StartCoroutine(ExitTheVoidTools.FadeText(mbs.ButtonText, ShowSpeed * .6f, 0f));
            }
            StartCoroutine(ExitTheVoidTools.DisableGameObjectAfterSeconds(gameObject, ShowSpeed*.75f));
        }


        public void Back()
        {
            SwitchViewSignal.Dispatch(PauseMenuState.MainPauseMenu);
        }

        

  

    }
}
