﻿ 

using Assets.Scripts.common.config;
using Assets.Scripts.game.model;
using Assets.Scripts.ui.model;
using strange.extensions.mediation.impl;

namespace Assets.Scripts.ui.view.pausemenu
{
    /// <summary>
    /// Mediates PauseButtonView, not allowing it to communicate with the business logic. 
    /// </summary>
    public class PauseMenuMediator : Mediator
    {
        [Inject]
        public GamePauseCommandSignal PauseCommandSignal { get; set; }
        [Inject]
        public PauseMenu PausemenuView { get; set; }
/*        [Inject]
        public GamePauseCommandSignal PauseSignal { get; set; }*/

        [Inject] public ShowPauseButtonsSignal ShowPauseButtonsSignal { get; set; }
 /*       [Inject]
        public ResetMenuSignal ResetmenuSignal { get; set; }*/
        [Inject]
        public LevelStateModel GamestateModel { get; set; }

        [Inject]
        public PauseMenuSignal PauseMenusignal { get; set; }
 
      

        public override void OnRegister()
        {
            ShowPauseButtonsSignal.AddListener(ShowPauseMenuButtons);
            PausemenuView.UnpauseGame.AddListener(UnpauseGame);
            PausemenuView.SwitchViewSignal.AddListener(SwitchViews);
            PausemenuView.ShowSpeed = GamestateModel.MinimumPauseTime;

        }

        public override void OnRemove()
        {
            PausemenuView.UnpauseGame.RemoveListener(UnpauseGame);
            ShowPauseButtonsSignal.RemoveListener(ShowPauseMenuButtons);
            PausemenuView.SwitchViewSignal.RemoveListener(SwitchViews);
        }
        /// <summary>
        /// Switches the view according to the new state. This will be listened to be the PauseMenuController.cs
        /// </summary>
        /// <param name="newState"></param>
        public void SwitchViews(PauseMenuState newState)
        {
           // PauseSignal.Dispatch();
            //ResetmenuSignal.Dispatch();
            PauseMenusignal.Dispatch(newState);
        }

        public void ShowPauseMenuButtons(bool state)
        {
            if (state)
            {
                PausemenuView.Show();
            }
            else
            {
                PausemenuView.Hide();
            }
        }

        /// <summary>
        /// called after resume button has been clicked.
        /// </summary>
        public void UnpauseGame()
        {
            PauseCommandSignal.Dispatch();
        }
    }
}
