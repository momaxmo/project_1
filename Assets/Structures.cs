﻿ 
using UnityEngine;

namespace Assets
{
    class Structures
    {
    }
    /// <summary>
    /// structure that holds a character's gameobject, transform, speed properties
    /// </summary>
    public struct CharacterControllerProperties
    {
        public GameObject Go;
        public Vector2 Velocity;
        public Transform GoTransform;


        public CharacterControllerProperties(GameObject go, Vector2 vel)
        {
            Go = go;
            GoTransform = go.GetComponent<Transform>();
            Velocity = vel;
        }
    }
}
