﻿using UnityEngine; 
using Assets.Scripts.game.sfx;

public class testingGhosting : MonoBehaviour
{

    private GhostEffect _gcContainer;

    private SpriteRenderer _srSpriteRenderer;
 
    public float Speed;
    public float SpawnRate;
    public int MaxGhosts;
   // public int Spacing;
    private Vector3 _originalpos;
    [SerializeField] private Color _desiredColor;
    void Start()
    {
        _gcContainer = GetComponent<GhostEffect>();
        _srSpriteRenderer = GetComponent<SpriteRenderer>();
        _originalpos = transform.position;
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
            _gcContainer.StartEffect(MaxGhosts, SpawnRate, _srSpriteRenderer, _desiredColor);
        if (Input.GetKeyDown(KeyCode.F2))
            _gcContainer.StopEffect();
        if (Input.GetKeyDown(KeyCode.F3))
            transform.position = _originalpos;
        transform.position += (Speed * Time.deltaTime) * Vector3.right;
    }
}
